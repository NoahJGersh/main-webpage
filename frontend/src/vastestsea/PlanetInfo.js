import React from 'react';
import j2r from 'json2react';

/**
 * Planet Information
 *
 * @param {object} props the properties
 * @return {objct} JSX
 */
function PlanetInfo(props) {
  return (
    <div
      id={props.planet + '_info'}
    >
      {props.info.map((elem) => {
        return j2r(React.createElement, props.mapper, elem);
      })}
    </div>
  );
}

export default PlanetInfo;
