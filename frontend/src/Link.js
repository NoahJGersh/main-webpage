import React from 'react';
import {history} from 'react-router-dom';

/**
 * Link Element
 *
 * @param {object} props the link properties
 * @return {object} JSX
 */
function Link(props) {
  return (
    <a href={props.link}/>
  );
}