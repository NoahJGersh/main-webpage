# Noahger.sh

My main webpage, revised using a modern full-stack framework. The original webpage was static HTML/CSS with basic JS. This conversion is much more scalable and responsive. Out with the old way of hardcoding the Fjorunskara Lexicon and in with a new data-based approach. Worldbuilding information is stored in a database via PostreSQL, retrieved and modified through Express using OpenAPI compliant endpoints, and displayed to the user via React components. Responsive design is achieved with Material UI theme breakpoints.

### Current Features
- Main landing page
- Vastest Sea information
    - Planet info (content incomplete)
    - Deity info (content incomplete)

### Planned Features
- Vastest Sea information
    - Civilization info
    - Language info
    - Continue adding content (ongoing)
- Authorized contributors
    - Authenticate authorized contributors to make suggestions for the worldbuilding aspects
- Remote development
    - Client application to modify worldbuilding info
    - Receive notifications from contributor suggestions

### Technologies Used
- React.js
- MaterialUI
- Node.js
- Express with OpenAPI
- PostgreSQL

### Acknowledgements:
Based partially on my coursework from CSE183 - Web Applications, from UCSC, Summer 2021. Involves some adapted framework setup from Dr. David Harrison.
