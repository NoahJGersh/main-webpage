const {Pool} = require('pg');

const pool = new Pool({
  host: 'localhost',
  port: 5432,
  database: process.env.POSTGRES_DB,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
});

exports.getAll = async (req, res) => {
  const select =
    `SELECT name, ENCODE(description, 'escape')::jsonb AS description ` +
    `FROM planets ORDER BY name`;
  const query = {
    text: select,
    values: [],
  };

  const {rows} = await pool.query(query);

  return res.status(200).json(
    JSON.parse(JSON.stringify(rows).replace(/\&quot;/g, '\\"'))
  );
};

exports.getAllDescriptions = async (req, res) => {
  const select =
    `SELECT name, ENCODE(description, 'escape')::jsonb AS description, ` +
    `ENCODE(information, 'escape')::jsonb AS information ` +
    `FROM planets ORDER BY name`;
  const query = {
    text: select,
    values: [],
  };

  const {rows} = await pool.query(query);
  return res.status(200).json(
    JSON.parse(JSON.stringify(rows).replace(/\&quot;/g, '\\"'))
  );
};

exports.getByName = async (req, res) => {
  const select =
    `SELECT name, ENCODE(description, 'escape')::jsonb AS description, ` +
    `ENCODE(information, 'escape')::jsonb AS information ` +
    `FROM planets WHERE LOWER(name) = $1`;
  const query = {
    text: select,
    values: [req.params.name],
  };

  const {rows} = await pool.query(query);
  if (rows.length === 0) return res.status(404).send();
  res.status(200).json(
    JSON.parse(JSON.stringify(rows[0]).replace(/\&quot;/g, '\\"'))
  );
};


exports.post = async (req, res) => {
  const post = 'INSERT INTO planets (name, description, information) ' +
    'VALUES ($1, $2, $3) ' +
    'ON CONFLICT (name) DO NOTHING ' +
    'RETURNING id';
  const query = {
    text: post,
    values: [
      req.body.name,
      JSON.stringify(req.body.description),
      JSON.stringify(req.body.information),
    ],
  };

  const {rows} = await pool.query(query);
  if (rows.length === 0) return res.status(409).send();
  res.status(201).json({
    id: rows[0].id,
    name: req.body.name,
    description: req.body.description,
    information: req.body.information,
  });
};

exports.put = async (req, res) => {
  const find = 'SELECT id FROM planets WHERE LOWER(name) = $1';
  const fQuery = {
    text: find,
    values: [req.params.name],
  };

  let {rows} = await pool.query(fQuery);
  let pQuery;
  let status;
  if (rows.length > 0) {
    const put = 'UPDATE planets SET description = $2 ' +
      ' information = $3 WHERE id = $1 RETURNING *';
    pQuery = {
      text: put,
      values: [
        rows[0].id,
        req.body.description ?
          JSON.stringify(req.body.description) :
            rows[0].description,
        req.body.information ?
          JSON.stringify(req.body.information) :
            rows[0].information,
      ],
    };
    status = 200;
  } else {
    const post = 'INSERT INTO planets (name, description, information) ' +
      'VALUES ($1, $2, $3) RETURNING *';
    pQuery = {
      text: put,
      values: [
        req.params.name,
        req.body.description
      ],
    };
    status = 201;
  }

  rows = {rows} = await pool.query(pQuery);
  res.status(status).json(rows[0]);
};
