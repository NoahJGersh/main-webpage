const {Pool} = require('pg');
const fetch = require('isomorphic-fetch');

const pool = new Pool({
  host: 'localhost',
  port: 5432,
  database: process.env.POSTGRES_DB,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
});

const reformat = (json) => {
  return JSON.parse(
      JSON.stringify(json)
      .replace(/\&quot;/g, '\\"')
      .replace(/\&redacted;/g, `██████████`)
      .replace(/\\\\u[0-9A-F]{4}/g, (match) => JSON.parse(`"${match}"`)));
};

exports.getAll = async (req, res) => {
  const select =
    `SELECT name_roman, name_glyph, ENCODE(description, 'escape')::jsonb AS description ` +
    `FROM languages ORDER BY name_roman`;
  const query = {
    text: select,
    values: [],
  };

  const {rows} = await pool.query(query);
  return res.status(200).json(reformat(rows));
};

exports.getByName = async (req, res) => {
  const select =
    `SELECT id, name_roman, name_glyph, ` +
    `ENCODE(description, 'escape')::jsonb AS description, ` +
    `ENCODE(main_info, 'escape')::jsonb AS main_info, ` +
    `ENCODE(inv_orth_info, 'escape')::jsonb AS inv_orth_info, ` +
    `ENCODE(syntax_info, 'escape')::jsonb AS syntax_info, ` +
    `ENCODE(lex_info, 'escape')::jsonb AS lex_info ` +
    `FROM languages WHERE LOWER(name_roman) = $1`;
  const query = {
    text: select,
    values: [req.params.name],
  };

  const {rows} = await pool.query(query);
  if (rows.length === 0) return res.status(404).send();
  return res.status(200).json(reformat(rows[0]));
};


exports.post = async (req, res) => {
  const post = 'INSERT INTO languages (name_roman, name_glyph, description, main_info, inv_orth_info, syntax_info, lex_info) ' +
    'VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ' +
    'ON CONFLICT (name) DO NOTHING ' +
    'RETURNING id';
  const query = {
    text: post,
    values: [
      req.body.name_roman,
      req.body.name_glyph,
      JSON.stringify(req.body.description),
      JSON.stringify(req.body.main_info),
      JSON.stringify(req.body.inv_orth_info),
      JSON.stringify(req.body.syntax_info),
      JSON.stringify(req.body.lex_info),
    ],
  };

  const {rows} = await pool.query(query);
  if (rows.length === 0) return res.status(409).send();
  return res.status(201).json({
    id: rows[0].id,
    ...req.body,
  });
};

exports.put = async (req, res) => {
  const find = 'SELECT id FROM languages WHERE LOWER(name_roman) = $1';
  const fQuery = {
    text: find,
    values: [req.params.name],
  };

  let {rows} = await pool.query(fQuery);
  let pQuery;
  let status;
  if (rows.length > 0) {
    const put = 'UPDATE languages SET ' +
      'name_roman = $2 name_glyph = $3 description = $4 main_info = $5 ' +
      'inv_orth_info = $6 syntax_info = $7 lex_info = $8 ' +
      'WHERE id = $1 RETURNING *';
    pQuery = {
      text: put,
      values: [
        rows[0].id,
        req.body.name_roman ?
          JSON.stringify(req.body.name_roman) :
            rows[0].name_roman,
        req.body.name_glyph ?
          JSON.stringify(req.body.name_glyph) :
            rows[0].name_glyph,
        req.body.description ?
          JSON.stringify(req.body.description) :
            rows[0].description,
        req.body.main_info ?
          JSON.stringify(req.body.main_info) :
            rows[0].main_info,
        req.body.inv_orth_info ?
          JSON.stringify(req.body.inv_orth_info) :
            rows[0].inv_orth_info,
        req.body.syntax_info ?
          JSON.stringify(req.body.syntax_info) :
            rows[0].syntax_info,
        req.body.lex_info ?
          JSON.stringify(req.body.lex_info) :
            rows[0].lex_info,
      ],
    };
    status = 200;
  } else {
    const post = 'INSERT INTO languages (name_roman, name_glyph, description, main_info, inv_orth_info, syntax_info, lex_info) ' +
      'VALUES ($1, $2, $3, $4, $5, $6, $7, $8) ' +
      'ON CONFLICT (name) DO NOTHING ' +
      'RETURNING id';
    pQuery = {
      text: post,
      values: [
        req.body.name_roman,
        req.body.name_gyph,
        JSON.stringify(req.body.description),
        JSON.stringify(req.body.main_info),
        JSON.stringify(req.body.inv_orth_info),
        JSON.stringify(req.body.syntax_info),
        JSON.stringify(req.body.lex_info),
      ],
    };
    status = 201;
  }

  rows = {rows} = await pool.query(pQuery);
  return res.status(200).json(rows[0]);
};

exports.getLexSections = async (req, res) => {
  const get = `SELECT * FROM lexicons WHERE language = $1`;
  const query = {
    text: get,
    values: [req.params.name.toLowerCase()],
  };

  const {rows} = await pool.query(query);
  if (rows.length === 0) return res.status(404).send();

  const sections = [];
  const lookAheads = {};

  // Setup multicharacter lookaheads for second loop
  rows[0].order.forEach((s) => {
    if (s.length > 1) {
      if (lookAheads[s[0]]) lookAheads[s[0]].push(s);
      else lookAheads[s[0]] = [s];
    }
  });

  // Second loop to sort each section
  rows[0].order.forEach((s) => {
    const section = rows[0].sections[s];

    if (section) {
      section.words?.sort((a, b) => {
        const aName = a.name_roman.toLowerCase();
        const bName = b.name_roman.toLowerCase();

        if (aName === bName) return 0;

        for (let i = 0, j = 0; i < aName.length && j < bName.length; ++i, ++j) {
          // Look at next characters
          let aCur = aName[i];
          let bCur = bName[j];

          // Check if current characters are parts of ligatures
          if (lookAheads[aCur]) {
            let bestLength = 1;
            lookAheads[aName[i]].forEach((combo) => {
              if (combo.length > bestLength && aName.substring(i, i + combo.length) === combo) {
                aCur = combo;
                i += combo.length - bestLength;
                bestLength = combo.length;
              }
            });
          }

          if (lookAheads[bCur]) {
            let bestLength = 1;
            lookAheads[bName[j]].forEach((combo) => {
              if (combo.length > bestLength && bName.substring(j, j + combo.length) === combo) {
                bCur = combo;
                j += combo.length - bestLength;
                bestLength = combo.length;
              }
            });
          }

          // Get start's alphabetical order
          const aIdx = rows[0].order.indexOf(aCur);
          const bIdx = rows[0].order.indexOf(bCur);

          // Compare order
          if (aIdx < bIdx) {
            return -1;
          } else if (aIdx > bIdx) {
            return 1;
          }
        }

        // If lengths different but so far all characters are different, compare lengths
        if (aName.length < bName.length) return -1;
        else if (aName.length > bName.length) return 1;
      });

      sections.push({start: s, values: section});
    }
  });

  return res.status(200).json({sections: sections});
};

exports.getLexSection = async (req, res) => {
  const getLex = `SELECT sections FROM lexicons WHERE language = $1`;
  const getQuery = {
    text: getLex,
    values: [req.params.name.toLowerCase()],
  };

  const {rows} = await pool.query(getQuery);
  if (rows.length === 0) return res.status(404).send();

  const section = rows[0].sections[req.params.start.toLowerCase()];
  if (section) return res.status(200).json({start: req.params.start.toLowerCase(), values: section});
  return res.status(404).send();
};

exports.getWord = async (req, res) => {
  const getLex = `SELECT * FROM lexicons WHERE language = $1`;
  const getQuery = {
    text: getLex,
    values: [req.params.name.toLowerCase()],
  };

  const {rows} = await pool.query(getQuery);

  if (rows.length === 0) return res.status(404).send();

  const start = req.query.start?.toLowerCase() ||
    req.params.word[0].toLowerCase();

  let sectionChoice = start;

  const lookAheads = {};

  // Setup multicharacter lookaheads for second loop
  rows[0].order.forEach((s) => {
    if (s.length > 1) {
      if (lookAheads[s[0]]) lookAheads[s[0]].push(s);
      else lookAheads[s[0]] = [s];
    }
  });

  if (lookAheads[start]) {
    let bestLength = 1;
    lookAheads[start].forEach((combo) => {
      if (combo.length > bestLength && req.params.word.toLowerCase().substring(0, combo.length) === combo) {
        sectionChoice = combo;
        bestLength = combo.length;
      }
    });
  }

  const section = rows[0].sections[sectionChoice];
  if (!section) return res.status(404).send();

  let word;
  section.words?.some((w) => {
    if (w.name_roman.toLowerCase() === req.params.word.toLowerCase() || w.name_glyph.toLowerCase() === req.params.word.toLowerCase()) {
      word = w;
      return true;
    }
  });

  if (!word) return res.status(404).send();
  else return res.status(200).json(word);
};

exports.translate = async (req, res) => {
  const getLex = `SELECT * FROM lexicons WHERE language = $1`;
  const query = {
    text: getLex,
    values: [req.params.name],
  };

  const {rows} = await pool.query(query);
  console.log(rows);

  if (rows.length === 0) res.status(404).send();

  const matches = [];
  const getMatch = (word) => {
    const matcher = new RegExp(`\\b${word}\\b`, 'gi');
    console.log(matcher);
    return Object.keys(rows[0].sections)?.some((section) => {
      // console.log('Checking section', section);
      return rows[0].sections[section].words?.some((word) => {
        // console.log('Got word', word);
        if (matcher.test(word?.definition)) {
          console.log('Matched with ', word.definition);
          matches.push(word);
          return true;
        }
      });
    });
  };

  let matchedInput = getMatch(req.params.word);
  console.log('Matched ',matchedInput);
  if (!matchedInput) {
    // Get synonyms
    await fetch('https://lingua-robot.p.rapidapi.com/language/v1/entries/en/' + req.params.word, {
      method: 'GET',
      headers: new Headers({
        'Content-Type': 'application/json',
        'x-rapidapi-host': 'lingua-robot.p.rapidapi.com',
        'x-rapidapi-key': 'ceaa58f4c0mshe05958e0b12e019p1db70bjsn1b6cf821784d',
      }),
    })
      .then((res) => {
        if (!res.ok) throw res;
        return res.json();
      })
      .then((json) => {
        // console.log(json);
        const allSynonyms = json.entries?.flatMap((entry) => {
          return entry.lexemes?.flatMap((lexeme) => {
            return lexeme.synonymSets?.flatMap((set) => {
              return set.synonyms;
            });
          });
        });
        // console.log(allSynonyms);

        allSynonyms.forEach((synonym) => {
          if (!synonym) return;
          getMatch(synonym);
        });
      })
      .catch((error) => {
        console.error(error);
        res.status(500).send();
      });
  }

  // console.log(matches);

  if (matches.length === 1) return res.status(200).json(matches[0]);
  else if (matches.length > 1) return res.status(200).json(matches);
  else return res.status(404).send();
};
