const jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
const {Pool} = require('pg');

const secrets = {
  accessToken: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.` + 
    `eyJlbWFpbCI6ImFubmFAYm9va3MuY29tIiwicm9sZSI6ImFkbWluIiwiaWF0IjoxNjA2Mjc3MDAxLCJleHAiOjE2MDYyNzcwNjF9.` +
    `1nwY0lDMGrb7AUFFgSaYd4Q7Tzr-BjABclmoKZOqmr4`
};

const pool = new Pool({
  host: 'localhost',
  port: 5432,
  database: process.env.POSTGRES_DB,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
});

exports.authenticate = async (req, res) => {
  const { email, password } = req.body;
  const getUser = async () => {
    let command = `SELECT * FROM "user" WHERE email = $1`;
    let query = {
      text: command,
      values: [email],
    };
    const {rows} = await pool.query(query);
    // check email and password
    if (rows.length === 0 || rows[0].email !== email || !bcrypt.compareSync(password, rows[0].password)) return false;
    return {
      id: rows[0].id,
      email: rows[0].email,
      name: rows[0].name,
      workspace: rows[0].workspace,
      status: rows[0].status,
      statusText: rows[0].statusText,
    };
  };
  const user = await getUser();
  if (user) {
    const accessToken = jwt.sign(
      {email: user.email}, 
      secrets.accessToken, {
        expiresIn: '30m',
        algorithm: 'HS256'
      });
    res.status(200).json({
      id: user.id,
      name: user.name,
      workspace: user.workspace,
      status: user.status,
      statusText: user.statusText,
      accessToken: accessToken,
    });
  } else {
    res.status(401).send('Username or password incorrect');
  }
};

exports.check = (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (authHeader) {
    const token = authHeader.split(' ')[1];
    jwt.verify(token, secrets.accessToken, (err, user) => {
      if (err) {
        return res.sendStatus(403);
      }
      req.user = user;
      next();
    });
  } else {
    res.sendStatus(401);
  }
};

