const express = require('express');
const cors = require('cors');
const yaml = require('js-yaml');
const swaggerUi = require('swagger-ui-express');
const fs = require('fs');
const path = require('path');
const OpenApiValidator = require('express-openapi-validator');

const auth = require('./auth');
const planet = require('./planet');
const deity = require('./deity');
const language = require('./language');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

const apiSpec = path.join(__dirname, '../api/openapi.yaml');

const apidoc = yaml.load(fs.readFileSync(apiSpec, 'utf8'));
app.use('/v0/api-docs', swaggerUi.serve, swaggerUi.setup(apidoc));

app.post('/authenticate', auth.authenticate);

app.use(
  OpenApiValidator.middleware({
    apiSpec: apiSpec,
    validateRequests: true,
    validateResponses: true,
  }),
);

// Special admin routes
app.get('/v0/admin/planets', auth.check, planet.getAll);

// Planet routes
app.get('/v0/planets', planet.getAllDescriptions);
app.post('/v0/planets', auth.check, planet.post);
app.get('/v0/planets/:name', planet.getByName);
app.put('/v0/planets/:name', auth.check, planet.put);

// Deity routes
app.get('/v0/deities', deity.getAllDescriptions);
app.post('/v0/deities', auth.check, deity.post);
app.get('/v0/deities/:name', deity.getByName);
app.put('/v0/deities/:name', auth.check, deity.put);

// Language routes
app.get('/v0/languages', language.getAll);
app.post('/v0/languages', auth.check, language.post);
app.get('/v0/languages/:name', language.getByName);
app.put('/v0/languages/:name', auth.check, language.put);

// Lexicon routes
app.get('/v0/languages/:name/lexicon/sections', language.getLexSections)
app.get('/v0/languages/:name/lexicon/sections/:start', language.getLexSection)
app.get('/v0/languages/:name/lexicon/:word', language.getWord);
app.get('/v0/languages/:name/lexicon/translate/:word', language.translate);

app.use((err, req, res, next) => {
  res.status(err.status).json({
    message: err.message,
    errors: err.errors,
    status: err.status,
  });
});

module.exports = app;