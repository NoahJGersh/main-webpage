INSERT INTO planets ("name", "description", "information")
  VALUES ('Fjorunskar',
    '[
      {
        "type": "span",
        "children": "Fjorunskar is an important hotspot for divine activities. Born of the late Macrocosm &quot;Fjorun,&quot; its name literally translates to &quot;skull of Fjorun&quot; in the ancient language "
      },
      {
        "type": "Link",
        "props": {

          "to": "/vastestsea/languages/fjorunskara"
        },
        "children": {
          "type": "Glyph",
          "props": {

            "language": "fjorunskara"
          },
          "children": "fjorunskara"
        }
      },
      {
        "type": "span",
        "children": ". The world has been largely transformed by the direct connection of the planar forms of gods to the Sea itself, through portals known only as &quot;Ixax Convergence Points.&quot; The name refers to the plane of "
      },
      {
        "type": "Glyph",
        "props": {

          "language": "fjorunskara"
        },
        "children": "Ixax"
      },
      {
        "type": "span",
        "children": ", through which the gods of this world mesh and communicate."
      }
    ]',
    '[
      {
        "type": "Centered",
        "children": "This page under construction."
      }
    ]');

INSERT INTO planets ("name", "description", "information")
  VALUES ('Riuzhan',
    '[
      {
        "type": "span",
        "children": "Riuzhan is a world with an oddly skewed axis. &quot;Summer&quot; for a hemisphere means scorching heat as the entire hemisphere is subjected to its sun''s unrelenting heat. &quot;Winter&quot; means no sunlight there whatsoever. As the planet revolves around its star, the poles swap relative places, leaving only the equator as the most &quot;hospitable&quot; location on the planet."
      }
    ]',
    '[
      {
        "type": "Centered",
        "children": "This page under construction."
      }
    ]');

INSERT INTO planets ("name", "description", "information")
  VALUES ('Salmunth',
    '[
      {
        "type": "span",
        "children": "Salmunth is a desert planet, whose only sources of water lie in ever-expansive caves that snake beneath its surface. The surface itself is inhospitable to the majority of its life, who must seek shelter and safety in the caves below. A small population of incredibly large beasts roams the surface, seeking out any life unfortunate enough to trek nearby."
      }
    ]',
    '[
      {
        "type": "Centered",
        "children": "This page under construction."
      }
    ]');

INSERT INTO deities ("name", "description", "information")
  VALUES ('Macrocosms',
    '[
      {
        "type": "span",
        "children": "Macrocosms are immortal, fundamental constructs of the universe that constitute every primary aspect of it. They rarely commune directly with mortals, instead choosing to interact directly with other deities in the Sea."
      }
    ]',
    '[
      {
        "type": "Heading",
        "children": "The Fundament of the Sea"
      },
      {
        "type": "p",
        "children": "The Sea is merely the collection of a number of so-called Macrocosmic entities, whose very wills shape the currents and swells of their ocean of creation. Here you can find the collected information on each Macrocosm known to exist. This page will be periodically updated with more information about these mysterious figures as it becomes available."
      },
      {
        "type": "Heading",
        "props": {
          "id": "Borborunga",
          "intoc": "true"
        },
        "children": "Borborunga, the Transitive"
      },
      {
        "type": "p",
        "children": "Borborunga controls the transition of all things between states. This includes transitions between states of matter, transitions between positions, and especially the transition of the Sea between states in time."
      },
      {
        "type": "Heading",
        "props": {
          "id": "Fjorun",
          "intoc": "true"
        },
        "children": "Fjorun, the Prime World"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Fjorun is the progenitor of the Fjorunskar pantheon, and is the Macrocosm whose head constitutes the world of "
          },
          {
            "type": "Link",
            "props": {
              "to": "/vastestsea/planets/fjorunskar"
            },
            "children": "Fjorunskar"
          },
          {
            "type": "span",
            "children": " itself. Fjorun''s purpose is unknown, and it is assumed to be &quot;dead,&quot; given that its tripartite aspects of "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "Arystos"
          },
          {
            "type": "span",
            "children": " (Arystos), "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "demo''Aut"
          },
          {
            "type": "span",
            "children": " (Demo''aut), and "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "zettrus"
          },
          {
            "type": "span",
            "children": " (Zettrus) split in a cognitive civil war millenia ago."
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Fraforranfa",
          "intoc": "true"
        },
        "children": "Fraforranfa, the Gate"
      },
      {
        "type": "p",
        "children": "Fraforranfa is the guardian of the Sea, and maintains interplanar transportation. It is the last bastion of protection between extraplanar threats and the Sea itself."
      },
      {
        "type": "Heading",
        "props": {
          "id": "Gommrentrun",
          "intoc": "true"
        },
        "children": "Gommrentun, the Previous"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Commonly found along with "
          },
          {
            "type": "Link",
            "props": {
              "to": "#Borborunga"
            },
            "children": "Borborunga"
          },
          {
            "type": "span",
            "children": ", Gommrentun represents ever past state that the sea has existed as. It ranges from the very last moment to have lapsed to the very earliest moment to have begun."
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Hiitanavektz",
          "intoc": "true"
        },
        "children": "Hiitanavektz, the Shaking"
      },
      {
        "type": "p",
        "children": "Hiitanavektz is the ever-quaking, the solely-shaking, the immortally rumbling. Its sole purpose is to convey the concept of motion to the sea itself."
      },
      {
        "type": "Heading",
        "props": {
          "id": "Kolastor",
          "intoc": "true"
        },
        "children": "Kolastor, the Soulspeaker"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "All cultures in the Sea have an afterlife. Some of them are more... unique than others. What most mortal cultures in the sea "
          },
          {
            "type": "Italics",
            "children": "don''t"
          },
          {
            "type": "span",
            "children": " have, however, is the knowledge that "
          },
          {
            "type": "Italics",
            "children": "each"
          },
          {
            "type": "span",
            "children": " culture''s afterlife is, technically, correct. They are all overseen by Kolastor, who &quot;maintains&quot; the collection of all Sea-wide souls and &redacted; <DATA CORRUPTED>"
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Kwiggwanoth",
          "intoc": "true"
        },
        "children": "Kwiggwanoth, the Prescient"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Kwiggwanoth was born of "
          },
          {
            "type": "Link",
            "props": {
              "to": "#Borborunga"
            },
            "children": "Borborunga''s"
          },
          {
            "type": "span",
            "children": " need to keep track of potential future states of the Sea, and isolate the most likely in order ot preempt Sea-ending threats whenever possible."
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Kjorun",
          "intoc": "true"
        },
        "children": "Kjorun, the Twin World"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "Link",
            "props": {
              "to": "/ItWoTB"
            },
            "children": "In the Wake of Those Beyond"
          },
          {
            "type": "span",
            "children": " spoilers ahead! Click the box to reveal."
          },
          {
            "type": "br"
          },
          {
            "type": "br"
          },
          {
            "type": "Spoiler",
            "children": [
              {
                "type": "span",
                "children": "Kjorun was born of a branched replica of "
              },
              {
                "type": "Link",
                "props": {
                  "to": "/vastestsea/planets/fjorunskar"
                },
                "children": "Fjorunskar"
              },
              {
                "type": "span",
                "children": ", following "
              },
              {
                "type": "Link",
                "props": {
                  "to": "#Kolastor"
                },
                "children": "Kolastor''s"
              },
              {
                "type": "span",
                "children": " unusual attempt at destroying the planet and all of its life. Living separately from the Cycle itself, its course has naturally diverged from its prototype predecessor''s..."
              }
            ]
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Menvendendit",
          "intoc": "true"
        },
        "children": "Menvendendit, the Present"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Menvendendit, along with "
          },
          {
            "type": "Link",
            "props": {
              "to": "#Gommrentun"
            },
            "children": "Gommrentun"
          },
          {
            "type": "span",
            "children": " and "
          },
          {
            "type": "Link",
            "props": {
              "to": "#Kwiggwanoth"
            },
            "children": "Kwiggwanoth"
          },
          {
            "type": "span",
            "children": ", was created by Borborunga to maintain some aspect of the timeline. Menvendendit controls each passing moment, and carries it from Kwiggwanoth''s acceptable futures to Gommrentun''s collected pasts."
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Pproduwantog",
          "intoc": "true"
        },
        "children": "Pproduwantog, the Expansive"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "For all intents and purposes, Pproduwantog represents the Sea itself... or at the very least its breadth. Pproduwantog''s entire body constitutes the vacuous space between celestial object in the Sea. If the planets and stars of the Sea were driftwood, Pproduwantog would be the waves."
          },
          {
            "type": "br"
          },
          {
            "type": "br"
          },
          {
            "type": "span",
            "children": "Pproduwantog employs the Fjorunskari god "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "Jytem"
          },
          {
            "type": "span",
            "children": " (Jytem) as its &quot;Warpcurrents,&quot; which provide a means of faster-than-light travel to the beings sophisticated enough to decipher them. You can read more about it in "
          },
          {
            "type": "Link",
            "props": {
              "to": "/ItWoTB"
            },
            "children": "In the Wake of Those Beyond"
          },
          {
            "type": "span",
            "children": ", once "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "sena"
          },
          {
            "type": "span",
            "children": " (Sena)''s path is unlocked."
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Putoveereshek",
          "intoc": "true"
        },
        "children": "Putoveereshek, the Waveform"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Coupled with "
          },
          {
            "type": "Link",
            "props": {
              "to": "#Hiitanavektz"
            },
            "children": "Hiitanavektz''s"
          },
          {
            "type": "span",
            "children": " frequency, Putoveereshek represents the very waveforms by which sound and light are defined."
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Rendeltex",
          "intoc": "true"
        },
        "children": "Rendeltex, the Afflicted"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Rendeltex is an odd member of the Macrocosmic class. It seems to spread a constant disease that it uses to convert mortal beings to its unknown cause. Many planets have been torn asunder by its waves of zombified beings, but it was notably turned back by the gods of "
          },
          {
            "type": "Link",
            "props": {
              "to": "/vastestsea/planets/fjorunskar"
            },
            "children": "Fjorunskar"
          },
          {
            "type": "span",
            "children": ". You can read more about Rendeltex''s incursion in "
          },
          {
            "type": "Link",
            "props": {
              "to": "/ItWoTB"
            },
            "children": "In the Wake of Those Beyond"
          },
          {
            "type": "span",
            "children": ", once "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "sena"
          },
          {
            "type": "span",
            "children": " (Sena)''s path is unlocked."
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Salmunth",
          "intoc": "true"
        },
        "children": "Salmunth, the War-Torn"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Salmunth is much like "
          },
          {
            "type": "Link",
            "props": {
              "to": "#Fjorun"
            },
            "children": "Fjorun"
          },
          {
            "type": "span",
            "children": ", in that it is a planetary Macrocosm birthed for an unknown purpose. What "
          },
          {
            "type": "Italics",
            "children": "is"
          },
          {
            "type": "span",
            "children": " known is that its own pantheon of gods is fundamentally limited to three: Skyfather Deor-tehl, Rockmother Grahten, and Sandspawn Lynthesk."
          },
          {
            "type": "br"
          },
          {
            "type": "br"
          },
          {
            "type": "span",
            "children": "You can read more about Salmunth, the planet, "
          },
          {
            "type": "Link",
            "props": {
              "to": "/vastestsea/planets/salmunth"
            },
            "children": "here"
          },
          {
            "type": "span",
            "children": "."
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Tace",
          "intoc": "true"
        },
        "children": "Tace, the Storyteller"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Tace is the result of a grand vision from "
          },
          {
            "type": "Link",
            "props": {
              "to": "#Borborunga"
            },
            "children": "Borborunga"
          },
          {
            "type": "span",
            "children": " and "
          },
          {
            "type": "Link",
            "props": {
              "to": "#Xarrodddegnon"
            },
            "children": "Xarrodddegnon"
          },
          {
            "type": "span",
            "children": ": a Macrocosm to produce and enforce metanarratives to give the world purpose."
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Xarrodddegnon",
          "intoc": "true"
        },
        "children": "Xarrodddegnon, the Lingual"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "As Xarodddegnon speaks it, it is willed into existence. So too was language spoken into existence. Xarrodddegnon''s name is unpronounceable unless you have three mouths, given that it manifests itself as three enormous mouths which speak its own name in unison."
          },
          {
            "type": "br"
          },
          {
            "type": "br"
          },
          {
            "type": "span",
            "children": "One of these tongues is Lingua Franca, whose utterances guide the common tongue and its evolutions throughout the Sea. It is thanks to this dedicated mouth that most modern species are able to communicate with ease. All languages, shared or not, stem from this being''s ancestral utterances. Even "
          },
          {
            "type": "Link",
            "props": {
              "to": "/vastestsea/languages/fjorunskara"
            },
            "children": "Fjorunskara"
          },
          {
            "type": "span",
            "children": ", the language devised by "
          },
          {
            "type": "Link",
            "props": {
              "to": "#Fjorun"
            },
            "children": "Fjorun"
          },
          {
            "type": "span",
            "children": ", was ultimately influenced by Xarrodddegnon."
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Xiorvadestet",
          "intoc": "true"
        },
        "children": "Xiorvadestet, the Incalculable"
      },
      {
        "type": "p",
        "children": "Xiorvadestet maintains quantity and computation within the Sea itself, and ensures consistency among them."
      },
      {
        "type": "Heading",
        "props": {
          "id": "Yyyyltandus",
          "intoc": "true"
        },
        "children": "Yyyltandus, the Outer Reaches"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Yyyltandus is everything within the Sea that lies beyond the extent of "
          },
          {
            "type": "Link",
            "props": {
              "to": "#Pproduwantog"
            },
            "children": "Pproduwantog"
          },
          {
            "type": "span",
            "children": ". It is a generally poorly understood entity, given the lack of Warpcurrent access and far-flung distance from the Sea''s depths."
          }
        ]
      }
    ]');

INSERT INTO languages ("name_roman", "name_glyph", "description", "main_info", "inv_orth_info", "syntax_info", "lex_info")
  VALUES (
    'Fjorunskara', 'fjorunskara',
    '{
      "type": "p",
      "children": "Fjorunskara literally translates to the &quot;system of the skull of Fjorun&quot;. It is the language of the gods born of Fjorun (and of Fjorun itself)."
    }',
    '[
      {
        "type": "Heading",
        "children": "Gifted to Mortals"
      },
      {
        "type": "p",
        "children": "Fjorunskara is the endemic language of Fjorun and its progeny, but it was gifted from these deities to their mortal creations as well. The ancient Fjorunskari population were given the language, along with a writing system devised by the goddess Sena, so that they would be able to communicate with ease and with the gods."
      },
      {
        "type": "Heading",
        "children": "Inherent Abstraction"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "The gods of Fjorunskar are represented physically and conceptually in different ways. The physical form of a god, its Avatar, comes in three different varieties: Aspect, Ruling, and Titan. These physical forms have different purposes, but generally involve the god''s involvement in the affairs of the Sea itself. The conceptual form of a god, its Entity or Planar form, represents the very abstractly defined preconceptions of its domain and physicalizes them in the form of extradimensional locations and beings."
          },
          {
            "type": "br"
          },
          {
            "type": "br"
          },
          {
            "type": "span",
            "children": "The distinction between these two paradigms is also present in their language, where nouns are classified by their Abstraction. Nouns can be either physical or conceptual, with the exception of "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "zett"
          },
          {
            "type": "span",
            "children": " (Zett, stuff), and adjectives must agree with this classification. Some adjectives support both physical and conceptual nouns, others are only permitted for one of them. Nouns can also be physicalized or conceptualized with fitting suffixes. The "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "afa"
          },
          {
            "type": "span",
            "children": " (-afa) suffix swaps a noun''s classification from physical to conceptual; the "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "urok"
          },
          {
            "type": "span",
            "children": " (-urok) suffix performs the opposite conversion."
          }
        ]
      }
    ]',
    '[
      {
        "type": "p",
        "children": "Fjorunskara has a different inventory (set of phonemes) than English. It also has a totally different alphabet. These details are discussed here."
      },
      {
        "type": "Heading",
        "props": {
          "id": "Vowels",
          "intoc": "true"
        },
        "children": "Vowels"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Fjorunskara has seven primary vowels, each of which comes in a &quot;stemmed&quot; and &quot;free&quot; form. Stemmed vowels are found at the beginnings of words, immediately following a glottal stop ("
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "''"
          },
          {
            "type": "span",
            "children": "), or following a free vowel-j combo ("
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "ajA"
          },
          {
            "type": "span",
            "children": " is correct instead of "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "aja"
          },
          {
            "type": "span",
            "children": ", as in "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "hyJalix"
          },
          {
            "type": "span",
            "children": "). Free vowels are found everywhere else (that vowels can be found in the language) and are frequently attached to the preceding consonant; in these cases, the consonant should be interpreted as standing in for its stem. There is no phonemic difference between stemmed and free vowels, instead the difference is important orthographically. Each of the following vowels is listed with a standard romanization and then its Fjorunskara version, with stemmed Fjorunskara glyphs preceding the free ones."
          }
        ]
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "A",
          "character": "a",
          "hasUpperCase": true,
          "language": "fjorunskara",
          "transcription": "\\u00E4",
          "equivalent": "bar",
          "exampleRoman": "Jaros",
          "exampleGlyph": "Jaros"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "E",
          "character": "e",
          "hasUpperCase": true,
          "language": "fjorunskara",
          "transcription": "e",
          "equivalent": "bet",
          "exampleRoman": "Sena",
          "exampleGlyph": "sena"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "I",
          "character": "i",
          "hasUpperCase": true,
          "language": "fjorunskara",
          "transcription": "\\u026A",
          "equivalent": "bit",
          "exampleRoman": "Inkuitus",
          "exampleGlyph": "Inkuitus"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "O",
          "character": "o",
          "hasUpperCase": true,
          "language": "fjorunskara",
          "transcription": "o",
          "equivalent": "boat",
          "exampleRoman": "Rokund",
          "exampleGlyph": "Rokund"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "U",
          "character": "u",
          "hasUpperCase": true,
          "language": "fjorunskara",
          "transcription": "u",
          "equivalent": "boot",
          "exampleRoman": "Zettrus",
          "exampleGlyph": "zettrus"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "Y",
          "character": "y",
          "hasUpperCase": true,
          "language": "fjorunskara",
          "transcription": "a\\u026A",
          "equivalent": "bite",
          "exampleRoman": "Jytem",
          "exampleGlyph": "Jytem"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "Au",
          "character": "au",
          "hasUpperCase": true,
          "language": "fjorunskara",
          "transcription": "a\\u028A",
          "equivalent": "bout",
          "exampleRoman": "Demo''aut",
          "exampleGlyph": "demo''Aut"
        }
      },
      {
        "type": "Heading",
        "props": {
          "id": "Consonants",
          "intoc": "true"
        },
        "children": "Consonants"
      },
      {
        "type": "p",
        "children": "Fjorunskara has many consonants as well. Most of them are straightforward, with the exception of the rhotics, glottal stop (''), and &quot;j&quot;. Note that R/r and J/j should be stemmed and free according to the vowel rules for stemming."
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "B",
          "character": "b",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "b",
          "equivalent": "bar",
          "exampleRoman": "Bri''il",
          "exampleGlyph": "bri''Il"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "P",
          "character": "p",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "p",
          "equivalent": "par",
          "exampleRoman": "Preitex",
          "exampleGlyph": "preitex"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "D",
          "character": "d",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "d",
          "equivalent": "dart",
          "exampleRoman": "Drakk",
          "exampleGlyph": "drakk"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "T",
          "character": "t",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "t",
          "equivalent": "tar",
          "exampleRoman": "Trectris",
          "exampleGlyph": "trectris"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "G",
          "character": "g",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "g",
          "equivalent": "gaunt",
          "exampleRoman": "Gryvelt",
          "exampleGlyph": "gryvelt"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "K",
          "character": "k",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "k",
          "equivalent": "car",
          "exampleRoman": "Krestos",
          "exampleGlyph": "krestos"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "C",
          "character": "c",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "k\\u02B0",
          "equivalent": "car",
          "exampleRoman": "Criff",
          "exampleGlyph": "criff"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "Z",
          "character": "z",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "z",
          "equivalent": "zeal",
          "exampleRoman": "Zettrus",
          "exampleGlyph": "zettrus"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "S",
          "character": "s",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "s",
          "equivalent": "seal",
          "exampleRoman": "Sraltil",
          "exampleGlyph": "sraltil"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "Sh",
          "character": "sh",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "\\u0283",
          "equivalent": "shade",
          "exampleRoman": "Shenbur",
          "exampleGlyph": "shenbur"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "L",
          "character": "l",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "l",
          "equivalent": "love",
          "exampleRoman": "Lyg (noun: home)",
          "exampleGlyph": "lyg"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "M",
          "character": "m",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "m",
          "equivalent": "meal",
          "exampleRoman": "Met (noun: month)",
          "exampleGlyph": "met"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "N",
          "character": "n",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "n",
          "equivalent": "near",
          "exampleRoman": "Njarvy (noun: cat)",
          "exampleGlyph": "njarvy"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "V",
          "character": "v",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "v",
          "equivalent": "video",
          "exampleRoman": "Vido (adj: high quality)",
          "exampleGlyph": "vido"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "F",
          "character": "f",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "f",
          "equivalent": "feel",
          "exampleRoman": "Fumau (adj: loud)",
          "exampleGlyph": "fumau"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "X",
          "character": "x",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "ks",
          "equivalent": "books",
          "exampleRoman": "Ix (number: one)",
          "exampleGlyph": "Ix"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "H",
          "character": "h",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "h",
          "equivalent": "head",
          "exampleRoman": "Hyjalix",
          "exampleGlyph": "hyjalix"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "J",
          "character": "j",
          "hasUpperCase": true,
          "language": "fjorunskara",
          "transcription": "j",
          "equivalent": "year",
          "exampleRoman": "J''arat",
          "exampleGlyph": "J''Arat"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "R",
          "character": "r",
          "hasUpperCase": true,
          "language": "fjorunskara",
          "transcription": "\\u0279/ or /\\u027E",
          "equivalent": "real&quot; or the Spanish &quot;oro",
          "exampleRoman": "Ruvak",
          "exampleGlyph": "Ruvak"
        }
      },
      {
        "type": "Grapheme",
        "props": {
          "id": "Glottal Stop",
          "character": "''",
          "hasUpperCase": false,
          "language": "fjorunskara",
          "transcription": "\\u0294",
          "equivalent": "uh-oh",
          "exampleRoman": "Cri''a (noun: ice)",
          "exampleGlyph": "cri''A"
        }
      },
      {
        "type": "Heading",
        "props": {
          "id": "Phonetic Notes",
          "intoc": "true"
        },
        "children": "Phonetic Notes"
      },
      {
        "type": "Subheading",
        "props": {
          "id": "Rhotic Rules"
        },
        "children": "Rhotic Rules"
      },
      {
        "type": "p",
        "children": "The Fjorunskara rhotic is pronounced differently based on context. If it follows a vowel, glottal stop, or voiced consonant, or if it is at the beginning of a word, it should be tapped and pronounced like the Spanish r. It may additionally only be flapped when followed by a vowel or at the end of a word. Otherwise, it should be an approximant and pronounced like the English r."
      },
      {
        "type": "Subheading",
        "props": {
          "id": "Long Vowels"
        },
        "children": "Long Vowels"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "A long vowel may occur when two identical vowels are adjacent to each other, such as in the word Jeeshaung (noun: thin or runny liquid) - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "Jeeshaung"
          },
          {
            "type": "span",
            "children": ". Long vowels are held for longer than their short counterparts; there is no change in pronunciation."
          }
        ]
      },
      {
        "type": "Subheading",
        "props": {
          "id": "Geminate Consonants"
        },
        "children": "Geminate Consonants"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "A geminate consonant occurs just like a long vowel, when two identical consonants are butted up against each other. For example, the god of creation Zettrus - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "zettrus"
          },
          {
            "type": "span",
            "children": " has a geminate /t\\u02D0/ - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "tt"
          },
          {
            "type": "span",
            "children": ". Geminate consonants in that they are held longer than their standard counterparts."
          }
        ]
      },
      {
        "type": "Subheading",
        "props": {
          "id": "Hj Exception"
        },
        "children": "The Hj Exception"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "When &quot;hj&quot; - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "hj"
          },
          {
            "type": "span",
            "children": " is found in a word, the "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "h"
          },
          {
            "type": "span",
            "children": " is pronounced as /x/, which is more of a hissing sound than the standard /h/. An example of this is in the name Hjultus - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "hjultus"
          },
          {
            "type": "span",
            "children": "."
          }
        ]
      },
      {
        "type": "Subheading",
        "props": {
          "id": "Ng Exception"
        },
        "children": "The Ng Exception"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "When &quot;ng&quot; - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "ng"
          },
          {
            "type": "span",
            "children": " is found in a word, the cluster is pronounced as /\\u014B/, as in &quot;running.&quot;"
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Typing",
          "intoc": "true"
        },
        "children": "Romanization and Typing"
      },
      {
        "type": "p",
        "children": "For the purposes of actual written communication with the language, handwriting works just fine with the symbols. On the other hand, however, what if one wanted to communicate digitally? Unicode certainly doesn''t support Fjorunskara out of the box. Here are the official ways to write in the language using a computer."
      },
      {
        "type": "Subheading",
        "props": {
          "id": "Romanization"
        },
        "children": "Romanization"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "The easiest way to type in Fjorunskara is using the Latin alphabet, with English norms. Letters should be typed as they appear above, with capitalization only occurring for the first letter of a sentence, or the first letters of proper nouns. Punctuation is acceptable, with commas separating clauses, and the traditional {. ? !} according to the tone of a sentence."
          },
          {
            "type": "br",
            "props": {

            }
          },
          {
            "type": "br",
            "props": {

            }
          },
          {
            "type": "span",
            "children": "As an example:"
          },
          {
            "type": "br",
            "props": {

            }
          },
          {
            "type": "span",
            "children": "Borcroeshad ko''ylja ylakinjarvy kivycroenge''ef, kivycroengeskad fe''i gommva."
          },
          {
            "type": "br",
            "props": {

            }
          },
          {
            "type": "span",
            "children": "The kittens drank all their milk, leaving nothing behind."
          }
        ]
      },
      {
        "type": "Subheading",
        "props": {
          "id": "Font"
        },
        "children": "Custom Font"
      },
      {
        "type": "p",
        "children": "The best way to type in Fjorunskara is using the custom font I''ve developed for the language. It comes in a TrueTypeFont file, which is hosted on my GitHub and loaded into the webpage for you to see! This is the correct way to type Fjorunskara when custom fonts are available. The font has some odd methods employed to ensure congruity with the actual writing system, though."
      },
      {
        "type": "p",
        "children": [
          {
            "type": "Bold",
            "children": "Ligatures Required."
          },
          {
            "type": "span",
            "children": " The font makes use of ligatures for things as simple as beautifying some of the consonant-vowel combinations (where kerning is insufficient), but also for things as crucial as the Au, au, and sh graphemes. If the application you''re using doesn''t support font ligatures, you will need to use the Romanization method above."
          }
        ]
      },
      {
        "type": "p",
        "children": [
          {
            "type": "Bold",
            "children": "Capital Punishment."
          },
          {
            "type": "span",
            "children": " No caps allowed!!! Well, sort of. Capital letters are reserved for the stemmed versions of letters. For stemmed vowels, J, and R, use uppercase. For free vowels, j, and r, use lowercase. Note that AU and Au are both acceptable typings for their stemmed grapheme. Because all other consonants only have a single form, only lowercase letters will work for them."
          }
        ]
      },
      {
        "type": "p",
        "children": [
          {
            "type": "Bold",
            "children": "Timid, Not Bold."
          },
          {
            "type": "span",
            "children": " The intricate, thin nature of Fjorunskara graphemes means that they''re already hard enough to read at normal font size. Please increase the font size of legibility if you can (one of my biggest regrets in making the font was making it so small). For context, the website uses around 140% text size for Fjorunskara text. Additionally, bold weight really messes with the details and legibility. Try to keep it at regular font weighting for optimal legibility and accuracy."
          }
        ]
      },
      {
        "type": "p",
        "children": [
          {
            "type": "Bold",
            "children": "Gimme Gimme."
          },
          {
            "type": "span",
            "children": " You can get the font at my GitHub repository "
          },
          {
            "type": "Link",
            "props": {

              "to": "https://github.com/NoahJGersh/Fjorunskara"
            },
            "children": "here"
          },
          {
            "type": "span",
            "children": ". If you have any problems with the font (&quot;hey, the kerning''s borked&quot;, &quot;this thing is really hard to read pls change it&quot;) feel free to make an issue on the repo and I''ll get to it when I have the time!"
          }
        ]
      }
    ]',
    '[
      {
        "type": "p",
        "children": "Fjorunskara sentences and morphology differ greatly from English. Here, you can find all of the phonotactic, morphological, and syntactical rules for the language."
      },
      {
        "type": "Heading",
        "props": {
          "id": "Phonotactics",
          "intoc": "true"
        },
        "children": "Phonotactics"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "The following table lists the permitted phonotactic structures for each syllable in a Fjorunskara word. Within are the following sets of sounds:"
          },
          {
            "type": "br",
            "props": {

            }
          },
          {
            "type": "br",
            "props": {

            }
          },
          {
            "type": "PhonoSet",
            "props": {

              "setname": "C",
              "setdesc": "all consonants, excluding sounds from G and ''"
            }
          },
          {
            "type": "PhonoSet",
            "props": {

              "setname": "V",
              "setdesc": "all vowels"
            }
          },
          {
            "type": "PhonoSet",
            "props": {

              "setname": "R",
              "setdesc": "rhotic"
            }
          },
          {
            "type": "PhonoSet",
            "props": {

              "setname": "G",
              "setdesc": "{R, L, J}"
            }
          },
          {
            "type": "PhonoSet",
            "props": {

              "setname": "L",
              "setdesc": "/l/"
            }
          },
          {
            "type": "PhonoSet",
            "props": {

              "setname": "S",
              "setdesc": "/s/"
            }
          },
          {
            "type": "PhonoSet",
            "props": {

              "setname": "J",
              "setdesc": "/j/"
            }
          },
          {
            "type": "PhonoSet",
            "props": {

              "setname": "''",
              "setdesc": "/\\u0294/"
            }
          },
          {
            "type": "br",
            "props": {

            }
          }
        ]
      },
      {
        "type": "p",
        "children": "Treat anything within parentheses as optional. A (...) in the onset means any accepted onset, and a (...) in the coda means any accepted coda."
      },
      {
        "type": "PhonoTable",
        "children": [
          {
            "type": "PhonoRule",
            "props": {

              "struct": "V",
              "desc": "Vowels may stand alone."
            }
          },
          {
            "type": "PhonoRule",
            "props": {

              "struct": "J''",
              "desc": "/j/ may be followed by a glottal stop to constitute a single syllable."
            }
          },
          {
            "type": "PhonoRule",
            "props": {

              "struct": "(C)(G)V(...)",
              "desc": "The onset may contain a consonant, a sound from set G, or both (in that order)."
            }
          },
          {
            "type": "PhonoRule",
            "props": {

              "struct": "(S)CV(...)",
              "desc": "If the onset contains a consonant, it may be preceded by /s/, but only if there is no sound from set G in the onset. This overrules the previous syllable structure."
            }
          },
          {
            "type": "PhonoRule",
            "props": {

              "struct": "(S)RV(...)",
              "desc": "If the onset contains a rhotic, it may be preceded by /s/, but only if there is no other sound in the onset."
            }
          },
          {
            "type": "PhonoRule",
            "props": {

              "struct": "(...)V(R)(L)",
              "desc": "The coda may contain a rhotic, /l/, or both (in that order), when a sound from set C is not present."
            }
          },
          {
            "type": "PhonoRule",
            "props": {

              "struct": "(...)V(L)C",
              "desc": "If the coda contains a sound from set C, /l/ may be inserted before it."
            }
          },
          {
            "type": "PhonoRule",
            "props": {

              "struct": "(...)V''",
              "desc": "The coda may contain a glottal stop, but only if it stands alone."
            }
          }
        ]
      },
      {
        "type": "ExtraPhonoRules",
        "children": [
          {
            "type": "span",
            "children": [
              {
                "type": "span",
                "children": "If a word ends in y "
              },
              {
                "type": "Glyph",
                "props": {

                  "language": "fjorunskara"
                },
                "children": "y"
              },
              {
                "type": "span",
                "children": ", and a second word that starts in hy "
              },
              {
                "type": "Glyph",
                "props": {

                  "language": "fjorunskara"
                },
                "children": "hy"
              },
              {
                "type": "span",
                "children": " would be compounded on its end, remove the intial y."
              }
            ]
          },
          {
            "type": "span",
            "children": "The glottal stop may not be used as the first or last phoneme in a word."
          },
          {
            "type": "span",
            "children": [
              {
                "type": "span",
                "children": "X - "
              },
              {
                "type": "Glyph",
                "props": {

                  "language": "fjorunskara"
                },
                "children": "x"
              },
              {
                "type": "span",
                "children": " may only be used in the coda."
              }
            ]
          },
          {
            "type": "span",
            "children": [
              {
                "type": "span",
                "children": "J'' - "
              },
              {
                "type": "Glyph",
                "props": {

                  "language": "fjorunskara"
                },
                "children": "J''"
              },
              {
                "type": "span",
                "children": " may only exist at the start of a word."
              }
            ]
          },
          {
            "type": "span",
            "children": "Up to two vowels can stand next to each other uninterrupted; any more must be separated by a glottal stop for every two vowels in sequence."
          },
          {
            "type": "span",
            "children": [
              {
                "type": "span",
                "children": "Some double-vowels should be separated by a glottal stop anyways. These combinations are a''i "
              },
              {
                "type": "Glyph",
                "props": {

                  "language": "fjorunskara"
                },
                "children": "A''I"
              },
              {
                "type": "span",
                "children": ", a''y "
              },
              {
                "type": "Glyph",
                "props": {

                  "language": "fjorunskara"
                },
                "children": "A''Y"
              },
              {
                "type": "span",
                "children": ", a''au "
              },
              {
                "type": "Glyph",
                "props": {

                  "language": "fjorunskara"
                },
                "children": "A''AU"
              },
              {
                "type": "span",
                "children": ", e''y "
              },
              {
                "type": "Glyph",
                "props": {

                  "language": "fjorunskara"
                },
                "children": "E''Y"
              },
              {
                "type": "span",
                "children": ", and e''au "
              },
              {
                "type": "Glyph",
                "props": {

                  "language": "fjorunskara"
                },
                "children": "E''AU"
              },
              {
                "type": "span",
                "children": "."
              }
            ]
          },
          {
            "type": "span",
            "children": "/i/ followed by a different vowel gets replaced with /j/, unless /i/ is preceded by a sound from G. In that case, /i/ and the other vowel are separated by a glottal stop."
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Morphology",
          "intoc": "true"
        },
        "children": "Morphology"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Fjorunskara is an agglutinative language. The language builds what it can from &quot;divine&quot; root words. That is to say, many of the root words in Fjorunskara are truncated from the names of the gods of Fjorunskar. As an example, the word &quot;hjult&quot; - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "hjult"
          },
          {
            "type": "span",
            "children": ", which is a noun that means &quot;tree,&quot; is truncated from the god &quot;Hjultus&quot; - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "hjultus"
          },
          {
            "type": "span",
            "props": {

              "language": "fjorunskara"
            },
            "children": ", whose domain revolves exclusively around trees."
          }
        ]
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "All root words are nouns, without exception. Bound morphemes allow for the conversion of nouns to other parts of speech, which may have interesting implications. Using our earlier example of &quot;hjult&quot; - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "hjult"
          },
          {
            "type": "span",
            "children": ", we can add the bound suffix &quot;-o&quot; to convert from a noun to an adjective. The resulting adjective &quot;hjulto&quot; - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "hjulto"
          },
          {
            "type": "span",
            "children": " does not mean &quot;tree-like.&quot; Instead, it translates to &quot;tall.&quot;"
          }
        ]
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Other bound morphemes modify the meaning of a word, rather than simply the part of speech. Examples of this include the suffix &quot;-a&quot;, which modifies a noun to indicate some system of or involving the root. The &quot;system of or involving a tree,&quot; or the &quot;hjulta&quot; - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "hjulta"
          },
          {
            "type": "span",
            "children": ", is more directly translated at &quot;forest.&quot;"
          }
        ]
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Compound words are also highly prevalent, but they typically tend to be between nouns. Mixed parts-of-speech for compound words are technically allowed, where a full noun compound might not convey the correct meaning, but they are generally frowned upon. If we combine &quot;hjult&quot; - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "hjult"
          },
          {
            "type": "span",
            "children": " with the word &quot;ji''an&quot; - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "Ji''Ana"
          },
          {
            "type": "span",
            "children": " (which is a noun meaning &quot;hand&quot;), we get &quot;hjultji''ana&quot; - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "hjultji''Ana"
          },
          {
            "type": "span",
            "children": ", which translates to arm! Literally, it is the &quot;tree''s hand,&quot; which is a &quot;branch&quot;, which is then used for all sorts of arm-like objects, such as tentacles."
          }
        ]
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "The last thing to be aware of is the existence of &quot;titular morphemes.&quot; Ancient Fjorunskari people, and the gods they worship, place a heavy importance on titles granted for deeds and qualities. In fact, some of the gods'' names are titles, such as J''arat - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "J''Arat"
          },
          {
            "type": "span",
            "children": ", which literally translates to &quot;Bringer of Storm.&quot; Titular prefixes indicate the titlebearer''s action upon the root word, and the titular suffixes indicate the titlebearer''s quality derived from the root word."
          }
        ]
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "In the case where the rot word is a title itself, meaning should be derived from context. Where the title Aut''arin - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "Aut''Arin"
          },
          {
            "type": "span",
            "children": " translates to &quot;Shadow of Death,&quot; the title Jyga''aut''arin - "
          },
          {
            "type": "Glyph",
            "props": {

              "language": "fjorunskara"
            },
            "children": "Jyga''Aut''Arin"
          },
          {
            "type": "span",
            "children": " translates to &quot;Rider of the Shadow of Death.&quot; Another possible translation would be &quot;Shadow of the Rider of Death.&quot;"
          }
        ]
      },
      {
        "type": "p",
        "children": "The last main note on morphology is that it does not impact the stress of a word. The first root syllable of a word is always stressed, as the gods wish their names to be emphasized when spoken, even while fragmented."
      },
      {
        "type": "p",
        "children": "All nouns use the following morphological structure when affixing other morphemes to them:"
      },
      {
        "type": "p",
        "children": "(Title Prefix) - (Prefixes) - (Root Structure) - (Suffixes) - (Title Suffix) - Possessive"
      },
      {
        "type": "p",
        "children": "Where the root structure is any noun, recursively defined down to the root words as the base case."
      },
      {
        "type": "p",
        "children": "Below, you can find a list of all the bound morphemes, what they can be attached to, and what they do. Prefixes have a hyphen at the end and suffixes have a hyphen at the beginning. There are no infixes or circumfixes in the language. Bound morphemes may distinguish between conceptual or physical noun roots; these are marked as (Conc.) or (Phys.), respectively. If not specified, they work for either kind. Note that the plurality of possessive morphemes is based on the owner''s plurality, not the noun''s."
      },
      {
        "type": "MorphemeTable",
        "props": {
          "id": "Bound Morphemes",
          "intoc": "true"
        },
        "children": [
          {
            "type": "Morpheme",
            "props": {

              "roman": "-a",
              "glyph": "-a",
              "language": "fjorunskara",
              "type": "N,V->N",
              "desc": "System of or involving root (must not end in /a/)."
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-''a",
              "glyph": "-''A",
              "language": "fjorunskara",
              "type": "N",
              "desc": "System of or involving root (must end in /a/)"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-a''af",
              "glyph": "-a''Af",
              "language": "fjorunskara",
              "type": "N",
              "desc": "Third person (plural) possessive"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-af",
              "glyph": "-af",
              "language": "fjorunskara",
              "type": "N",
              "desc": "Third person (singular) possessive"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-afa",
              "glyph": "-afa",
              "language": "fjorunskara",
              "type": "N (Phys.)",
              "desc": "Convert root to conceptual"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-''arin",
              "glyph": "-''Arin",
              "language": "fjorunskara",
              "type": "N (Titular)",
              "desc": "Shadow of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-ax",
              "glyph": "-ax",
              "language": "fjorunskara",
              "type": "N (Conc.)",
              "desc": "Plural of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Bor-",
              "glyph": "bor-",
              "language": "fjorunskara",
              "type": "N",
              "desc": "Transition of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-bun",
              "glyph": "-bun",
              "language": "fjorunskara",
              "type": "N (Conc.)",
              "desc": "&quot;Much&quot; magnitude of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-d",
              "glyph": "-d",
              "language": "fjorunskara",
              "type": "Adj,N->V",
              "desc": "Convert adjective or noun (must not end in /e/) to verb"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Demo''-",
              "glyph": "demo''-",
              "language": "fjorunskara",
              "type": "N (Conc., Titular)",
              "desc": "Bringer of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-dul",
              "glyph": "-dul",
              "language": "fjorunskara",
              "type": "Any->Adj",
              "desc": "Like root in quality"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-''e",
              "glyph": "-''E",
              "language": "fjorunskara",
              "type": "Adj,N->Verb",
              "desc": "Convert adjective or noun (ending in /e/) to verb"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-ead",
              "glyph": "-ead",
              "language": "fjorunskara",
              "type": "N",
              "desc": "Second person (plural) possessive"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-ed",
              "glyph": "-ed",
              "language": "fjorunskara",
              "type": "N",
              "desc": "Second person (singular) possessive"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-eng",
              "glyph": "-eng",
              "language": "fjorunskara",
              "type": "N",
              "desc": "All, total of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-esh",
              "glyph": "-esh",
              "language": "fjorunskara",
              "type": "Adj->N",
              "desc": "Having the quality of the root; the exemplar"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-ezet",
              "glyph": "-ezet",
              "language": "fjorunskara",
              "type": "N (Titular)",
              "desc": "Denizen of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Hju-",
              "glyph": "hju-",
              "language": "fjorunskara",
              "type": "N (Phys.)",
              "desc": "Something &quot;above&quot; root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-il",
              "glyph": "-il",
              "language": "fjorunskara",
              "type": "N (Titular)",
              "desc": "Island of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-''it",
              "glyph": "-''It",
              "language": "fjorunskara",
              "type": "N (Conc., Titular)",
              "desc": "Symbol of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "J''-",
              "glyph": "J''-",
              "language": "fjorunskara",
              "type": "N (Phys., Titular)",
              "desc": "Bringer of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Je-",
              "glyph": "Je-",
              "language": "fjorunskara",
              "type": "Any",
              "desc": "Negation of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-jul",
              "glyph": "-jul",
              "language": "fjorunskara",
              "type": "V->Adj",
              "desc": "Convert verb to adjective"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Jyga-",
              "glyph": "Jyga-",
              "language": "fjorunskara",
              "type": "N (Phys., Titular)",
              "desc": "Rider of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Kata-",
              "glyph": "kata-",
              "language": "fjorunskara",
              "type": "N",
              "desc": "Good"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Ki-",
              "glyph": "ki-",
              "language": "fjorunskara",
              "type": "N",
              "desc": "Diminutive, comparative less"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Kiva-",
              "glyph": "kiva-",
              "language": "fjorunskara",
              "type": "N",
              "desc": "Diminutive, comparative least"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Ko-",
              "glyph": "ko-",
              "language": "fjorunskara",
              "type": "N",
              "desc": "Other than root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-lunt",
              "glyph": "-lunt",
              "language": "fjorunskara",
              "type": "N (Titular)",
              "desc": "Land of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-le",
              "glyph": "-le",
              "language": "fjorunskara",
              "type": "V->N (Phys.)",
              "desc": "Tool for root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-met",
              "glyph": "-met",
              "language": "fjorunskara",
              "type": "N (Titular)",
              "desc": "Month of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-ne",
              "glyph": "-ne",
              "language": "fjorunskara",
              "type": "V->N (Phys.)",
              "desc": "One who performs root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-o",
              "glyph": "-o",
              "language": "fjorunskara",
              "type": "N->Adj",
              "desc": "Convert noun to adjective. If the noun ends in &quot;a&quot;, replace the &quot;a&quot; with &quot;au&quot; instead."
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-on",
              "glyph": "-on",
              "language": "fjorunskara",
              "type": "N",
              "desc": "First person (singular) possessive"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Ot-",
              "glyph": "Ot-",
              "language": "fjorunskara",
              "type": "N (Phys.)",
              "desc": "Something &quot;below&quot; root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-o''on",
              "glyph": "-o''On",
              "language": "fjorunskara",
              "type": "N",
              "desc": "First person (plural) possessive"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Shi-",
              "glyph": "shi-",
              "language": "fjorunskara",
              "type": "N",
              "desc": "Same as root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Tro-",
              "glyph": "tro-",
              "language": "fjorunskara",
              "type": "N",
              "desc": "Bad"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-tura",
              "glyph": "-tura",
              "language": "fjorunskara",
              "type": "N (Titular)",
              "desc": "Born for/of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-urok",
              "glyph": "-urok",
              "language": "fjorunskara",
              "type": "N (Conc.)",
              "desc": "Convert root to physical"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-va",
              "glyph": "-va",
              "language": "fjorunskara",
              "type": "Adj",
              "desc": "Comparative very/more"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-vara",
              "glyph": "-vara",
              "language": "fjorunskara",
              "type": "Adj",
              "desc": "Comparative most"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "-vri",
              "glyph": "-vri",
              "language": "fjorunskara",
              "type": "N (Titular)",
              "desc": "Land of root"
            }
          },
          {
            "type": "Morpheme",
            "props": {

              "roman": "Yla-",
              "glyph": "Yla-",
              "language": "fjorunskara",
              "type": "N (Phys.)",
              "desc": "Plural of root"
            }
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Syntax",
          "intoc": "true"
        },
        "children": "Syntax"
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "While English follows Subject Verb Object (SVO) word order, Fjorunskara departs from that. This language employs Verb Subject Object (VSO) word order. As an example, the sentence &quot;Le''ad fe''i jeshuro&quot; - "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara",
              "sentence": "true"
            },
            "children": "le''Ad fe''I Jeshuro"
          },
          {
            "type": "span",
            "children": " translates to &quot;They are not honest.&quot; In this case, though, &quot;le''ad&quot; - "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "le''Ad"
          },
          {
            "type": "span",
            "children": " translates to &quot;to be,&quot; &quot;fe''i&quot; - "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "fe''I"
          },
          {
            "type": "span",
            "children": " translates to &quot;they/them,&quot; and &quot;jeshuro&quot; - "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "Jeshuro"
          },
          {
            "type": "span",
            "children": " translates to &quot;dishonest&quot;. Translated word by word, and without fixing the order to be like English, it reads as &quot;Be they dishonest.&quot;"
          }
        ]
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Furthermore, adjectival and adverbial phrases belong after the noun phrase they describe, as in Spanish. &quot;Red storm,&quot; translated in Fjorunskara, is &quot;arat ashuro&quot; - "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara",
              "sentence": "true"
            },
            "children": "Arat Ashuro"
          },
          {
            "type": "span",
            "children": ", which translates word-for-word to &quot;storm red.&quot Note that there is no distinction between adjectives and adverbs in the language; use the same word to modify noun phrases and verb phrases alike."
          }
        ]
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "In the case of possessives, they are sometimes too vague to properly explain who is being referenced. If one wanted to express ownership of some single other person, unrelated to the speaker or the listener, the third-person singular possessive "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "-af"
          },
          {
            "type": "span",
            "children": " (-af) could mean anybody. In these cases, the relevant owner should be expressed as an adjectival phrase of the owned noun. In the English phrase &quot;Lexicon of Fjorunskara,&quot; or equivalently &quot;Fjorunskara''s Lexicon,&quot; Lexicon is the owned noun, and Fjorunskara represents a singular third-person possessor. Translated in Fjorunskara correctly, this would be "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara",
              "sentence": "true"
            },
            "children": "Omeadjulaueshaf fjorunskara"
          },
          {
            "type": "span",
            "children": " (Omeadjulaueshaf fjorunskara), where Fjorunskara is an unmodified adjectival phrase of Omeadjulaueshaf."
          }
        ]
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "Any adjectives describing the possessee itself should come after any possessor clarifications. Using the above example: "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara",
              "sentence": "true"
            },
            "children": "Omeadjulaueshaf fjorunskara Ygo"
          },
          {
            "type": "span",
            "children": " (Omeadjulaueshaf fjorunskara ygo) means &quot;Fjorunskara''s Big Lexicon,&quot; with Fjorunskara standing in for the possessor clarification, and Ygo describing the actual Lexicon."
          }
        ]
      },
      {
        "type": "p",
        "children": "Watch out for double object verbs! Most verbs have subtly different meanings when used in this way or not. To enforce the transitive meaning, the direct object should be prefixed to the verb itself. Do not use articles for nouns prefixed to verbs in this way; the article should be apparent from context. On the other hand, adjectives describing the prefixed noun should take the form of an adjectival phrase between the verb and the subject. Reflexive and single-object verbs operate as normal."
      },
      {
        "type": "p",
        "children": [
          {
            "type": "span",
            "children": "E.g. "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "fa"
          },
          {
            "type": "span",
            "children": " (Fa)."
          },
          {
            "type": "br"
          },
          {
            "type": "span",
            "children": "No object: "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara",
              "sentence": "true"
            },
            "children": "fa nu-"
          },
          {
            "type": "span",
            "children": " Fa nu. I give to them (implied object)."
          },
          {
            "type": "br"
          },
          {
            "type": "span",
            "children": "Single object: "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara",
              "sentence": "true"
            },
            "children": "fa nu dyyloton-"
          },
          {
            "type": "span",
            "children": " Fa nu dyyloton. I give to my friend."
          },
          {
            "type": "br"
          },
          {
            "type": "span",
            "children": "Double object: "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara",
              "sentence": "true"
            },
            "children": "Jarfa nu dyyloton-"
          },
          {
            "type": "span",
            "children": " Jarfa nu dyyloton. I give my friend (a/the) gem."
          },
          {
            "type": "br"
          },
          {
            "type": "span",
            "children": "Double object (adj): "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara",
              "sentence": "true"
            },
            "children": "Jarfa Ashuro nu dyyloton-"
          },
          {
            "type": "span",
            "children": " Jarfa ashuro nu dyyloton. I give my friend (a/the) red gem."
          }
        ]
      },
      {
        "type": "p",
        "props": {
          "id": "Tense",
          "intoc": "true"
        },
        "children": [
          {
            "type": "span",
            "children": "Another big difference is in tense. English verbs differ based on tense, like &quot;run&quot; in the present tense or &quot;ran&quot; in the past tense. Verbs in Fjorunskara, however, do not change for tense! Instead, tense is always assumed to be in the present, unless specified otherwise with specific words near the end of a clause. If all clauses in a sentence share the same tense, use the "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "-va"
          },
          {
            "type": "span",
            "children": " (-va) suffix to indicate that at the very end of the sentence. Otherwise, use each corresponding word at the end of each clause."
          }
        ]
      },
      {
        "type": "MarkerTable",
        "props": {
          "language": "fjorunskara",
          "name": "Tense"
        },
        "children": [
          {
            "type": "Marker",
            "props": {
              "roman": "(None)",
              "glyph": "",
              "meaning": "Simple Present"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Menven",
              "glyph": "menven",
              "meaning": "Present Continuous"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Menrunga",
              "glyph": "menrunga",
              "meaning": "Present Perfect"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Menvenrunga",
              "glyph": "menvenrunga",
              "meaning": "Present Perfect Continuous"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Gomm",
              "glyph": "gomm",
              "meaning": "Simple Past"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Gommren",
              "glyph": "gommren",
              "meaning": "Past Continuous"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Gommrunga",
              "glyph": "gommrunga",
              "meaning": "Past Perfect"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Gommrenrunga",
              "glyph": "gommrenrunga",
              "meaning": "Past Perfect Continuous"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Vigg",
              "glyph": "vigg",
              "meaning": "Simple Future"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Viggvan",
              "glyph": "viggvan",
              "meaning": "Future Continuous"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Viggrunga",
              "glyph": "viggrunga",
              "meaning": "Future Perfect"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Viggvanrunga",
              "glyph": "viggvanrunga",
              "meaning": "Future Perfect Continuous"
            }
          }
        ]
      },
      {
        "type": "p",
        "props": {
          "id": "Tone",
          "intoc": "true"
        },
        "children": [
          {
            "type": "span",
            "children": "Yet another difference is in the tone or emotion of a phrase. Punctuation in Fjorunskara is virtually nonexistent, with a simple, thin dash to represent the end of a sentence ("
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "-"
          },
          {
            "type": "span",
            "children": "). To denote questions, exclamations, mandates, or more, use a corresponding tone marker at the end of a sentence. When omitted, tone is assumed to be factual and calm when written. If all clauses in a sentence share the same tone, use the "
          },
          {
            "type": "Glyph",
            "props": {
              "language": "fjorunskara"
            },
            "children": "-va"
          },
          {
            "type": "span",
            "children": " (-va) suffix to indicate that at the very end of the sentence. Otherwise, use the corresponding word at the end of each clause."
          }
        ]
      },
      {
        "type": "MarkerTable",
        "props": {
          "language": "fjorunskara",
          "name": "Tone"
        },
        "children": [
          {
            "type": "Marker",
            "props": {
              "roman": "(None)",
              "glyph": "",
              "meaning": "Factual"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Fufum",
              "glyph": "fufum",
              "meaning": "Exclamatory (joyful)"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Fafum",
              "glyph": "fafum",
              "meaning": "Exclamatory (shocked/disgusted/afraid)"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Fyfum",
              "glyph": "fyfum",
              "meaning": "Exclamatory (rude)"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Shusen",
              "glyph": "shusen",
              "meaning": "Interrogative (deep/truthful)"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Jeshusen",
              "glyph": "Jeshusen",
              "meaning": "Interrogative (accusatory)"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Sega",
              "glyph": "sega",
              "meaning": "Mandative"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Feli",
              "glyph": "feli",
              "meaning": "Humorous"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Ena",
              "glyph": "Ena",
              "meaning": "Thoughtful"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Jebi",
              "glyph": "Jebi",
              "meaning": "Uneasy"
            }
          },
          {
            "type": "Marker",
            "props": {
              "roman": "Melur",
              "glyph": "melur",
              "meaning": "Warning"
            }
          }
        ]
      },
      {
        "type": "p",
        "props": {
          "id": "Questions",
          "intoc": "true"
        },
        "children": "As a final note, questions are a little bit different. Each question word can stand on its own as a sentence, or it can serve as a stand-in for a subject or object in a sentence. Simple questions require no tone marker, though any interrogative tone marker would be an acceptable substitution for a bit more conveyance of the point behind a question."
      },
      {
        "type": "DoubleTable",
        "props": {
          "language": "fjorunskara",
          "name": "Questions",
          "left": "Fjorunskara",
          "right": "English"
        },
        "children": [
          {
            "type": "DoubleEntry",
            "props": {
              "left": {
                "roman": "Vendi",
                "glyph": "vendi"
              },
              "right": {
                "roman": "Who? (singular)",
                "glyph": ""
              }
            }
          },
          {
            "type": "DoubleEntry",
            "props": {
              "left": {
                "roman": "Vendja",
                "glyph": "vendja"
              },
              "right": {
                "roman": "Who? (plural)",
                "glyph": ""
              }
            }
          },
          {
            "type": "DoubleEntry",
            "props": {
              "left": {
                "roman": "Veneska",
                "glyph": "veneska"
              },
              "right": {
                "roman": "Where?",
                "glyph": ""
              }
            }
          },
          {
            "type": "DoubleEntry",
            "props": {
              "left": {
                "roman": "Venjyta",
                "glyph": "venjyta"
              },
              "right": {
                "roman": "How?",
                "glyph": ""
              }
            }
          },
          {
            "type": "DoubleEntry",
            "props": {
              "left": {
                "roman": "Venle''ada",
                "glyph": "venle''Ada"
              },
              "right": {
                "roman": "Why?",
                "glyph": ""
              }
            }
          },
          {
            "type": "DoubleEntry",
            "props": {
              "left": {
                "roman": "Venval",
                "glyph": "venval"
              },
              "right": {
                "roman": "When?",
                "glyph": ""
              }
            }
          },
          {
            "type": "DoubleEntry",
            "props": {
              "left": {
                "roman": "Venzett",
                "glyph": "venzett"
              },
              "right": {
                "roman": "What?",
                "glyph": ""
              }
            }
          }
        ]
      },
      {
        "type": "Heading",
        "props": {
          "id": "Examples",
          "intoc": "true"
        },
        "children": "Examples"
      },
      {
        "type": "Example",
        "props": {
          "roman": "Grukad di inmisa fyfum!",
          "glyph": "grukad di Inmisa fyfum-",
          "translation": "You''re afraid of the dark!",
          "language": "fjorunskara"
        }
      },
      {
        "type": "Example",
        "props": {
          "roman": "Le''ad ko''ix eshaungau gomm fafum!",
          "glyph": "le''Ad ko''Ix Eshaungau gomm fafum-",
          "translation": "That was so sticky!",
          "language": "fjorunskara"
        }
      },
      {
        "type": "Example",
        "props": {
          "roman": "Bid nu ylaashur.",
          "glyph": "bid nu Ylaashur-",
          "translation": "I enjoy apples.",
          "language": "fjorunskara"
        }
      },
      {
        "type": "Example",
        "props": {
          "roman": "Venle''ada borjytad di menven?",
          "glyph": "venle''Ada borjytad di menven-",
          "translation": "Why are you running?",
          "language": "fjorunskara"
        }
      },
      {
        "type": "Example",
        "props": {
          "roman": "Borcroeshad ko''ylja ylakinjarvy kivycroenge''ef kivycroengeskad fe''i gommva.",
          "glyph": "borcroeshad ko''Ylja Ylakinarvy kivycroenge''Ef kivycroengeskad fe''I gommva-",
          "translation": "The kittens drank all their milk, leaving nothing behind.",
          "language": "fjorunskara"
        }
      }
    ]',
    '[
      {
        "type": "Centered",
        "children": "This page under construction."
      }
    ]');

INSERT INTO lexicons ("language", "order", "sections")
  VALUES (
    'fjorunskara',
    '{"a", "e", "i", "o", "u", "y", "au", "b", "p", "d", "t", "g", "k", "c", "z", "s", "sh", "l", "m", "n", "v", "f", "h", "j", "r", "''"}',
    '{
      "a": {
        "glyph": "A",
        "words": [
          {
            "name_roman": "Ashur",
            "name_glyph": "Ashur",
            "part": "Noun (Phys.)",
            "definition": "Apple"
          },
          {
            "name_roman": "Ashura",
            "name_glyph": "Ashura",
            "part": "Noun (Phys.)",
            "definition": "Fruit"
          },
          {
            "name_roman": "Ashuro",
            "name_glyph": "Ashuro",
            "part": "Adjective (Phys.)",
            "definition": "Red"
          },
          {
            "name_roman": "Ashuroesh",
            "name_glyph": "Ashuroesh",
            "part": "Noun (Conc.)",
            "definition": "The color red, redness"
          },
          {
            "name_roman": "Ashurod",
            "name_glyph": "Ashurod",
            "part": "Verb",
            "definition": "To be embarrassed; to embarrass"
          },
          {
            "name_roman": "Ashurodne",
            "name_glyph": "Ashurodne",
            "part": "Noun (Phys.)",
            "definition": "Embarrassment"
          },
          {
            "name_roman": "Ashurau",
            "name_glyph": "Ashurau",
            "part": "Adjective (Phys.)",
            "definition": "Fruity, fruitlike"
          },
          {
            "name_roman": "Ara",
            "name_glyph": "Ara",
            "part": "Number",
            "definition": "Five"
          },
          {
            "name_roman": "Arat",
            "name_glyph": "Arat",
            "part": "Noun (Phys.)",
            "definition": "Storm"
          },
          {
            "name_roman": "Arata",
            "name_glyph": "Arata",
            "part": "Noun (Conc.)",
            "definition": "Rage, anger"
          },
          {
            "name_roman": "Aratad",
            "name_glyph": "Aratad",
            "part": "Verb",
            "definition": "To be angry; to anger"
          },
          {
            "name_roman": "Arato",
            "name_glyph": "Arato",
            "part": "Adjective (Phys.)",
            "definition": "Storm"
          },
          {
            "name_roman": "Aratau",
            "name_glyph": "Aratau",
            "part": "Adjective (Phys.)",
            "definition": "Angry"
          },
          {
            "name_roman": "Arys",
            "name_glyph": "Arys",
            "part": "Noun (Conc.)",
            "definition": "Magic"
          },
          {
            "name_roman": "Arysa",
            "name_glyph": "Arysa",
            "part": "Noun (Conc.)",
            "definition": "Magetide"
          },
          {
            "name_roman": "Arysad",
            "name_glyph": "Arysad",
            "part": "Verb",
            "definition": "To harness the Magetide, to cast spells; to imbue with magic"
          },
          {
            "name_roman": "Arysadle",
            "name_glyph": "Arysadle",
            "part": "Noun (Phys.)",
            "definition": "Staff, wand, magical implement"
          },
          {
            "name_roman": "Arysadne",
            "name_glyph": "Arysadne",
            "part": "Noun (Phys.)",
            "definition": "Mage"
          },
          {
            "name_roman": "Aryso",
            "name_glyph": "Aryso",
            "part": "Adjective (Both)",
            "definition": "Magical"
          },
          {
            "name_roman": "Arysau",
            "name_glyph": "Arysau",
            "part": "Adjective (Conc.)",
            "definition": "Magetidal"
          }
        ]
      },
      "e": {
        "glyph": "E",
        "words": [
          {
            "name_roman": "Esk",
            "name_glyph": "Esk",
            "part": "Noun (Phys.)",
            "definition": "Here, this place" 
          },
          {
            "name_roman": "Eska",
            "name_glyph": "Eska",
            "part": "Noun (Conc.)",
            "definition": "Location"
          },
          {
            "name_roman": "Eskad",
            "name_glyph": "Eskad",
            "part": "Verb",
            "definition": "To be somewhere; to move"
          },
          {
            "name_roman": "Eskadle",
            "name_glyph": "Eskadle",
            "part": "Noun (Phys.)",
            "definition": "Dolly, transporter"
          },
          {
            "name_roman": "Eskadne",
            "name_glyph": "Eskadne",
            "part": "Noun (Phys.)",
            "definition": "Courier"
          },
          {
            "name_roman": "Eskaurok",
            "name_glyph": "Eskaurok",
            "part": "Noun (Phys.)",
            "definition": "Physical location"
          },
          {
            "name_roman": "Esko",
            "name_glyph": "Esko",
            "part": "Adjective (Phys.)",
            "definition": "Right here"
          },
          {
            "name_roman": "Eskau",
            "name_glyph": "Eskau",
            "part": "Adjective (Phys.)",
            "definition": "Local"
          },
          {
            "name_roman": "Eshaung",
            "name_glyph": "Eshaung",
            "part": "Noun (Phys.)",
            "definition": "Goop"
          },
          {
            "name_roman": "Eshaunga",
            "name_glyph": "Eshaunga",
            "part": "Noun (Conc.)",
            "definition": "Stickiness"
          },
          {
            "name_roman": "Eshaungad",
            "name_glyph": "Eshaungad",
            "part": "Verb",
            "definition": "To stick, to climb; to carry"
          },
          {
            "name_roman": "Eshaungadle",
            "name_glyph": "Eshaungadle",
            "part": "Noun (Phys.)",
            "definition": "Piton"
          },
          {
            "name_roman": "Eshaungadne",
            "name_glyph": "Eshaungadne",
            "part": "Noun (Phys.)",
            "definition": "Climber, carrier"
          },
          {
            "name_roman": "Eshaungadjul",
            "name_glyph": "Eshaungadjul",
            "part": "Adjective (Both)",
            "definition": "Stuck"
          },
          {
            "name_roman": "Eshaungo",
            "name_glyph": "Eshaungo",
            "part": "Adjective (Phys.)",
            "definition": "Viscous"
          },
          {
            "name_roman": "Eshaungau",
            "name_glyph": "Eshaungau",
            "part": "Adjective (Phys.)",
            "definition": "Sticky"
          },
          {
            "name_roman": "Eshaungauesh",
            "name_glyph": "Eshaungauesh",
            "part": "Noun (Phys.)",
            "definition": "Adhesive"
          },
          {
            "name_roman": "Eluf",
            "name_glyph": "Eluf",
            "part": "Noun (Phys.)",
            "definition": "Sugar"
          },
          {
            "name_roman": "Elufa",
            "name_glyph": "Elufa",
            "part": "Noun (Conc.)",
            "definition": "Sweetness"
          },
          {
            "name_roman": "Elufad",
            "name_glyph": "Elufad",
            "part": "Verb",
            "definition": "To be sweet; to sweeten"
          },
          {
            "name_roman": "Elufadle",
            "name_glyph": "Elufadle",
            "part": "Noun (Phys.)",
            "definition": "Sweetener"
          },
          {
            "name_roman": "Elufadne",
            "name_glyph": "Elufadne",
            "part": "Noun (Phys.)",
            "definition": "Baker"
          },
          {
            "name_roman": "Elufo",
            "name_glyph": "Elufo",
            "part": "Adjective (Phys.)",
            "definition": "Sugary"
          },
          {
            "name_roman": "Elufau",
            "name_glyph": "Elufau",
            "part": "Adjective (Phys.)",
            "definition": "Sweet"
          }
        ]
      },
      "i": {
        "glyph": "I",
        "words": [
          {
            "name_roman": "Isseg",
            "name_glyph": "Isseg",
            "part": "Noun (Conc.)",
            "definition": "Labor"
          },
          {
            "name_roman": "Issega",
            "name_glyph": "Issega",
            "part": "Noun (Conc.)",
            "definition": "Job"
          },
          {
            "name_roman": "Issegad",
            "name_glyph": "Issegad",
            "part": "Verb",
            "definition": "To work; to indenture, to hire"
          },
          {
            "name_roman": "Issegadne",
            "name_glyph": "Issegadne",
            "part": "Noun (Phys.)",
            "definition": "Employer"
          },
          {
            "name_roman": "Issegadle",
            "name_glyph": "Issegadle",
            "part": "Noun (Phys.)",
            "definition": "Contract"
          },
          {
            "name_roman": "Issegadjul",
            "name_glyph": "Issegadjul",
            "part": "Adjective (Phys.)",
            "definition": "Hard-working"
          },
          {
            "name_roman": "Issegadjulesh",
            "name_glyph": "Issegadjulesh",
            "part": "Noun (Phys.)",
            "definition": "Compensated servant, employee"
          },
          {
            "name_roman": "Issego",
            "name_glyph": "Issego",
            "part": "Adjective (Both)",
            "definition": "Laborous"
          },
          {
            "name_roman": "Issegoesh",
            "name_glyph": "Issegoesh",
            "part": "Noun (Conc.)",
            "definition": "Servitude"
          },
          {
            "name_roman": "Issegau",
            "name_glyph": "Issegau",
            "part": "Adjective (Both)",
            "definition": "Joblike"
          },
          {
            "name_roman": "Issegauesh",
            "name_glyph": "Issegauesh",
            "part": "Noun (Conc.)",
            "definition": "Career"
          },
          {
            "name_roman": "Il",
            "name_glyph": "Il",
            "part": "Noun (Phys.)",
            "definition": "Island"
          },
          {
            "name_roman": "Ila",
            "name_glyph": "Ila",
            "part": "Noun (Phys.)",
            "definition": "Archipelago"
          },
          {
            "name_roman": "Ilo",
            "name_glyph": "Ilo",
            "part": "Adjective (Phys.)",
            "definition": "Tropical"
          },
          {
            "name_roman": "Iloesh",
            "name_glyph": "Iloesh",
            "part": "Noun (Phys.)",
            "definition": "Jungle"
          },
          {
            "name_roman": "Inmis",
            "name_glyph": "Inmis",
            "part": "Noun (Conc.)",
            "definition": "Shadow"
          },
          {
            "name_roman": "Inmisa",
            "name_glyph": "Inmisa",
            "part": "Noun (Conc.)",
            "definition": "Darkness"
          },
          {
            "name_roman": "Inmisad",
            "name_glyph": "Inmisad",
            "part": "Verb",
            "definition": "To become darker; to darken"
          },
          {
            "name_roman": "Inmisadjul",
            "name_glyph": "Inmisadjul",
            "part": "Adjective (Both)",
            "definition": "Dimmer (Phys.), evil (Conc.)"
          },
          {
            "name_roman": "Inmiso",
            "name_glyph": "Inmiso",
            "part": "Adjective (Phys.)",
            "definition": "Shaded, shadowy"
          },
          {
            "name_roman": "Inmisoesh",
            "name_glyph": "Inmisoesh",
            "part": "Noun (Phys.)",
            "definition": "Shade"
          },
          {
            "name_roman": "Inmisau",
            "name_glyph": "Inmisau",
            "part": "Adjective (Phys.)",
            "definition": "Dark"
          },
          {
            "name_roman": "Ix",
            "name_glyph": "Ix",
            "part": "Number",
            "definition": "One"
          },
          {
            "name_roman": "Ixa",
            "name_glyph": "Ixa",
            "part": "Noun (Conc.)",
            "definition": "Number"
          },
          {
            "name_roman": "Ixesh",
            "name_glyph": "Ixesh",
            "part": "Noun (Conc.)",
            "definition": "Point (mathematics)"
          },
          {
            "name_roman": "Ixo",
            "name_glyph": "Ixo",
            "part": "Adjective (Both)",
            "definition": "Sole, lone, singular"
          },
          {
            "name_roman": "Ixoesh",
            "name_glyph": "Ixoesh",
            "part": "Noun (Conc.)",
            "definition": "The first"
          }
        ]
      },
      "o": {
        "glyph": "O",
        "words": [
          {
            "name_roman": "Ome",
            "name_glyph": "Ome",
            "part": "Noun (Phys.)",
            "definition": "Rune"
          },
          {
            "name_roman": "Omea",
            "name_glyph": "Omea",
            "part": "Noun (Conc.)",
            "definition": "Script, orthography"
          },
          {
            "name_roman": "Omead",
            "name_glyph": "Omead",
            "part": "Verb",
            "definition": "To print, as text"
          },
          {
            "name_roman": "Omeadle",
            "name_glyph": "Omeadle",
            "part": "Noun (Phys.)",
            "definition": "Printing press, printer"
          },
          {
            "name_roman": "Omeadjul",
            "name_glyph": "Omeadjul",
            "part": "Adjective (Both)",
            "definition": "Written, described"
          },
          {
            "name_roman": "Omeadjula",
            "name_glyph": "Omeadjula",
            "part": "Noun (Conc.)",
            "definition": "Definition"
          },
          {
            "name_roman": "Omeadjulesh",
            "name_glyph": "Omeadjulesh",
            "part": "Noun (Phys.)",
            "definition": "Book"
          },
          {
            "name_roman": "Omeadjulesha",
            "name_glyph": "Omeadjulesha",
            "part": "Noun (Phys.)",
            "definition": "Library"
          },
          {
            "name_roman": "Omeadjulau",
            "name_glyph": "Omeadjulau",
            "part": "Adjective (Both)",
            "definition": "Defined"
          },
          {
            "name_roman": "Omeadjulauesh",
            "name_glyph": "Omeadjulauesh",
            "part": "Noun (Phys.)",
            "definition": "Dictionary, lexicon"
          },
          {
            "name_roman": "Omesenafa",
            "name_glyph": "Omesenafa",
            "part": "Noun (Conc.)",
            "definition": "Recording"
          },
          {
            "name_roman": "Omesenafad",
            "name_glyph": "Omesenafad",
            "part": "Verb",
            "definition": "To record"
          },
          {
            "name_roman": "Omesenafadle",
            "name_glyph": "Omesenafadle",
            "part": "Noun (Phys.)",
            "definition": "Microphone"
          },
          {
            "name_roman": "Omesenafadne",
            "name_glyph": "Omesenafadne",
            "part": "Noun (Phys.)",
            "definition": "Transcriber"
          },
          {
            "name_roman": "Omesenafau",
            "name_glyph": "Omesenafau",
            "part": "Adjective (Both)",
            "definition": "Recorded"
          },
          {
            "name_roman": "Omesenafauesh",
            "name_glyph": "Omesenafauesh",
            "part": "Noun (Phys.)",
            "definition": "Journal, log"
          },
          {
            "name_roman": "Omefum",
            "name_glyph": "Omefum",
            "part": "Noun (Conc.)",
            "definition": "Phoneme"
          },
          {
            "name_roman": "Omefuma",
            "name_glyph": "Omefuma",
            "part": "Noun (Conc.)",
            "definition": "Morpheme, syllable"
          },
          {
            "name_roman": "Omefuma''a",
            "name_glyph": "Omefuma''A",
            "part": "Noun (Conc.)",
            "definition": "Word"
          },
          {
            "name_roman": "Omefuma''a''a",
            "name_glyph": "Omefuma''A''A",
            "part": "Noun (Conc.)",
            "definition": "Sentence"
          },
          {
            "name_roman": "Omefuma''a''a''a",
            "name_glyph": "Omefuma''A''A''A",
            "part": "Noun (Conc.)",
            "definition": "Paragraph"
          },
          {
            "name_roman": "Ome''e",
            "name_glyph": "Ome''E",
            "part": "Verb",
            "definition": "To write"
          },
          {
            "name_roman": "Ome''ele",
            "name_glyph": "Ome''Ele",
            "part": "Noun (Phys.)",
            "definition": "Writing implement"
          },
          {
            "name_roman": "Ome''ene",
            "name_glyph": "Ome''Ene",
            "part": "Noun (Phys.)",
            "definition": "Author, scribe"
          },
          {
            "name_roman": "Otussauesh",
            "name_glyph": "Otussauesh",
            "part": "Noun (Phys.)",
            "definition": "Dungeon"
          },
          {
            "name_roman": "Otussauesha",
            "name_glyph": "Otussauesha",
            "part": "Noun (Phys.)",
            "definition": "Excavation, quarry"
          },
          {
            "name_roman": "Otmika",
            "name_glyph": "Otmika",
            "part": "Noun (Phys.)",
            "definition": "Subterranea"
          },
          {
            "name_roman": "Otmikad",
            "name_glyph": "Otmikad",
            "part": "Verb",
            "definition": "To delve, to dig; to bury"
          },
          {
            "name_roman": "Otmikadle",
            "name_glyph": "Otmikadle",
            "part": "Noun (Phys.)",
            "definition": "Shovel, excavator"
          },
          {
            "name_roman": "Otmikadne",
            "name_glyph": "Otmikadne",
            "part": "Noun (Phys.)",
            "definition": "Delver, digger"
          },
          {
            "name_roman": "Otmikau",
            "name_glyph": "Otmikau",
            "part": "Adjective (Phys.)",
            "definition": "Underground"
          }
        ]
      },
      "u": {
        "glyph": "U",
        "words": [
          {
            "name_roman": "Uzra",
            "name_glyph": "Uzra",
            "part": "Noun (Conc.)",
            "definition": "Flight"
          },
          {
            "name_roman": "Uzrad",
            "name_glyph": "Uzrad",
            "part": "Verb",
            "definition": "To fly"
          },
          {
            "name_roman": "Uzradle",
            "name_glyph": "Uzradle",
            "part": "Noun (Phys.)",
            "definition": "Wing"
          },
          {
            "name_roman": "Uzradne",
            "name_glyph": "Uzradne",
            "part": "Noun (Phys.)",
            "definition": "Pilot"
          },
          {
            "name_roman": "Uzradjul",
            "name_glyph": "Uzradjul",
            "part": "Adjective (Phys.)",
            "definition": "Flying, in flight"
          },
          {
            "name_roman": "Uzradjulesh",
            "name_glyph": "Uzradjulesh",
            "part": "Noun (Phys.)",
            "definition": "Bird"
          },
          {
            "name_roman": "Uzrau",
            "name_glyph": "Uzrau",
            "part": "Adjective (Phys.)",
            "definition": "Aloft, floating"
          },
          {
            "name_roman": "Uzrauesh",
            "name_glyph": "Uzrauesh",
            "part": "Noun (Phys.)",
            "definition": "Cloud"
          },
          {
            "name_roman": "Uss",
            "name_glyph": "Uss",
            "part": "Noun (Phys.)",
            "definition": "Bone"
          },
          {
            "name_roman": "Ussa",
            "name_glyph": "Ussa",
            "part": "Noun (Phys.)",
            "definition": "Skeleton"
          },
          {
            "name_roman": "Ussad",
            "name_glyph": "Ussad",
            "part": "Verb)",
            "definition": "To be reinforced; to constitute"
          },
          {
            "name_roman": "Ussadle",
            "name_glyph": "Ussadle",
            "part": "Noun (Phys.)",
            "definition": "Beam, pillar"
          },
          {
            "name_roman": "Ussadne",
            "name_glyph": "Ussadne",
            "part": "Noun (Phys.)",
            "definition": "Reinforcement"
          },
          {
            "name_roman": "Usso",
            "name_glyph": "Usso",
            "part": "Noun (Phys.)",
            "definition": "Bony"
          },
          {
            "name_roman": "Ussau",
            "name_glyph": "Ussau",
            "part": "Adjective (Both)",
            "definition": "Constitutional, structured"
          },
          {
            "name_roman": "Ussauesh",
            "name_glyph": "Ussauesh",
            "part": "Noun (Phys.)",
            "definition": "Building, structure"
          },
          {
            "name_roman": "Ussaueshad",
            "name_glyph": "Ussaueshad",
            "part": "Verb",
            "definition": "To construct"
          },
          {
            "name_roman": "Ussaueshadle",
            "name_glyph": "Ussaueshadle",
            "part": "Noun (Phys.)",
            "definition": "Blueprint"
          },
          {
            "name_roman": "Ussaueshadne",
            "name_glyph": "Ussaueshadne",
            "part": "Noun (Phys.)",
            "definition": "Builder"
          }
        ]
      },
      "y": {
        "glyph": "Y",
        "words": [
          {
            "name_roman": "Yga",
            "name_glyph": "Yga",
            "part": "Noun (Conc.)",
            "definition": "Size"
          },
          {
            "name_roman": "Ygad",
            "name_glyph": "Ygad",
            "part": "Verb",
            "definition": "To grow; to cause to grow"
          },
          {
            "name_roman": "Ygadle",
            "name_glyph": "Ygadle",
            "part": "Noun (Phys.)",
            "definition": "Fertilizer"
          },
          {
            "name_roman": "Ygadjul",
            "name_glyph": "Ygadjul",
            "part": "Adjective (Both)",
            "definition": "Grown of scope (Conc.), grown of size or age (Phys.)"
          },
          {
            "name_roman": "Ygo",
            "name_glyph": "Ygo",
            "part": "Adjective (Phys.)",
            "definition": "Large"
          },
          {
            "name_roman": "Ylja",
            "name_glyph": "Ylja",
            "part": "Noun (Conc.)",
            "definition": "Group, collection"
          },
          {
            "name_roman": "Yljad",
            "name_glyph": "Yljad",
            "part": "Verb",
            "definition": "To comprise; to collect"
          },
          {
            "name_roman": "Yljadne",
            "name_glyph": "Yljadne",
            "part": "Noun (Phys.)",
            "definition": "Collector"
          },
          {
            "name_roman": "Yljadjul",
            "name_glyph": "Yljadjul",
            "part": "Adjective (Both)",
            "definition": "Together"
          },
          {
            "name_roman": "Yljau",
            "name_glyph": "Yljau",
            "part": "Adjective (Both)",
            "definition": "Myriad"
          },
          {
            "name_roman": "Yljauesh",
            "name_glyph": "Yljauesh",
            "part": "Noun (Phys.)",
            "definition": "Fragment, shard, particle"
          },
          {
            "name_roman": "Yljaud",
            "name_glyph": "Yljaud",
            "part": "Verb",
            "definition": "To split; to shatter into pieces"
          },
          {
            "name_roman": "Yljaudle",
            "name_glyph": "Yljaudle",
            "part": "Noun (Phys.)",
            "definition": "Axe, pickaxe"
          },
          {
            "name_roman": "Yljaudne",
            "name_glyph": "Yljaudne",
            "part": "Noun (Phys.)",
            "definition": "Lumberjack, miner"
          }
        ]
      },
      "au": {
        "glyph": "Au",
        "words": [
          {
            "name_roman": "Aut",
            "name_glyph": "Aut",
            "part": "Noun (Conc.)",
            "definition": "Death"
          },
          {
            "name_roman": "Auta",
            "name_glyph": "Auta",
            "part": "Noun (Conc.)",
            "definition": "Afterlife"
          },
          {
            "name_roman": "Autad",
            "name_glyph": "Autad",
            "part": "Verb",
            "definition": "To die; to kill"
          },
          {
            "name_roman": "Autadle",
            "name_glyph": "Autadle",
            "part": "Noun (Phys.)",
            "definition": "Poison"
          },
          {
            "name_roman": "Autadne",
            "name_glyph": "Autadne",
            "part": "Noun (Phys.)",
            "definition": "Killer, assassin"
          },
          {
            "name_roman": "Auto",
            "name_glyph": "Auto",
            "part": "Adjective (Both)",
            "definition": "Dead (Phys.), gone (Conc.)"
          },
          {
            "name_roman": "Autoesh",
            "name_glyph": "Autoesh",
            "part": "Noun (Conc.)",
            "definition": "Void, lack"
          },
          {
            "name_roman": "Autoesha",
            "name_glyph": "Autoesha",
            "part": "Noun (Phys.)",
            "definition": "The Vastest Sea"
          },
          {
            "name_roman": "Autoeshad",
            "name_glyph": "Autoeshad",
            "part": "Verb",
            "definition": "To become empty; to drain"
          },
          {
            "name_roman": "Autoeshadle",
            "name_glyph": "Autoeshadle",
            "part": "Noun (Phys.)",
            "definition": "Drain, gutter"
          },
          {
            "name_roman": "Aulat",
            "name_glyph": "Aulat",
            "part": "Noun (Conc.)",
            "definition": "Grief, sadness"
          },
          {
            "name_roman": "Aulatad",
            "name_glyph": "Aulatad",
            "part": "Verb",
            "definition": "To mourn, to feel sad; to sadden"
          },
          {
            "name_roman": "Aulato",
            "name_glyph": "Aulato",
            "part": "Adjective (Phys.)",
            "definition": "Melancholy, sad"
          }
        ]
      },
      "b": {
        "glyph": "b",
        "words": [
          {
            "name_roman": "Barix",
            "name_glyph": "barix",
            "part": "Article",
            "definition": "A, an (indefinite singular)"
          },
          {
            "name_roman": "Barylja",
            "name_glyph": "barylja",
            "part": "Article",
            "definition": "Some (indefinite plural)"
          },
          {
            "name_roman": "Bi",
            "name_glyph": "bi",
            "part": "Noun (Conc.)",
            "definition": "Comfort"
          },
          {
            "name_roman": "Bid",
            "name_glyph": "bid",
            "part": "Verb",
            "definition": "To be comfortable; to comfort"
          },
          {
            "name_roman": "Bidle",
            "name_glyph": "bidle",
            "part": "Noun (Phys.)",
            "definition": "Blanket, comforter"
          },
          {
            "name_roman": "Bidne",
            "name_glyph": "bidne",
            "part": "Noun (Phys.)",
            "definition": "Emotional support"
          },
          {
            "name_roman": "Bidlevy",
            "name_glyph": "bidlevy",
            "part": "Noun (Phys.)",
            "definition": "Stuffie"
          },
          {
            "name_roman": "Bod",
            "name_glyph": "bod",
            "part": "Noun (Phys.)",
            "definition": "There"
          },
          {
            "name_roman": "Borcroesh",
            "name_glyph": "borcroesh",
            "part": "Noun (Conc.)",
            "definition": "Water cycle"
          },
          {
            "name_roman": "Borkuita''a",
            "name_glyph": "borkuita''A",
            "part": "Noun (Conc.)",
            "definition": "Hue shift"
          },
          {
            "name_roman": "Borkuita''ad",
            "name_glyph": "borkuita''Ad",
            "part": "Verb",
            "definition": "To change color (of oneself, gradually); to induce a color shift"
          },
          {
            "name_roman": "Borkuita''adjul",
            "name_glyph": "borkuita''Adjul",
            "part": "Adjective (Phys.)",
            "definition": "Color-changing"
          },
          {
            "name_roman": "Borkuita''adjulesh",
            "name_glyph": "borkuita''Adjulesh",
            "part": "Noun (Phys.)",
            "definition": "Camouflage"
          },
          {
            "name_roman": "Borkuita''au",
            "name_glyph": "borkuita''Au",
            "part": "Adjective (Phys.)",
            "definition": "Rainbow"
          },
          {
            "name_roman": "Borkuita''auesh",
            "name_glyph": "borkuita''Auesh",
            "part": "Noun (Conc.)",
            "definition": "Color spectrum"
          },
          {
            "name_roman": "Borcroesha",
            "name_glyph": "borcroesha",
            "part": "Noun (Conc.)",
            "definition": "Thirst"
          },
          {
            "name_roman": "Borcroeshad",
            "name_glyph": "borcroeshad",
            "part": "Verb",
            "definition": "To drink; to imbibe"
          },
          {
            "name_roman": "Borcroeshadle",
            "name_glyph": "borcroeshadle",
            "part": "Noun (Phys.)",
            "definition": "Cup, glass, mug"
          },
          {
            "name_roman": "Borcroeshadne",
            "name_glyph": "borcroeshadne",
            "part": "Noun (Phys.)",
            "definition": "Drinker"
          },
          {
            "name_roman": "Borcroeshau",
            "name_glyph": "borcroeshau",
            "part": "Adjective (Phys.)",
            "definition": "Thirsty"
          },
          {
            "name_roman": "Borjyt",
            "name_glyph": "borjyt",
            "part": "Noun (Conc.)",
            "definition": "Speed"
          },
          {
            "name_roman": "Borjyta",
            "name_glyph": "borjyta",
            "part": "Noun (Conc.)",
            "definition": "Race"
          },
          {
            "name_roman": "Borjytad",
            "name_glyph": "borjytad",
            "part": "Verb",
            "definition": "To run, race"
          },
          {
            "name_roman": "Borjyto",
            "name_glyph": "borjyto",
            "part": "Adjective (Phys.)",
            "definition": "Fast"
          },
          {
            "name_roman": "Borjytadne",
            "name_glyph": "borjytadne",
            "part": "Noun (Phys.)",
            "definition": "Runner, racer"
          },
          {
            "name_roman": "Borjytoesh",
            "name_glyph": "borjytoesh",
            "part": "Noun (Phys.)",
            "definition": "Flash, instant"
          },
          {
            "name_roman": "Borjytoeshad",
            "name_glyph": "borjytoeshad",
            "part": "Verb",
            "definition": "To flash, teleport"
          },
          {
            "name_roman": "Borjytoesho",
            "name_glyph": "borjytoesho",
            "part": "Adjective (Phys.)",
            "definition": "Instantaneous"
          },
          {
            "name_roman": "Blevy",
            "name_glyph": "blevy",
            "part": "Noun (Phys.)",
            "definition": "Sheep"
          },
          {
            "name_roman": "Blevhyjal",
            "name_glyph": "blehyjal",
            "part": "Noun (Phys.)",
            "definition": "Mutton"
          },
          {
            "name_roman": "Bja",
            "name_glyph": "bja",
            "part": "Noun (Conc.)",
            "definition": "Luxury"
          },
          {
            "name_roman": "Bjad",
            "name_glyph": "bjad",
            "part": "Verb",
            "definition": "To luxuriate, enjoy"
          },
          {
            "name_roman": "Bjadne",
            "name_glyph": "bjadne",
            "part": "Noun (Phys.)",
            "definition": "Enjoyer"
          },
          {
            "name_roman": "Bjadjul",
            "name_glyph": "bjadjul",
            "part": "Adjective (Phys.)",
            "definition": "Hedonistic"
          },
          {
            "name_roman": "Bjadjulesh",
            "name_glyph": "bjadjulesh",
            "part": "Noun (Phys.)",
            "definition": "Hedonist"
          },
          {
            "name_roman": "Bjadjulesha",
            "name_glyph": "bjadjulesha",
            "part": "Noun (Conc.)",
            "definition": "Hedonism"
          },
          {
            "name_roman": "Bjo",
            "name_glyph": "bjo",
            "part": "Adjective (Phys.)",
            "definition": "Comfortable"
          },
          {
            "name_roman": "Bjau",
            "name_glyph": "bjau",
            "part": "Adjective (Both)",
            "definition": "Luxurious"
          },
          {
            "name_roman": "Bri",
            "name_glyph": "bri",
            "part": "Noun (Phys.)",
            "definition": "Vine"
          },
          {
            "name_roman": "Bri''ad",
            "name_glyph": "bri''Ad",
            "part": "Verb",
            "definition": "To creep, climb"
          },
          {
            "name_roman": "Bri''adle",
            "name_glyph": "bri''Adle",
            "part": "Noun (Phys.)",
            "definition": "Rope"
          },
          {
            "name_roman": "Bri''adne",
            "name_glyph": "bri''Adne",
            "part": "Noun (Phys.)",
            "definition": "Creep, stalker"
          },
          {
            "name_roman": "Bri''adjul",
            "name_glyph": "bri''Adjul",
            "part": "Adjective (Both)",
            "definition": "Creepy"
          },
          {
            "name_roman": "Bri''o",
            "name_glyph": "bri''O",
            "part": "Adjective (Phys.)",
            "definition": "Slender"
          }
        ]
      },
      "p": {
        "glyph": "p",
        "words": [
          {
            "name_roman": "Povy",
            "name_glyph": "povy",
            "part": "Noun (Phys.)",
            "definition": "Cow"
          },
          {
            "name_roman": "Povhyjal",
            "name_glyph": "povhyjal",
            "part": "Noun (Phys.)",
            "definition": "Beef"
          },
          {
            "name_roman": "Pjo",
            "name_glyph": "pjo",
            "part": "Adjective (Conc.)",
            "definition": "Physical (often unused, defer to -urok when possible)"
          },
          {
            "name_roman": "Pjoad",
            "name_glyph": "pjoad",
            "part": "Verb",
            "definition": "To be something physical"
          },
          {
            "name_roman": "Pjoada",
            "name_glyph": "pjoada",
            "part": "Noun (Conc.)",
            "definition": "Physicality"
          },
          {
            "name_roman": "Preit",
            "name_glyph": "preit",
            "part": "Noun (Conc.)",
            "definition": "Wealth, value"
          },
          {
            "name_roman": "Preita",
            "name_glyph": "preita",
            "part": "Noun (Conc.)",
            "definition": "Greed"
          },
          {
            "name_roman": "Preitad",
            "name_glyph": "preitad",
            "part": "Verb",
            "definition": "To desire, want"
          },
          {
            "name_roman": "Preitadle",
            "name_glyph": "preitadle",
            "part": "Noun (Conc.)",
            "definition": "Taste, desire"
          },
          {
            "name_roman": "Preitadne",
            "name_glyph": "preitadne",
            "part": "Noun (Phys.)",
            "definition": "Wanter, beggar"
          },
          {
            "name_roman": "Preitadjul",
            "name_glyph": "preitadjul",
            "part": "Adjective (Phys.)",
            "definition": "Wanting"
          },
          {
            "name_roman": "Preitazett",
            "name_glyph": "preitazett",
            "part": "Noun (Conc.)",
            "definition": "Possession"
          },
          {
            "name_roman": "Preitazetta",
            "name_glyph": "preitazetta",
            "part": "Noun (Conc.)",
            "definition": "Ownership"
          },
          {
            "name_roman": "Preitazettad",
            "name_glyph": "preitazettad",
            "part": "Verb",
            "definition": "To own, have"
          },
          {
            "name_roman": "Preitazettadne",
            "name_glyph": "preitazettadne",
            "part": "Noun (Phys.)",
            "definition": "Owner"
          },
          {
            "name_roman": "Preitazetto",
            "name_glyph": "preitazetto",
            "part": "Adjective (Phys.)",
            "definition": "Possessed"
          },
          {
            "name_roman": "Preitazettoesh",
            "name_glyph": "preitazettoesh",
            "part": "Noun (Conc.)",
            "definition": "Belonging"
          },
          {
            "name_roman": "Preitazettau",
            "name_glyph": "preitazettau",
            "part": "Adjective (Phys.)",
            "definition": "Owned"
          },
          {
            "name_roman": "Preitazettauesh",
            "name_glyph": "preitazettauesh",
            "part": "Noun (Phys.)",
            "definition": "Slave"
          },
          {
            "name_roman": "Preito",
            "name_glyph": "preito",
            "part": "Adjective (Both)",
            "definition": "Valuable"
          },
          {
            "name_roman": "Preitau",
            "name_glyph": "preitau",
            "part": "Adjective (Both)",
            "definition": "Greedy"
          }
        ]
      },
      "d": {
        "glyph": "d",
        "words": [
          {
            "name_roman": "Da",
            "name_glyph": "da",
            "part": "Noun (Conc.)",
            "definition": "Conflict"
          },
          {
            "name_roman": "Dad",
            "name_glyph": "dad",
            "part": "Verb",
            "definition": "To disagree"
          },
          {
            "name_roman": "Dadne",
            "name_glyph": "dadne",
            "part": "Noun (Phys.)",
            "definition": "Arguer, debater"
          },
          {
            "name_roman": "Da''a",
            "name_glyph": "da''A",
            "part": "Noun (Conc.)",
            "definition": "War"
          },
          {
            "name_roman": "Da''ad",
            "name_glyph": "da''Ad",
            "part": "Verb",
            "definition": "To fight; to challenge"
          },
          {
            "name_roman": "Da''adle",
            "name_glyph": "da''Adle",
            "part": "Noun (Phys.)",
            "definition": "Arena"
          },
          {
            "name_roman": "Da''adne",
            "name_glyph": "da''Adne",
            "part": "Noun (Phys.)",
            "definition": "Fighter, warrior, gladiator"
          },
          {
            "name_roman": "Da''au",
            "name_glyph": "da''Au",
            "part": "Adjective (Both)",
            "definition": "Warlike"
          },
          {
            "name_roman": "De",
            "name_glyph": "de",
            "part": "Verb",
            "definition": "To be a gift to you (second-person singular)"
          },
          {
            "name_roman": "Dea",
            "name_glyph": "dea",
            "part": "Verb",
            "definition": "To be a gift to you all (second-person plural)"
          },
          {
            "name_roman": "Dealygon",
            "name_glyph": "dealygon",
            "part": "Verb/Interjection",
            "definition": "Welcome"
          },
          {
            "name_roman": "Dealygonne",
            "name_glyph": "dealygonne",
            "part": "Noun (Phys.)",
            "definition": "Welcomer"
          },
          {
            "name_roman": "Dealygono",
            "name_glyph": "dealygono",
            "part": "Adjective (Both)",
            "definition": "Welcome"
          },
          {
            "name_roman": "Demorad",
            "name_glyph": "demorad",
            "part": "Verb",
            "definition": "To have (a concept); To bring (a concept)"
          },
          {
            "name_roman": "Demoradne",
            "name_glyph": "demoradne",
            "part": "Noun (Phys.)",
            "definition": "Bringer (of a concept)"
          },
          {
            "name_roman": "Di",
            "name_glyph": "di",
            "part": "Pronoun",
            "definition": "You (second-person singular)"
          },
          {
            "name_roman": "Duris",
            "name_glyph": "duris",
            "part": "Noun (Phys.)",
            "definition": "Herb"
          },
          {
            "name_roman": "Durisa",
            "name_glyph": "durisa",
            "part": "Noun (Conc.)",
            "definition": "Herbalism"
          },
          {
            "name_roman": "Duriso",
            "name_glyph": "duriso",
            "part": "Adjective (Phys.)",
            "definition": "Herbal"
          },
          {
            "name_roman": "Dyylot",
            "name_glyph": "dyylot",
            "part": "Noun (Conc.)",
            "definition": "Friend"
          },
          {
            "name_roman": "Dyylota",
            "name_glyph": "dyylota",
            "part": "Noun (Conc.)",
            "definition": "Friendship"
          },
          {
            "name_roman": "Dyylotad",
            "name_glyph": "dyylotad",
            "part": "Adjective (Both)",
            "definition": "Of a partnership or bond"
          },
          {
            "name_roman": "Dau",
            "name_glyph": "dau",
            "part": "Adjective (Both)",
            "definition": "Contradictory"
          },
          {
            "name_roman": "Dauesh",
            "name_glyph": "dauesh",
            "part": "Noun (Conc.)",
            "definition": "Antithesis, opposite"
          },
          {
            "name_roman": "Dja",
            "name_glyph": "dja",
            "part": "Pronoun",
            "definition": "You (second-person plural)"
          },
          {
            "name_roman": "Djam",
            "name_glyph": "djam",
            "part": "Number",
            "definition": "Six"
          },
          {
            "name_roman": "Djamesh",
            "name_glyph": "djamesh",
            "part": "Noun (Conc.)",
            "definition": "Hexagon"
          },
          {
            "name_roman": "Djar",
            "name_glyph": "djar",
            "part": "Noun (Conc.)",
            "definition": "Greeting"
          },
          {
            "name_roman": "Djarad",
            "name_glyph": "djarad",
            "part": "Verb",
            "definition": "To greet"
          },
          {
            "name_roman": "Djaradne",
            "name_glyph": "djaradne",
            "part": "Noun (Phys.)",
            "definition": "Greeter"
          },
          {
            "name_roman": "Djaradjul",
            "name_glyph": "djaradjul",
            "part": "Adjective (Phys.)",
            "definition": "Met, as one who has been greeted before"
          },
          {
            "name_roman": "Djaradjulesh",
            "name_glyph": "djaradjulesh",
            "part": "Noun (Conc.)",
            "definition": "Meeting"
          },
          {
            "name_roman": "Djarva",
            "name_glyph": "djarva",
            "part": "Interjection",
            "definition": "Hello"
          },
          {
            "name_roman": "Dra",
            "name_glyph": "dra",
            "part": "Noun (Phys.)",
            "definition": "Reptile"
          },
          {
            "name_roman": "Dra''a",
            "name_glyph": "dra''A",
            "part": "Noun (Phys.)",
            "definition": "Scale"
          },
          {
            "name_roman": "Dra''ad",
            "name_glyph": "dra''Ad",
            "part": "Verb",
            "definition": "To deflect, protect"
          },
          {
            "name_roman": "Dra''adle",
            "name_glyph": "dra''Adle",
            "part": "Noun (Phys.)",
            "definition": "Shield"
          },
          {
            "name_roman": "Dra''adne",
            "name_glyph": "dra''Adne",
            "part": "Noun (Phys.)",
            "definition": "Guard"
          },
          {
            "name_roman": "Dra''au",
            "name_glyph": "dra''Au",
            "part": "Adjective (Phys.)",
            "definition": "Scaled"
          },
          {
            "name_roman": "Drau",
            "name_glyph": "drau",
            "part": "Adjective (Phys.)",
            "definition": "Reptilian"
          },
          {
            "name_roman": "Drauesh",
            "name_glyph": "drauesh",
            "part": "Noun (Phys.)",
            "definition": "Dragon"
          }
        ]
      },
      "t": {
        "glyph": "t",
        "words": [
          {
            "name_roman": "Taltor",
            "name_glyph": "taltor",
            "part": "Noun (Conc.)",
            "definition": "Loathing, hatred"
          },
          {
            "name_roman": "Taltorad",
            "name_glyph": "taltorad",
            "part": "Verb",
            "definition": "To hate"
          },
          {
            "name_roman": "Taltoradne",
            "name_glyph": "taltoradne",
            "part": "Noun (Phys.)",
            "definition": "Hater"
          },
          {
            "name_roman": "Taltoradjul",
            "name_glyph": "taltoradjul",
            "part": "Adjective (Both)",
            "definition": "Hateful"
          },
          {
            "name_roman": "Taltoro",
            "name_glyph": "taltoro",
            "part": "Adjective (Both)",
            "definition": "Hated"
          },
          {
            "name_roman": "Ta''ix",
            "name_glyph": "ta''Ix",
            "part": "Article",
            "definition": "The (definite singular)"
          },
          {
            "name_roman": "Ta''ylja",
            "name_glyph": "ta''Ylja",
            "part": "Article",
            "definition": "The (definite plural)"
          },
          {
            "name_roman": "Telo",
            "name_glyph": "telo",
            "part": "Adjective (Conc.)",
            "definition": "Low quality"
          },
          {
            "name_roman": "Tem",
            "name_glyph": "tem",
            "part": "Noun (Phys.)",
            "definition": "Projectile"
          },
          {
            "name_roman": "Tema",
            "name_glyph": "tema",
            "part": "Noun (Conc.)",
            "definition": "Trajectory, arc"
          },
          {
            "name_roman": "Temad",
            "name_glyph": "temad",
            "part": "Verb",
            "definition": "To throw"
          },
          {
            "name_roman": "Temadle",
            "name_glyph": "temadle",
            "part": "Noun (Phys.)",
            "definition": "Sling"
          },
          {
            "name_roman": "Temadne",
            "name_glyph": "temadne",
            "part": "Noun (Phys.)",
            "definition": "Thrower"
          },
          {
            "name_roman": "Temadjul",
            "name_glyph": "temadjul",
            "part": "Adjective (Phys.)",
            "definition": "Thrown"
          },
          {
            "name_roman": "Temazal",
            "name_glyph": "temazal",
            "part": "Noun (Phys.)",
            "definition": "Ejaculate"
          },
          {
            "name_roman": "Temazalad",
            "name_glyph": "temazalad",
            "part": "Verb",
            "definition": "To ejaculate"
          },
          {
            "name_roman": "Temo",
            "name_glyph": "temo",
            "part": "Adjective (Phys.)",
            "definition": "Projectile-like, of a projectile"
          },
          {
            "name_roman": "Temau",
            "name_glyph": "temau",
            "part": "Adjective (Phys.)",
            "definition": "Arcing"
          },
          {
            "name_roman": "Tilo",
            "name_glyph": "tilo",
            "part": "Adjective (Phys.)",
            "definition": "Low quality"
          },
          {
            "name_roman": "Tla",
            "name_glyph": "tla",
            "part": "Number",
            "definition": "Two"
          },
          {
            "name_roman": "Tlaesh",
            "name_glyph": "tlaesh",
            "part": "Noun (Conc.)",
            "definition": "Line"
          },
          {
            "name_roman": "Tlau",
            "name_glyph": "tlau",
            "part": "Adjective (Both)",
            "definition": "Paired, binary"
          },
          {
            "name_roman": "Tris",
            "name_glyph": "tris",
            "part": "Noun (Phys.)",
            "definition": "Plant"
          },
          {
            "name_roman": "Trisa",
            "name_glyph": "trisa",
            "part": "Noun (Phys.)",
            "definition": "Garden, ecosystem"
          },
          {
            "name_roman": "Trisad",
            "name_glyph": "trisad",
            "part": "Verb",
            "definition": "To garden; to plant"
          },
          {
            "name_roman": "Trisadle",
            "name_glyph": "trisadle",
            "part": "Noun (Phys.)",
            "definition": "Trowel"
          },
          {
            "name_roman": "Trisadne",
            "name_glyph": "trisadne",
            "part": "Noun (Phys.)",
            "definition": "Gardener"
          },
          {
            "name_roman": "Trisadjul",
            "name_glyph": "trisadjul",
            "part": "Adjective (Both)",
            "definition": "Rooted"
          },
          {
            "name_roman": "Trisafa",
            "name_glyph": "trisafa",
            "part": "Noun (Conc.)",
            "definition": "Metamorphosis, perpetual growth"
          },
          {
            "name_roman": "Trisafad",
            "name_glyph": "trisafad",
            "part": "Verb",
            "definition": "To bloom; to cultivate"
          },
          {
            "name_roman": "Trisafadne",
            "name_glyph": "trisafadne",
            "part": "Noun (Phys.)",
            "definition": "Bloomer, farmer"
          },
          {
            "name_roman": "Trisafadjul",
            "name_glyph": "trisafadjul",
            "part": "Adjective (Both)",
            "definition": "Developed"
          },
          {
            "name_roman": "Trisafau",
            "name_glyph": "trisafau",
            "part": "Adjective (Phys.)",
            "definition": "Blossomed, flowered (of a plant or region)"
          },
          {
            "name_roman": "Triso",
            "name_glyph": "triso",
            "part": "Adjective (Both)",
            "definition": "Plantlike"
          },
          {
            "name_roman": "Trokalt",
            "name_glyph": "trokalt",
            "part": "Noun (Conc.)",
            "definition": "Nightmare"
          },
          {
            "name_roman": "Trokalta",
            "name_glyph": "trokalta",
            "part": "Noun (Conc.)",
            "definition": "Fear"
          },
          {
            "name_roman": "Trokaltad",
            "name_glyph": "trokaltad",
            "part": "Verb",
            "definition": "To have a nightmare, be scared; to scare"
          },
          {
            "name_roman": "Trokaltadne",
            "name_glyph": "trokaltadne",
            "part": "Noun (Phys.)",
            "definition": "Scarer"
          },
          {
            "name_roman": "Trokaltadjul",
            "name_glyph": "trokaltadjul",
            "part": "Adjective (Phys.)",
            "definition": "Plagued with nightmares, afraid"
          },
          {
            "name_roman": "Trokalto",
            "name_glyph": "trokalto",
            "part": "Adjective (Both)",
            "definition": "Nightmarish"
          },
          {
            "name_roman": "Trokaltau",
            "name_glyph": "trokaltau",
            "part": "Adjective (Conc.)",
            "definition": "Frightening"
          },
          {
            "name_roman": "Trosenafa",
            "name_glyph": "trosenafa",
            "part": "Noun (Conc.)",
            "definition": "Madness, insanity"
          },
          {
            "name_roman": "Trosenafad",
            "name_glyph": "trosenafad",
            "part": "Verb",
            "definition": "To be crazy; to craze"
          },
          {
            "name_roman": "Trosenafadle",
            "name_glyph": "trosenafadle",
            "part": "Noun (Phys.)",
            "definition": "Drug"
          },
          {
            "name_roman": "Trosenafadne",
            "name_glyph": "trosenafadne",
            "part": "Noun (Phys.)",
            "definition": "Maddener"
          },
          {
            "name_roman": "Trosenafau",
            "name_glyph": "trosenafau",
            "part": "Adjective (Both)",
            "definition": "Insane, crazy"
          },
          {
            "name_roman": "Trosenafauesh",
            "name_glyph": "trosenafauesh",
            "part": "Noun (Phys.)",
            "definition": "Crazy person"
          },
          {
            "name_roman": "Trojevala''a",
            "name_glyph": "trojevala''A",
            "part": "Noun (Conc.)",
            "definition": "Cult"
          },
          {
            "name_roman": "Trojevala''ad",
            "name_glyph": "trojevala''Ad",
            "part": "Verb",
            "definition": "To worship in a poor way, especially cult-like"
          },
          {
            "name_roman": "Trojevala''adne",
            "name_glyph": "trojevala''Adne",
            "part": "Noun (Phys.)",
            "definition": "Cultist, fanatic"
          }
        ]
      },
      "g": {
        "glyph": "g",
        "words": [
          {
            "name_roman": "Gomma",
            "name_glyph": "gomma",
            "part": "Noun (Conc.)",
            "definition": "The past"
          },
          {
            "name_roman": "Gommad",
            "name_glyph": "gommad",
            "part": "Verb",
            "definition": "To return, go back"
          },
          {
            "name_roman": "Gommau",
            "name_glyph": "gommau",
            "part": "Adjective (Both)",
            "definition": "Previous"
          }
        ]
      },
      "k": {
        "glyph": "k",
        "words": [
          {
            "name_roman": "Kalt",
            "name_glyph": "kalt",
            "part": "Noun (Conc.)",
            "definition": "Dream"
          },
          {
            "name_roman": "Kalta",
            "name_glyph": "kalta",
            "part": "Noun (Conc.)",
            "definition": "Sleep"
          },
          {
            "name_roman": "Kaltad",
            "name_glyph": "kaltad",
            "part": "Verb",
            "definition": "To sleep, dream"
          },
          {
            "name_roman": "Kaltadle",
            "name_glyph": "kaltadle",
            "part": "Noun (Phys.)",
            "definition": "Bed"
          },
          {
            "name_roman": "Kaltadne",
            "name_glyph": "kaltadne",
            "part": "Noun (Phys.)",
            "definition": "Sleeper, dreamer"
          },
          {
            "name_roman": "Kaltadjul",
            "name_glyph": "kaltadjul",
            "part": "Adjective (Phys.)",
            "definition": "Lucid"
          },
          {
            "name_roman": "Kalto",
            "name_glyph": "kalto",
            "part": "Adjective (Both)",
            "definition": "Dreamlike"
          },
          {
            "name_roman": "Kell",
            "name_glyph": "kell",
            "part": "Noun (Phys.)",
            "definition": "Grass"
          },
          {
            "name_roman": "Kella",
            "name_glyph": "kella",
            "part": "Noun (Phys.)",
            "definition": "Field, meadow"
          },
          {
            "name_roman": "Kellad",
            "name_glyph": "kellad",
            "part": "Verb",
            "definition": "To till a field"
          },
          {
            "name_roman": "Kelladle",
            "name_glyph": "kelladle",
            "part": "Noun (Phys.)",
            "definition": "Plow"
          },
          {
            "name_roman": "Kelladne",
            "name_glyph": "kelladne",
            "part": "Noun (Phys.)",
            "definition": "Plower"
          },
          {
            "name_roman": "Kelladjul",
            "name_glyph": "kelladjul",
            "part": "Adjective (Both)",
            "definition": "Tilled (Phys.), blank, as a slate (Conc.)"
          },
          {
            "name_roman": "Kello",
            "name_glyph": "kello",
            "part": "Adjective (Phys.)",
            "definition": "Grassy"
          },
          {
            "name_roman": "Kellau",
            "name_glyph": "kellau",
            "part": "Adjective (Phys.)",
            "definition": "Meadowy"
          },
          {
            "name_roman": "Kellauta",
            "name_glyph": "kellauta",
            "part": "Noun (Conc.)",
            "definition": "The harvest, uproot"
          },
          {
            "name_roman": "Kellautad",
            "name_glyph": "kellautad",
            "part": "Verb",
            "definition": "To harvest"
          },
          {
            "name_roman": "Kellautadle",
            "name_glyph": "kellautadle",
            "part": "Noun (Phys.)",
            "definition": "Sickle, scythe"
          },
          {
            "name_roman": "Kellautadne",
            "name_glyph": "kellautadne",
            "part": "Adjective (Phys.)",
            "definition": "Ripe"
          },
          {
            "name_roman": "Kikata",
            "name_glyph": "kikata",
            "part": "Noun (Conc.)",
            "definition": "Mediocrity"
          },
          {
            "name_roman": "Kikatau",
            "name_glyph": "kikatau",
            "part": "Adjective (Both)",
            "definition": "Mediocre, average"
          },
          {
            "name_roman": "Kivycro",
            "name_glyph": "kivycro",
            "part": "Noun (Phys.)",
            "definition": "Milk"
          },
          {
            "name_roman": "Kivycroa",
            "name_glyph": "kivycroa",
            "part": "Noun (Phys.)",
            "definition": "Breast, udder"
          },
          {
            "name_roman": "Kivycroad",
            "name_glyph": "kivycroad",
            "part": "Verb",
            "definition": "To breastfeed; to nourish"
          },
          {
            "name_roman": "Kivycroadle",
            "name_glyph": "kivycroadle",
            "part": "Noun (Phys.)",
            "definition": "Breast pump"
          },
          {
            "name_roman": "Kivycroadne",
            "name_glyph": "kivycroadne",
            "part": "Noun (Phys.)",
            "definition": "Nourisher, caretaker"
          },
          {
            "name_roman": "Kivycroa''esh",
            "name_glyph": "kivycroa''Esh",
            "part": "Noun (Phys.)",
            "definition": "Biological mother"
          },
          {
            "name_roman": "Koix",
            "name_glyph": "koix",
            "part": "Pronoun",
            "definition": "That (demonstrative singular)"
          },
          {
            "name_roman": "Koylja",
            "name_glyph": "koylja",
            "part": "Pronoun",
            "definition": "Those (demonstrative plural)"
          },
          {
            "name_roman": "Kuit",
            "name_glyph": "kuit",
            "part": "Noun (Conc.)",
            "definition": "Sight"
          },
          {
            "name_roman": "Kuita",
            "name_glyph": "kuita",
            "part": "Noun (Phys.)",
            "definition": "Eye"
          },
          {
            "name_roman": "Kuitad",
            "name_glyph": "kuitad",
            "part": "Verb",
            "definition": "To see; to show"
          },
          {
            "name_roman": "Kuitadle",
            "name_glyph": "kuitadle",
            "part": "Noun (Phys.)",
            "definition": "Image, camera, screen"
          },
          {
            "name_roman": "Kuitadne",
            "name_glyph": "kuitadne",
            "part": "Noun (Phys.)",
            "definition": "Seer, demonstrator"
          },
          {
            "name_roman": "Kuitadjul",
            "name_glyph": "kuitadjul",
            "part": "Adjective (Phys.)",
            "definition": "Shown"
          },
          {
            "name_roman": "Kuita''a",
            "name_glyph": "kuita''A",
            "part": "Noun (Phys.)",
            "definition": "Color"
          },
          {
            "name_roman": "Kuita''ad",
            "name_glyph": "kuita''Ad",
            "part": "Verb",
            "definition": "To have color; to give color, colorize"
          },
          {
            "name_roman": "Kuita''adle",
            "name_glyph": "kuita''Adle",
            "part": "Noun (Phys.)",
            "definition": "Pigment"
          },
          {
            "name_roman": "Kuita''adne",
            "name_glyph": "kuita''Adne",
            "part": "Noun (Phys.)",
            "definition": "Painter"
          },
          {
            "name_roman": "Kuita''au",
            "name_glyph": "kuita''Au",
            "part": "Adjective (Phys.)",
            "definition": "Colorful"
          },
          {
            "name_roman": "Kuito",
            "name_glyph": "kuito",
            "part": "Adjective (Both)",
            "definition": "Visible (Phys.), apparent (Conc.)"
          },
          {
            "name_roman": "Krest",
            "name_glyph": "krest",
            "part": "Noun (Phys.)",
            "definition": "Moon"
          },
          {
            "name_roman": "Kresta",
            "name_glyph": "kresta",
            "part": "Noun (Conc.)",
            "definition": "Moonlight"
          },
          {
            "name_roman": "Krestad",
            "name_glyph": "krestad",
            "part": "Verb",
            "definition": "To shimmer"
          },
          {
            "name_roman": "Krestadle",
            "name_glyph": "krestadle",
            "part": "Noun (Phys.)",
            "definition": "Glitter"
          },
          {
            "name_roman": "Krestadne",
            "name_glyph": "krestadne",
            "part": "Noun (Phys.)",
            "definition": "Shimmerer"
          },
          {
            "name_roman": "Krestadjul",
            "name_glyph": "krestadjul",
            "part": "Adjective (Phys.)",
            "definition": "Shiny, glimmery"
          },
          {
            "name_roman": "Kresto",
            "name_glyph": "kresto",
            "part": "Adjective (Phys.)",
            "definition": "Moonlit"
          }
        ]
      },
      "c": {
        "glyph": "c",
        "words": [
          {
            "name_roman": "Cri",
            "name_glyph": "cri",
            "part": "Noun (Conc.)",
            "definition": "The cold"
          },
          {
            "name_roman": "Crid",
            "name_glyph": "crid",
            "part": "Verb",
            "definition": "To become cold; to chill"
          },
          {
            "name_roman": "Cridle",
            "name_glyph": "cridle",
            "part": "Noun (Phys.)",
            "definition": "Cooler"
          },
          {
            "name_roman": "Cridne",
            "name_glyph": "cridne",
            "part": "Noun (Phys.)",
            "definition": "Refrigerator"
          },
          {
            "name_roman": "Cridjul",
            "name_glyph": "cridjul",
            "part": "Adjective (Phys.)",
            "definition": "Chilly"
          },
          {
            "name_roman": "Cridjulesh",
            "name_glyph": "cridjulesh",
            "part": "Noun (Conc.)",
            "definition": "Coolness"
          },
          {
            "name_roman": "Cri''a",
            "name_glyph": "cri''A",
            "part": "Noun (Phys.)",
            "definition": "Ice"
          },
          {
            "name_roman": "Cri''ad",
            "name_glyph": "cri''Ad",
            "part": "Verb",
            "definition": "To be frozen; to freeze"
          },
          {
            "name_roman": "Cri''adle",
            "name_glyph": "cri''Adle",
            "part": "Noun (Phys.)",
            "definition": "Icebox"
          },
          {
            "name_roman": "Cri''adne",
            "name_glyph": "cri''Adne",
            "part": "Noun (Phys.)",
            "definition": "Freezer"
          },
          {
            "name_roman": "Cri''o",
            "name_glyph": "cri''O",
            "part": "Adjective (Phys.)",
            "definition": "Cold"
          },
          {
            "name_roman": "Cri''au",
            "name_glyph": "cri''Au",
            "part": "Adjective (Phys.)",
            "definition": "Frozen"
          },
          {
            "name_roman": "Cro",
            "name_glyph": "cro",
            "part": "Noun (Phys.)",
            "definition": "Water"
          },
          {
            "name_roman": "Croa",
            "name_glyph": "croa",
            "part": "Noun (Phys.)",
            "definition": "Body of water"
          },
          {
            "name_roman": "Croad",
            "name_glyph": "croad",
            "part": "Verb",
            "definition": "To become full; to fill"
          },
          {
            "name_roman": "Croadle",
            "name_glyph": "croadle",
            "part": "Noun (Phys.)",
            "definition": "Faucet"
          },
          {
            "name_roman": "Croadne",
            "name_glyph": "croadne",
            "part": "Noun (Phys.)",
            "definition": "Filler, pump"
          },
          {
            "name_roman": "Croadjul",
            "name_glyph": "croadjul",
            "part": "Adjective (Phys.)",
            "definition": "Flooded, submerged"
          },
          {
            "name_roman": "Croadjulesh",
            "name_glyph": "croajulesh",
            "part": "Noun (Phys.)",
            "definition": "Flood"
          },
          {
            "name_roman": "Croafa",
            "name_glyph": "croafa",
            "part": "Noun (Conc.)",
            "definition": "Flow"
          },
          {
            "name_roman": "Croafad",
            "name_glyph": "croafad",
            "part": "Verb",
            "definition": "To flow; to ease"
          },
          {
            "name_roman": "Croafadle",
            "name_glyph": "croafadle",
            "part": "Noun (Phys.)",
            "definition": "Pipe, tube"
          },
          {
            "name_roman": "Croafadne",
            "name_glyph": "croafadne",
            "part": "Noun (Phys.)",
            "definition": "Easer, one that flows"
          },
          {
            "name_roman": "Croo",
            "name_glyph": "croo",
            "part": "Adjective (Phys.)",
            "definition": "Wet"
          },
          {
            "name_roman": "Croo''esh",
            "name_glyph": "croo''Esh",
            "part": "Noun (Phys.)",
            "definition": "Wetness, moisture"
          },
          {
            "name_roman": "Crod",
            "name_glyph": "crod",
            "part": "Verb",
            "definition": "To become wet; to soak"
          },
          {
            "name_roman": "Crodle",
            "name_glyph": "crodle",
            "part": "Noun (Phys.)",
            "definition": "Sink"
          },
          {
            "name_roman": "Crodne",
            "name_glyph": "crodne",
            "part": "Noun (Phys.)",
            "definition": "Soaker"
          },
          {
            "name_roman": "Crodjul",
            "name_glyph": "crodjul",
            "part": "Adjective (Phys.)",
            "definition": "Soaked"
          },
          {
            "name_roman": "Cromyr",
            "name_glyph": "cromyr",
            "part": "Noun (Phys.)",
            "definition": "Ocean current"
          },
          {
            "name_roman": "Crufa",
            "name_glyph": "crufa",
            "part": "Number",
            "definition": "Four"
          },
          {
            "name_roman": "Crufaesh",
            "name_glyph": "crufaesh",
            "part": "Noun (Conc.)",
            "definition": "Square"
          }
        ]
      },
      "z": {
        "glyph": "z",
        "words": [
          {
            "name_roman": "Zal",
            "name_glyph": "zal",
            "part": "Noun (Phys.)",
            "definition": "Intercourse"
          },
          {
            "name_roman": "Zala",
            "name_glyph": "zala",
            "part": "Noun (Phys.)",
            "definition": "Genitalia"
          },
          {
            "name_roman": "Zalad",
            "name_glyph": "zalad",
            "part": "Verb",
            "definition": "To have sex"
          },
          {
            "name_roman": "Zaladle",
            "name_glyph": "zaladle",
            "part": "Noun (Phys.)",
            "definition": "Sex toy"
          },
          {
            "name_roman": "Zaladne",
            "name_glyph": "zaladne",
            "part": "Noun (Phys.)",
            "definition": "Sex worker, sexual partner"
          },
          {
            "name_roman": "Zaladjul",
            "name_glyph": "zaladjul",
            "part": "Adjective (Phys.)",
            "definition": "Aroused"
          },
          {
            "name_roman": "Zaladjulesh",
            "name_glyph": "zaladjulesh",
            "part": "Noun (Conc.)",
            "definition": "Libido"
          },
          {
            "name_roman": "Zalo",
            "name_glyph": "zalo",
            "part": "Adjective (Both)",
            "definition": "Sexual"
          },
          {
            "name_roman": "Zalau",
            "name_glyph": "zalau",
            "part": "Adjective (Phys.)",
            "definition": "Reproductive"
          },
          {
            "name_roman": "Zalpreita",
            "name_glyph": "zalpreita",
            "part": "Noun (Conc.)",
            "definition": "Lust"
          },
          {
            "name_roman": "Zett",
            "name_glyph": "zett",
            "part": "Noun (Both)",
            "definition": "Stuff, matter, things"
          },
          {
            "name_roman": "Zetta",
            "name_glyph": "zetta",
            "part": "Noun (Conc.)",
            "definition": "Creation (as the act of creating)"
          },
          {
            "name_roman": "Zettad",
            "name_glyph": "zettad",
            "part": "Verb",
            "definition": "To create"
          },
          {
            "name_roman": "Zettadne",
            "name_glyph": "zettadne",
            "part": "Noun (Phys.)",
            "definition": "Creator"
          },
          {
            "name_roman": "Zettadneo",
            "name_glyph": "zettadneo",
            "part": "Adjective (Both)",
            "definition": "Creative"
          },
          {
            "name_roman": "Zettadjul",
            "name_glyph": "zettadjul",
            "part": "Adjective (Both)",
            "definition": "Created"
          },
          {
            "name_roman": "Zettadjulesh",
            "name_glyph": "zettadjulesh",
            "part": "Noun (Phys.)",
            "definition": "Creation (as something that has been created)"
          },
          {
            "name_roman": "Zettau",
            "name_glyph": "zettau",
            "part": "Adjective (Both)",
            "definition": "Original"
          },
          {
            "name_roman": "Zjal",
            "name_glyph": "zjal",
            "part": "Number",
            "definition": "Five"
          },
          {
            "name_roman": "Zjalesh",
            "name_glyph": "zjalesh",
            "part": "Noun (Conc.)",
            "definition": "Pentagon"
          }
        ]
      },
      "s": {
        "glyph": "s",
        "words": [
          {
            "name_roman": "Sen",
            "name_glyph": "sen",
            "part": "Noun (Conc.)",
            "definition": "Sharpness"
          },
          {
            "name_roman": "Senad",
            "name_glyph": "senad",
            "part": "Verb",
            "definition": "To cut"
          },
          {
            "name_roman": "Senadle",
            "name_glyph": "senadle",
            "part": "Noun (Phys.)",
            "definition": "Knife, scissors, trimmer"
          },
          {
            "name_roman": "Senadne",
            "name_glyph": "senadne",
            "part": "Noun (Phys.)",
            "definition": "Slicer, cutter"
          },
          {
            "name_roman": "Senadjul",
            "name_glyph": "senadjul",
            "part": "Adjective (Both)",
            "definition": "Halved, cleaved, slashed"
          },
          {
            "name_roman": "Senafa",
            "name_glyph": "senafa",
            "part": "Noun (Conc.)",
            "definition": "Intelligence"
          },
          {
            "name_roman": "Senafad",
            "name_glyph": "senafad",
            "part": "Verb",
            "definition": "To learn; to teach"
          },
          {
            "name_roman": "Senafadle",
            "name_glyph": "senafadle",
            "part": "Noun (Phys.)",
            "definition": "Textbook"
          },
          {
            "name_roman": "Senafadne",
            "name_glyph": "senafadne",
            "part": "Noun (Phys.)",
            "definition": "Teacher, pupil"
          },
          {
            "name_roman": "Senafa''a",
            "name_glyph": "senafa''A",
            "part": "Noun (Conc.)",
            "definition": "Mind"
          },
          {
            "name_roman": "Senafa''ad",
            "name_glyph": "senafa''Ad",
            "part": "Verb",
            "definition": "To think"
          },
          {
            "name_roman": "Senafa''ada",
            "name_glyph": "senafa''Ada",
            "part": "Noun (Conc.)",
            "definition": "Thought"
          },
          {
            "name_roman": "Senafa''adau",
            "name_glyph": "senafa''Adau",
            "part": "Adjective (Conc.)",
            "definition": "Thoughtful"
          },
          {
            "name_roman": "Senafa''adne",
            "name_glyph": "senafa''Adne",
            "part": "Noun (Phys.)",
            "definition": "Thinker"
          },
          {
            "name_roman": "Senafa''aurok",
            "name_glyph": "senafa''Aurok",
            "part": "Noun (Phys.)",
            "definition": "Brain"
          },
          {
            "name_roman": "Senafuma",
            "name_glyph": "senafuma",
            "part": "Noun (Conc.)",
            "definition": "Speech"
          },
          {
            "name_roman": "Senafumad",
            "name_glyph": "senafumad",
            "part": "Verb",
            "definition": "To speak"
          },
          {
            "name_roman": "Senafumadle",
            "name_glyph": "senafumadle",
            "part": "Noun (Phys.)",
            "definition": "Speaker (technology)"
          },
          {
            "name_roman": "Senafumadne",
            "name_glyph": "senafumadne",
            "part": "Noun (Phys.)",
            "definition": "Speaker (person), orator"
          },
          {
            "name_roman": "Senafuma''a",
            "name_glyph": "senafuma''A",
            "part": "Noun (Conc.)",
            "definition": "Language"
          },
          {
            "name_roman": "Senafuma''ad",
            "name_glyph": "senafuma''Ad",
            "part": "Verb",
            "definition": "To converse"
          },
          {
            "name_roman": "Senafuma''adle",
            "name_glyph": "senafuma''Adle",
            "part": "Noun (Phys.)",
            "definition": "Tongue"
          },
          {
            "name_roman": "Senafuma''adne",
            "name_glyph": "senafuma''Adne",
            "part": "Noun (Phys.)",
            "definition": "Translator, conversationalist"
          },
          {
            "name_roman": "Senafau",
            "name_glyph": "senafau",
            "part": "Adjective (Both)",
            "definition": "Intelligent, smart"
          },
          {
            "name_roman": "Seno",
            "name_glyph": "seno",
            "part": "Adjective (Both)",
            "definition": "Sharp"
          },
          {
            "name_roman": "Senurok",
            "name_glyph": "senurok",
            "part": "Noun (Phys.)",
            "definition": "Blade"
          },
          {
            "name_roman": "Skar",
            "name_glyph": "skar",
            "part": "Noun (Phys.)",
            "definition": "Skull"
          }
        ]
      },
      "sh": {
        "glyph": "sh",
        "words": [
          {
            "name_roman": "Shen",
            "name_glyph": "shen",
            "part": "Noun (Phys.)",
            "definition": "Metal"
          },
          {
            "name_roman": "Shena",
            "name_glyph": "shena",
            "part": "Noun (Phys.)",
            "definition": "Alloy"
          },
          {
            "name_roman": "Shenad",
            "name_glyph": "shenad",
            "part": "Verb",
            "definition": "To alloy"
          },
          {
            "name_roman": "Shenadle",
            "name_glyph": "shenadle",
            "part": "Noun (Phys.)",
            "definition": "Forge"
          },
          {
            "name_roman": "Shenadne",
            "name_glyph": "shenadne",
            "part": "Noun (Phys.)",
            "definition": "Alloyer"
          },
          {
            "name_roman": "Shenaashuroesh",
            "name_glyph": "shenaashuroesh",
            "part": "Noun (Phys.)",
            "definition": "Steel"
          },
          {
            "name_roman": "Shenaashuroesho",
            "name_glyph": "shenaashuroesho",
            "part": "Adjective (Phys.)",
            "definition": "Steel"
          },
          {
            "name_roman": "Shenashuroesh",
            "name_glyph": "shenashuroesh",
            "part": "Noun (Phys.)",
            "definition": "Iron"
          },
          {
            "name_roman": "Shenashuroesho",
            "name_glyph": "shenashuroesho",
            "part": "Adjective (Phys.)",
            "definition": "Ferrous"
          },
          {
            "name_roman": "Shenafa",
            "name_glyph": "shenafa",
            "part": "Noun (Conc.)",
            "definition": "Smithing"
          },
          {
            "name_roman": "Shenafad",
            "name_glyph": "shenafad",
            "part": "Verb",
            "definition": "To smith, forge"
          },
          {
            "name_roman": "Shenafadl",
            "name_glyph": "shenafadle",
            "part": "Noun (Phys.)",
            "definition": "Anvil"
          },
          {
            "name_roman": "Shenafadne",
            "name_glyph": "shenafadne",
            "part": "Noun (Phys.)",
            "definition": "Blacksmith"
          },
          {
            "name_roman": "Shenafadjul",
            "name_glyph": "shenafadjul",
            "part": "Adjective (Phys.)",
            "definition": "Forged"
          },
          {
            "name_roman": "Shenafadjulesh",
            "name_glyph": "shenafadjulesh",
            "part": "Noun (Phys.)",
            "definition": "Work of smithing"
          },
          {
            "name_roman": "Sheno",
            "name_glyph": "sheno",
            "part": "Adjective (Phys.)",
            "definition": "Metallic"
          },
          {
            "name_roman": "Shenborkuita''a",
            "name_glyph": "shenborkuita''A",
            "part": "Noun (Phys.)",
            "definition": "Copper"
          },
          {
            "name_roman": "Shenborkuita''aa",
            "name_glyph": "shenborkuita''Aa",
            "part": "Noun (Conc.)",
            "definition": "Oxidization"
          },
          {
            "name_roman": "Shenborkuita''aad",
            "name_glyph": "shenborkuita''Aad",
            "part": "Verb",
            "definition": "To oxidize"
          },
          {
            "name_roman": "Shenborkuita''aadne",
            "name_glyph": "shenborkuita''Aadne",
            "part": "Noun (Phys.)",
            "definition": "Oxidizer"
          },
          {
            "name_roman": "Shenpreit",
            "name_glyph": "shenpreit",
            "part": "Noun (Phys.)",
            "definition": "Gold"
          },
          {
            "name_roman": "Shenpreita",
            "name_glyph": "shenpreita",
            "part": "Noun (Conc.)",
            "definition": "Currency"
          },
          {
            "name_roman": "Shenpreitaa",
            "name_glyph": "shenpreitaa",
            "part": "Noun (Conc.)",
            "definition": "Economy"
          },
          {
            "name_roman": "Shenpreitad",
            "name_glyph": "shenpreitad",
            "part": "Verb",
            "definition": "To pay; to buy"
          },
          {
            "name_roman": "Shenpreitadle",
            "name_glyph": "shenpreitadle",
            "part": "Noun (Phys.)",
            "definition": "Coin"
          },
          {
            "name_roman": "Shenpreitadne",
            "name_glyph": "shenpreitadne",
            "part": "Noun (Phys.)",
            "definition": "Buyer, client"
          },
          {
            "name_roman": "Shenpreito",
            "name_glyph": "shenpreito",
            "part": "Adjective (Phys.)",
            "definition": "Golden, yellow"
          },
          {
            "name_roman": "Shenpreitoesh",
            "name_glyph": "shenpreitoesh",
            "part": "Noun (Conc.)",
            "definition": "The colors gold, yellow"
          },
          {
            "name_roman": "Shenpreitva",
            "name_glyph": "shenpreitva",
            "part": "Noun (Phys.)",
            "definition": "Platinum"
          },
          {
            "name_roman": "Shenkipreit",
            "name_glyph": "shenkipreit",
            "part": "Noun (Phys.)",
            "definition": "Silver"
          },
          {
            "name_roman": "Shenkipreito",
            "name_glyph": "shenkipreito",
            "part": "Adjective (Phys.)",
            "definition": "Silvery"
          },
          {
            "name_roman": "Shenkipreitoesh",
            "name_glyph": "shenkipreitoesh",
            "part": "Noun (Conc.)",
            "definition": "The color silver"
          },
          {
            "name_roman": "Shentrosenafa",
            "name_glyph": "shentrosenafa",
            "part": "Noun (Phys.)",
            "definition": "Mercury, quicksilver"
          },
          {
            "name_roman": "Shenvelt",
            "name_glyph": "shenvelt",
            "part": "Noun (Phys.)",
            "definition": "Invention"
          },
          {
            "name_roman": "Shenvelta",
            "name_glyph": "shenvelta",
            "part": "Noun (Conc.)",
            "definition": "Tinkering"
          },
          {
            "name_roman": "Shenveltad",
            "name_glyph": "shenveltad",
            "part": "Verb",
            "definition": "To tinker"
          },
          {
            "name_roman": "Shenveltadle",
            "name_glyph": "shenveltadle",
            "part": "Noun (Phys.)",
            "definition": "Screwdriver"
          },
          {
            "name_roman": "Shenveltadne",
            "name_glyph": "shenveltadne",
            "part": "Noun (Phys.)",
            "definition": "Tinker"
          },
          {
            "name_roman": "Shenveltadjul",
            "name_glyph": "shenveltadjul",
            "part": "Adjective (Phys.)",
            "definition": "Finely crafted"
          },
          {
            "name_roman": "Shenveltadjulesh",
            "name_glyph": "shenveltadjulesh",
            "part": "Noun (Phys.)",
            "definition": "Masterpiece"
          },
          {
            "name_roman": "Shiix",
            "name_glyph": "shiix",
            "part": "Pronoun",
            "definition": "This (demonstrative singular)"
          },
          {
            "name_roman": "Shon",
            "name_glyph": "shon",
            "part": "Number",
            "definition": "Three"
          },
          {
            "name_roman": "Shonesh",
            "name_glyph": "shonesh",
            "part": "Noun (Conc.)",
            "definition": "Triangle"
          },
          {
            "name_roman": "Shur",
            "name_glyph": "shur",
            "part": "Noun (Conc.)",
            "definition": "Truth"
          },
          {
            "name_roman": "Shura",
            "name_glyph": "shura",
            "part": "Noun (Conc.)",
            "definition": "Honesty"
          },
          {
            "name_roman": "Shurad",
            "name_glyph": "shurad",
            "part": "Verb",
            "definition": "To tell the truth, admit to"
          },
          {
            "name_roman": "Shuradne",
            "name_glyph": "shuradne",
            "part": "Noun (Phys.)",
            "definition": "Truth-teller"
          },
          {
            "name_roman": "Shuro",
            "name_glyph": "shuro",
            "part": "Adjective (Both)",
            "definition": "Honest, truthful, genuine"
          },
          {
            "name_roman": "Shauvy",
            "name_glyph": "shauvy",
            "part": "Noun (Phys.)",
            "definition": "Boar, pig"
          },
          {
            "name_roman": "Shauvhyjal",
            "name_glyph": "shauvhyjal",
            "part": "Noun (Phys.)",
            "definition": "Pork"
          },
          {
            "name_roman": "Shja",
            "name_glyph": "shja",
            "part": "Noun (Conc.)",
            "definition": "Ecstasy"
          },
          {
            "name_roman": "Shjad",
            "name_glyph": "shjad",
            "part": "Verb",
            "definition": "To feel pleasure; to please"
          },
          {
            "name_roman": "Shjadle",
            "name_glyph": "shjadle",
            "part": "Noun (Phys.)",
            "definition": "Euphoriant"
          },
          {
            "name_roman": "Shjadne",
            "name_glyph": "shjadne",
            "part": "Noun (Phys.)",
            "definition": "Pleasurer"
          },
          {
            "name_roman": "Shjylja",
            "name_glyph": "shjylja",
            "part": "Pronoun",
            "definition": "These (demonstrative plural)"
          },
          {
            "name_roman": "Shjau",
            "name_glyph": "shjau",
            "part": "Adjective (Phys.)",
            "definition": "Ecstatic"
          },
          {
            "name_roman": "Shra",
            "name_glyph": "shra",
            "part": "Adjective (Phys.)",
            "definition": "Ecstatic"
          },
          {
            "name_roman": "Shrad",
            "name_glyph": "shrad",
            "part": "Verb",
            "definition": "To trade"
          },
          {
            "name_roman": "Shradle",
            "name_glyph": "shradle",
            "part": "Noun (Phys.)",
            "definition": "Ware"
          },
          {
            "name_roman": "Shradne",
            "name_glyph": "shradne",
            "part": "Noun (Phys.)",
            "definition": "Trader, merchant"
          },
          {
            "name_roman": "Shra''a",
            "name_glyph": "shra''A",
            "part": "Noun (Conc.)",
            "definition": "Agreement (of a deal)"
          },
          {
            "name_roman": "Shrau",
            "name_glyph": "shrau",
            "part": "Adjective (Conc.)",
            "definition": "Fair"
          }
        ]
      },
      "l": {
        "glyph": "l",
        "words": [
          {
            "name_roman": "Le''ad",
            "name_glyph": "le''Ad",
            "part": "Verb",
            "definition": "To be (conceptually)"
          },
          {
            "name_roman": "Le''ada",
            "name_glyph": "le''Ada",
            "part": "Noun (Conc.)",
            "definition": "Ideation"
          },
          {
            "name_roman": "Le''iz",
            "name_glyph": "le''Iz",
            "part": "Noun (Conc.)",
            "definition": "Amazement"
          },
          {
            "name_roman": "Le''izad",
            "name_glyph": "le''Izad",
            "part": "Verb",
            "definition": "To amaze"
          },
          {
            "name_roman": "Le''izadjul",
            "name_glyph": "le''Izadjul",
            "part": "Adjective (Both)",
            "definition": "Amazing"
          },
          {
            "name_roman": "Le''izo",
            "name_glyph": "le''Izo",
            "part": "Adjective (Phys.)",
            "definition": "Amazed"
          },
          {
            "name_roman": "Liffa",
            "name_glyph": "liffa",
            "part": "Noun (Conc.)",
            "definition": "Decay"
          },
          {
            "name_roman": "Liffad",
            "name_glyph": "liffad",
            "part": "Verb",
            "definition": "To rot; to corrode"
          },
          {
            "name_roman": "Liffadne",
            "name_glyph": "liffadne",
            "part": "Noun (Phys.)",
            "definition": "Acid, corroder, rotter"
          },
          {
            "name_roman": "Liffadjul",
            "name_glyph": "liffadjul",
            "part": "Adjective (Phys.)",
            "definition": "Rotten, necrotic"
          },
          {
            "name_roman": "Liffau",
            "name_glyph": "liffau",
            "part": "Adjective (Both)",
            "definition": "Decayed (Phys.), waned (Conc.)"
          },
          {
            "name_roman": "Lunt",
            "name_glyph": "lunt",
            "part": "Noun (Phys.)",
            "definition": "Land, ground"
          },
          {
            "name_roman": "Lunta",
            "name_glyph": "lunta",
            "part": "Noun (Phys.)",
            "definition": "Landmass"
          },
          {
            "name_roman": "Lunto",
            "name_glyph": "lunto",
            "part": "Adjective (Phys.)",
            "definition": "Earthy"
          },
          {
            "name_roman": "Lyg",
            "name_glyph": "lyg",
            "part": "Noun (Conc.)",
            "definition": "Home"
          },
          {
            "name_roman": "Lyga",
            "name_glyph": "lyga",
            "part": "Noun (Phys.)",
            "definition": "House"
          },
          {
            "name_roman": "Lyga''a",
            "name_glyph": "lyga''A",
            "part": "Noun (Phys.)",
            "definition": "Town"
          },
          {
            "name_roman": "Lygo",
            "name_glyph": "lygo",
            "part": "Adjective (Both)",
            "definition": "Homely"
          },
          {
            "name_roman": "Lyga''au",
            "name_glyph": "lyga''Au",
            "part": "Adjective (Both)",
            "definition": "Communal"
          }
        ]
      },
      "m": {
        "glyph": "m",
        "words": [
          {
            "name_roman": "Met",
            "name_glyph": "met",
            "part": "Noun (Conc.)",
            "definition": "Month"
          },
          {
            "name_roman": "Mik",
            "name_glyph": "mik",
            "part": "Noun (Phys.)",
            "definition": "Rock"
          },
          {
            "name_roman": "Mika",
            "name_glyph": "mika",
            "part": "Noun (Phys.)",
            "definition": "The ground"
          },
          {
            "name_roman": "Mikad",
            "name_glyph": "mikad",
            "part": "Verb",
            "definition": "To become petrified; to petrify"
          },
          {
            "name_roman": "Mikadne",
            "name_glyph": "mikadne",
            "part": "Noun (Phys.)",
            "definition": "Petrifier"
          },
          {
            "name_roman": "Mikadjul",
            "name_glyph": "mikadjul",
            "part": "Adjective (Phys.)",
            "definition": "Petrified"
          },
          {
            "name_roman": "Mikavy",
            "name_glyph": "mikavy",
            "part": "Noun (Phys.)",
            "definition": "Worm"
          },
          {
            "name_roman": "Mikavyo",
            "name_glyph": "mikavyo",
            "part": "Adjective (Phys.)",
            "definition": "Wiggly"
          },
          {
            "name_roman": "Mikavyoesh",
            "name_glyph": "mikavyoesh",
            "part": "Noun (Phys.)",
            "definition": "Noodle"
          },
          {
            "name_roman": "Mikavyji''ana",
            "name_glyph": "mikavyji''Ana",
            "part": "Noun (Phys.)",
            "definition": "Finger"
          },
          {
            "name_roman": "Miko",
            "name_glyph": "miko",
            "part": "Adjective (Phys.)",
            "definition": "Rocky, stony"
          },
          {
            "name_roman": "Mikussauesh",
            "name_glyph": "mikussauesh",
            "part": "Noun (Phys.)",
            "definition": "Mountain"
          },
          {
            "name_roman": "Mikussauesha",
            "name_glyph": "mikussauesha",
            "part": "Noun (Phys.)",
            "definition": "Mountain range"
          },
          {
            "name_roman": "Mikau",
            "name_glyph": "mikau",
            "part": "Adjective (Phys.)",
            "definition": "Solid"
          },
          {
            "name_roman": "Mikrus",
            "name_glyph": "mikrus",
            "part": "Noun (Phys.)",
            "definition": "Seed (as of a plant)"
          },
          {
            "name_roman": "Mikrusafa",
            "name_glyph": "mikrusafa",
            "part": "Noun (Conc.)",
            "definition": "Seed (as the root from which a concept germinates)"
          },
          {
            "name_roman": "Myr",
            "name_glyph": "myr",
            "part": "Noun (Phys.)",
            "definition": "Wind"
          },
          {
            "name_roman": "Myra",
            "name_glyph": "myra",
            "part": "Noun (Conc.)",
            "definition": "Breath"
          },
          {
            "name_roman": "Myrad",
            "name_glyph": "myrad",
            "part": "Verb",
            "definition": "To breathe; to resuscitate"
          },
          {
            "name_roman": "Myradne",
            "name_glyph": "myradne",
            "part": "Noun (Phys.)",
            "definition": "Breather, ventilator"
          },
          {
            "name_roman": "Myra''a",
            "name_glyph": "myra''A",
            "part": "Noun (Phys.)",
            "definition": "Lung"
          },
          {
            "name_roman": "Myro",
            "name_glyph": "myro",
            "part": "Adjective (Phys.)",
            "definition": "Swift"
          },
          {
            "name_roman": "Mjot",
            "name_glyph": "mjot",
            "part": "Noun (Phys.)",
            "definition": "Guest"
          },
          {
            "name_roman": "Mjotad",
            "name_glyph": "mjotad",
            "part": "Verb",
            "definition": "To visit"
          },
          {
            "name_roman": "Mjotadle",
            "name_glyph": "mjotadle",
            "part": "Noun (Phys.)",
            "definition": "Ticket"
          },
          {
            "name_roman": "Mjotadne",
            "name_glyph": "mjotadne",
            "part": "Noun (Phys.)",
            "definition": "Visitor, tourist"
          },
          {
            "name_roman": "Mjotadjul",
            "name_glyph": "mjotadjul",
            "part": "Adjective (Phys.)",
            "definition": "Nomadic"
          }
        ]
      },
      "n": {
        "glyph": "n",
        "words": [
          {
            "name_roman": "Nar",
            "name_glyph": "nar",
            "part": "Noun (Conc.)",
            "definition": "Feeling, sensation"
          },
          {
            "name_roman": "Nara",
            "name_glyph": "nara",
            "part": "Noun (Conc.)",
            "definition": "Emotion"
          },
          {
            "name_roman": "Narad",
            "name_glyph": "narad",
            "part": "Verb",
            "definition": "To feel an emotion; to induce an emotion"
          },
          {
            "name_roman": "Naradne",
            "name_glyph": "naradne",
            "part": "Noun (Phys.)",
            "definition": "Empath"
          },
          {
            "name_roman": "Naradjul",
            "name_glyph": "naradjul",
            "part": "Adjective (Both)",
            "definition": "Emotional"
          },
          {
            "name_roman": "Naro",
            "name_glyph": "naro",
            "part": "Adjective (Phys.)",
            "definition": "Sensory"
          },
          {
            "name_roman": "Narau",
            "name_glyph": "narau",
            "part": "Adjective (Both)",
            "definition": "Pertaining to emotion"
          },
          {
            "name_roman": "Narseno",
            "name_glyph": "narseno",
            "part": "Adjective (Both)",
            "definition": "Psychic"
          },
          {
            "name_roman": "No",
            "name_glyph": "no",
            "part": "Verb",
            "definition": "To be a gift to me (first-person singular)"
          },
          {
            "name_roman": "No''o",
            "name_glyph": "no''O",
            "part": "Verb",
            "definition": "To be a gift to us (first-person plural)"
          },
          {
            "name_roman": "Nu",
            "name_glyph": "nu",
            "part": "Pronoun",
            "definition": "I, me (first-person singular)"
          },
          {
            "name_roman": "Nu''o",
            "name_glyph": "nu''O",
            "part": "Pronoun",
            "definition": "We, us (first-person plural)"
          },
          {
            "name_roman": "Njarvy",
            "name_glyph": "njarvy",
            "part": "Noun (Phys.)",
            "definition": "Cat"
          },
          {
            "name_roman": "Njarvyo",
            "name_glyph": "njarvyo",
            "part": "Adjective (Phys.)",
            "definition": "Feline"
          }
        ]
      },
      "v": {
        "glyph": "v",
        "words": [
          {
            "name_roman": "Val",
            "name_glyph": "val",
            "part": "Noun (Conc.)",
            "definition": "Time"
          },
          {
            "name_roman": "Vala",
            "name_glyph": "vala",
            "part": "Noun (Conc.)",
            "definition": "Mortality, impermanence"
          },
          {
            "name_roman": "Valad",
            "name_glyph": "valad",
            "part": "Verb",
            "definition": "To live a lifespan"
          },
          {
            "name_roman": "Valo",
            "name_glyph": "valo",
            "part": "Adjective (Both)",
            "definition": "Temporal"
          },
          {
            "name_roman": "Valau",
            "name_glyph": "valau",
            "part": "Adjective (Both)",
            "definition": "Mortal, temporary"
          },
          {
            "name_roman": "Valauesh",
            "name_glyph": "valauesh",
            "part": "Noun (Phys.)",
            "definition": "Mortal being"
          },
          {
            "name_roman": "Velt",
            "name_glyph": "velt",
            "part": "Noun (Phys.)",
            "definition": "Artwork"
          },
          {
            "name_roman": "Velta",
            "name_glyph": "velta",
            "part": "Noun (Conc.)",
            "definition": "The arts"
          },
          {
            "name_roman": "Veltad",
            "name_glyph": "veltad",
            "part": "Verb",
            "definition": "To perform, create art"
          },
          {
            "name_roman": "Veltadle",
            "name_glyph": "veltadle",
            "part": "Noun (Phys.)",
            "definition": "Art tool (e.g. paint brush)"
          },
          {
            "name_roman": "Veltadne",
            "name_glyph": "veltadne",
            "part": "Noun (Phys.)",
            "definition": "Artist"
          },
          {
            "name_roman": "Velto",
            "name_glyph": "velto",
            "part": "Adjective (Phys.)",
            "definition": "Having a pleasant or well-defined aesthetic"
          },
          {
            "name_roman": "Veltau",
            "name_glyph": "veltau",
            "part": "Adjective (Both)",
            "definition": "Artistic"
          },
          {
            "name_roman": "Veneska",
            "name_glyph": "veneska",
            "part": "Interrogative",
            "definition": "Where?"
          },
          {
            "name_roman": "Venedi",
            "name_glyph": "vendi",
            "part": "Interrogative",
            "definition": "Who (singular)?"
          },
          {
            "name_roman": "Vendja",
            "name_glyph": "vendja",
            "part": "Interrogative",
            "definition": "Who (plural)?"
          },
          {
            "name_roman": "Venzett",
            "name_glyph": "venzett",
            "part": "Interrogative",
            "definition": "What?"
          },
          {
            "name_roman": "Venle''ada",
            "name_glyph": "venle''Ada",
            "part": "Interrogative",
            "definition": "Why?"
          },
          {
            "name_roman": "Venval",
            "name_glyph": "venval",
            "part": "Interrogative",
            "definition": "When?"
          },
          {
            "name_roman": "Venjyta",
            "name_glyph": "venjyta",
            "part": "Interrogative",
            "definition": "Where?"
          },
          {
            "name_roman": "Ven",
            "name_glyph": "ven",
            "part": "Interrogative",
            "definition": "Which?"
          },
          {
            "name_roman": "Vido",
            "name_glyph": "vido",
            "part": "Adjective (Phys.)",
            "definition": "High quality"
          },
          {
            "name_roman": "Vy",
            "name_glyph": "vy",
            "part": "Noun (Phys.)",
            "definition": "Animal"
          },
          {
            "name_roman": "Vya",
            "name_glyph": "vya",
            "part": "Noun (Conc.)",
            "definition": "Wilderness"
          },
          {
            "name_roman": "Vyad",
            "name_glyph": "vyad",
            "part": "Verb",
            "definition": "To overgrow"
          },
          {
            "name_roman": "Vyadne",
            "name_glyph": "vyadne",
            "part": "Noun (Phys.)",
            "definition": "Weed"
          },
          {
            "name_roman": "Vyadjul",
            "name_glyph": "vyadjul",
            "part": "Adjective (Phys.)",
            "definition": "Overgrown"
          },
          {
            "name_roman": "Vyo",
            "name_glyph": "vyo",
            "part": "Adjective (Both)",
            "definition": "Animalistic, feral (Phys.), instinctual (Conc.)"
          },
          {
            "name_roman": "Vyau",
            "name_glyph": "vyau",
            "part": "Adjective (Both)",
            "definition": "Wild, untamed"
          },
          {
            "name_roman": "Vripjoadau",
            "name_glyph": "vripjoadau",
            "part": "Adjective (Phys.)",
            "definition": "Of a different physical form"
          }
        ]
      },
      "f": {
        "glyph": "f",
        "words": [
          {
            "name_roman": "Fa",
            "name_glyph": "fa",
            "part": "Verb",
            "definition": "To be a gift to them (singular); to give to them (singular)"
          },
          {
            "name_roman": "Fang",
            "name_glyph": "fang",
            "part": "Noun (Phys.)",
            "definition": "Glass"
          },
          {
            "name_roman": "Fango",
            "name_glyph": "fango",
            "part": "Adjective (Phys.)",
            "definition": "Clear"
          },
          {
            "name_roman": "Fangoa",
            "name_glyph": "fangoa",
            "part": "Noun (Conc.)",
            "definition": "Clarity"
          },
          {
            "name_roman": "Fangoakuit",
            "name_glyph": "fangoakuit",
            "part": "Noun (Conc.)",
            "definition": "Clearsight - sight unhindered by emotion or illusion"
          },
          {
            "name_roman": "Fangkuit",
            "name_glyph": "fangkuit",
            "part": "Noun (Phys.)",
            "definition": "Window"
          },
          {
            "name_roman": "Fa''i",
            "name_glyph": "fa''I",
            "part": "Verb",
            "definition": "To be a gift to them (plural); to give to them (plural)"
          },
          {
            "name_roman": "Fe",
            "name_glyph": "fe",
            "part": "Pronoun",
            "definition": "Them (third-person singular)"
          },
          {
            "name_roman": "Fel",
            "name_glyph": "fel",
            "part": "Noun (Conc.)",
            "definition": "Lie"
          },
          {
            "name_roman": "Fela",
            "name_glyph": "fela",
            "part": "Noun (Conc.)",
            "definition": "Dishonesty"
          },
          {
            "name_roman": "Felad",
            "name_glyph": "felad",
            "part": "Verb",
            "definition": "To deceive"
          },
          {
            "name_roman": "Feladne",
            "name_glyph": "feladne",
            "part": "Noun (Phys.)",
            "definition": "Liar, deceiver"
          },
          {
            "name_roman": "Feladjul",
            "name_glyph": "feladjul",
            "part": "Adjective (Phys.)",
            "definition": "Deceitful"
          },
          {
            "name_roman": "Felau",
            "name_glyph": "felau",
            "part": "Adjective (Both)",
            "definition": "False, incorrect"
          },
          {
            "name_roman": "Felaud",
            "name_glyph": "felaud",
            "part": "Verb",
            "definition": "To be incorrect; to concede to"
          },
          {
            "name_roman": "Felkuit",
            "name_glyph": "felkuit",
            "part": "Noun (Conc.)",
            "definition": "Illusion"
          },
          {
            "name_roman": "Felkuito",
            "name_glyph": "felkuito",
            "part": "Adjective (Phys.)",
            "definition": "Illusory"
          },
          {
            "name_roman": "Felkuitoesh",
            "name_glyph": "felkuitoesh",
            "part": "Noun (Conc.)",
            "definition": "Mirage"
          },
          {
            "name_roman": "Fe''i",
            "name_glyph": "fe''I",
            "part": "Pronoun",
            "definition": "Them (third-person plural)"
          },
          {
            "name_roman": "Fila",
            "name_glyph": "fila",
            "part": "Noun (Conc.)",
            "definition": "Admiration"
          },
          {
            "name_roman": "Filad",
            "name_glyph": "filad",
            "part": "Verb",
            "definition": "To admire"
          },
          {
            "name_roman": "Filadne",
            "name_glyph": "filadne",
            "part": "Noun (Phys.)",
            "definition": "Admirer"
          },
          {
            "name_roman": "Filadjul",
            "name_glyph": "filadjul",
            "part": "Adjective (Both)",
            "definition": "Admirable"
          },
          {
            "name_roman": "Filau",
            "name_glyph": "filau",
            "part": "Adjective (Both)",
            "definition": "Appreciated"
          },
          {
            "name_roman": "Filauesh",
            "name_glyph": "filauesh",
            "part": "Noun (Conc.)",
            "definition": "Appreciation"
          },
          {
            "name_roman": "Fum",
            "name_glyph": "fum",
            "part": "Noun (Phys.)",
            "definition": "Rumble"
          },
          {
            "name_roman": "Fuma",
            "name_glyph": "fuma",
            "part": "Noun (Phys.)",
            "definition": "Sound"
          },
          {
            "name_roman": "Fumad",
            "name_glyph": "fumad",
            "part": "Verb",
            "definition": "To vibrate, make noise; to shake"
          },
          {
            "name_roman": "Fumadle",
            "name_glyph": "fumadle",
            "part": "Noun (Phys.)",
            "definition": "Musical instrument"
          },
          {
            "name_roman": "Fumadne",
            "name_glyph": "fumadne",
            "part": "Noun (Phys.)",
            "definition": "Vibrator, rumbler"
          },
          {
            "name_roman": "Fumadjul",
            "name_glyph": "fumadjul",
            "part": "Adjective (Phys.)",
            "definition": "Vibratory, prone to shaking"
          },
          {
            "name_roman": "Fumadjulesh",
            "name_glyph": "fumadjulesh",
            "part": "Noun (Conc.)",
            "definition": "Earthquake, tremor"
          },
          {
            "name_roman": "Fumo",
            "name_glyph": "fumo",
            "part": "Adjective (Phys.)",
            "definition": "Rumbly"
          },
          {
            "name_roman": "Fumau",
            "name_glyph": "fumau",
            "part": "Adjective (Phys.)",
            "definition": "Loud"
          },
          {
            "name_roman": "Fumauesh",
            "name_glyph": "fumauesh",
            "part": "Noun (Conc.)",
            "definition": "Shout, scream, yell"
          },
          {
            "name_roman": "Fumaueshad",
            "name_glyph": "fumaueshad",
            "part": "Verb",
            "definition": "To shout"
          },
          {
            "name_roman": "Fynta",
            "name_glyph": "fynta",
            "part": "Number",
            "definition": "Seven"
          },
          {
            "name_roman": "Fyntaesh",
            "name_glyph": "fyntaesh",
            "part": "Noun (Conc.)",
            "definition": "Heptagon"
          }
        ]
      },
      "h": {
        "glyph": "h",
        "words": [
          {
            "name_roman": "Hyjal",
            "name_glyph": "hyjal",
            "part": "Noun (Phys.)",
            "definition": "Flesh"
          },
          {
            "name_roman": "Hyjala",
            "name_glyph": "hyjala",
            "part": "Noun (Phys.)",
            "definition": "Muscle, musculature"
          },
          {
            "name_roman": "Hyjalad",
            "name_glyph": "hyjalad",
            "part": "Verb",
            "definition": "To move, ambulate; to shove"
          },
          {
            "name_roman": "Hyjaladle",
            "name_glyph": "hyjaladle",
            "part": "Noun (Phys.)",
            "definition": "Vehicle"
          },
          {
            "name_roman": "Hyjaladne",
            "name_glyph": "hyjaladne",
            "part": "Noun (Phys.)",
            "definition": "Mover, ambulator"
          },
          {
            "name_roman": "Hyjaladjul",
            "name_glyph": "hyjaladjul",
            "part": "Adjective (Phys.)",
            "definition": "Ambulatory, roaming, wandering"
          },
          {
            "name_roman": "Hyjalo",
            "name_glyph": "hyjalo",
            "part": "Adjective (Phys.)",
            "definition": "Fleshy"
          },
          {
            "name_roman": "Hyjaloa",
            "name_glyph": "hyjaloa",
            "part": "Noun (Phys.)",
            "definition": "Fat, adipose"
          },
          {
            "name_roman": "Hyjaload",
            "name_glyph": "hyjaload",
            "part": "Verb",
            "definition": "To gain weight; to stuff"
          },
          {
            "name_roman": "Hyjaloesh",
            "name_glyph": "hyjaloesh",
            "part": "Noun (Phys.)",
            "definition": "A particularly pudgy person"
          },
          {
            "name_roman": "Hyjalussauesh",
            "name_glyph": "hyjalussauesh",
            "part": "Noun (Phys.)",
            "definition": "Body"
          },
          {
            "name_roman": "Hyjalau",
            "name_glyph": "hyjalau",
            "part": "Adjective (Phys.)",
            "definition": "Muscular"
          },
          {
            "name_roman": "Hyjalauesh",
            "name_glyph": "hyjalauesh",
            "part": "Noun (Phys.)",
            "definition": "A particularly strong person"
          },
          {
            "name_roman": "Hjult",
            "name_glyph": "hjult",
            "part": "Noun (Phys.)",
            "definition": "Tree"
          },
          {
            "name_roman": "Hjulta",
            "name_glyph": "hjulta",
            "part": "Noun (Phys.)",
            "definition": "Forest"
          },
          {
            "name_roman": "Hjulto",
            "name_glyph": "hjulto",
            "part": "Adjective (Phys.)",
            "definition": "Tall"
          },
          {
            "name_roman": "Hjultau",
            "name_glyph": "hjultau",
            "part": "Adjective (Phys.)",
            "definition": "Dense"
          },
          {
            "name_roman": "Hjultaua",
            "name_glyph": "hjultaua",
            "part": "Noun (Conc.)",
            "definition": "Density"
          },
          {
            "name_roman": "Hjultussauesh",
            "name_glyph": "hjultussauesh",
            "part": "Noun (Phys.)",
            "definition": "Tower"
          },
          {
            "name_roman": "Hjultauesh",
            "name_glyph": "hjultauesh",
            "part": "Noun (Conc.)",
            "definition": "Singularity"
          },
          {
            "name_roman": "Hjulthyjal",
            "name_glyph": "hjulthyjal",
            "part": "Noun (Phys.)",
            "definition": "Trunk, torso"
          },
          {
            "name_roman": "Hjultji''ana",
            "name_glyph": "hjultji''Ana",
            "part": "Noun (Phys.)",
            "definition": "Arm, branch, tentacle"
          }
        ]
      },
      "j": {
        "glyph": "J",
        "words": [
          {
            "name_roman": "Jar",
            "name_glyph": "Jar",
            "part": "Noun (Phys.)",
            "definition": "Gem"
          },
          {
            "name_roman": "Jara",
            "name_glyph": "Jara",
            "part": "Noun (Phys.)",
            "definition": "Crystal structure"
          },
          {
            "name_roman": "Jarad",
            "name_glyph": "Jarad",
            "part": "Verb",
            "definition": "To encrust with gems"
          },
          {
            "name_roman": "Jaradne",
            "name_glyph": "Jaradne",
            "part": "Noun (Phys.)",
            "definition": "Encruster of gems"
          },
          {
            "name_roman": "Jaradjul",
            "name_glyph": "Jaradjul",
            "part": "Adjective (Phys.)",
            "definition": "Encrusted with gems"
          },
          {
            "name_roman": "Jaro",
            "name_glyph": "Jaro",
            "part": "Adjective (Both)",
            "definition": "Multifaceted, as of a topic or an object"
          },
          {
            "name_roman": "Jarau",
            "name_glyph": "Jarau",
            "part": "Adjective (Phys.)",
            "definition": "Crystalline"
          },
          {
            "name_roman": "Jearat",
            "name_glyph": "Jearat",
            "part": "Noun (Conc.)",
            "definition": "Peace"
          },
          {
            "name_roman": "Jearata",
            "name_glyph": "Jearata",
            "part": "Noun (Conc.)",
            "definition": "Tranquility"
          },
          {
            "name_roman": "Jearatad",
            "name_glyph": "Jearatad",
            "part": "Verb",
            "definition": "To be calm; to calm"
          },
          {
            "name_roman": "Jearato",
            "name_glyph": "Jearato",
            "part": "Adjective (Phys.)",
            "definition": "Placid"
          },
          {
            "name_roman": "Jearatau",
            "name_glyph": "Jearatau",
            "part": "Adjective (Phys.)",
            "definition": "Emotionally calm"
          },
          {
            "name_roman": "Jearys",
            "name_glyph": "Jearys",
            "part": "Noun (Conc.)",
            "definition": "Mundanity"
          },
          {
            "name_roman": "Jearyso",
            "name_glyph": "Jearyso",
            "part": "Adjective (Both)",
            "definition": "Mundane"
          },
          {
            "name_roman": "Jeeshaung",
            "name_glyph": "Jeeshaung",
            "part": "Noun (Phys.)",
            "definition": "Thin or runny liquid"
          },
          {
            "name_roman": "Jeeshaunga",
            "name_glyph": "Jeeshaunga",
            "part": "Noun (Conc.)",
            "definition": "Slipperiness"
          },
          {
            "name_roman": "Jeeshaungad",
            "name_glyph": "Jeeshaungad",
            "part": "Verb",
            "definition": "To slip, slide; to trip"
          },
          {
            "name_roman": "Jeeshaungadle",
            "name_glyph": "Jeeshaungadle",
            "part": "Noun (Phys.)",
            "definition": "Lubricant"
          },
          {
            "name_roman": "Jeeshaungadne",
            "name_glyph": "Jeeshaungadne",
            "part": "Noun (Phys.)",
            "definition": "Lubricator"
          },
          {
            "name_roman": "Jeeshaungadjul",
            "name_glyph": "Jeeshaungadjul",
            "part": "Adjective (Phys.)",
            "definition": "Unstuck"
          },
          {
            "name_roman": "Jeeshaungo",
            "name_glyph": "Jeeshaungo",
            "part": "Adjective (Phys.)",
            "definition": "Thin, of a liquid"
          },
          {
            "name_roman": "Jeeshaungoa",
            "name_glyph": "Jeeshaungoa",
            "part": "Noun (Conc.)",
            "definition": "Thinness"
          },
          {
            "name_roman": "Jeeshaungau",
            "name_glyph": "Jeeshaungau",
            "part": "Adjective (Phys.)",
            "definition": "Slippery"
          },
          {
            "name_roman": "Jeusso",
            "name_glyph": "Jeusso",
            "part": "Adjective (Phys.)",
            "definition": "Boneless"
          },
          {
            "name_roman": "Jeussau",
            "name_glyph": "Jeussau",
            "part": "Adjective (Both)",
            "definition": "Unstructured"
          },
          {
            "name_roman": "Jebi",
            "name_glyph": "Jebi",
            "part": "Noun (Conc.)",
            "definition": "Discomfort"
          },
          {
            "name_roman": "Jebid",
            "name_glyph": "Jebid",
            "part": "Verb",
            "definition": "To be uncomfortable; to cause discomfort in"
          },
          {
            "name_roman": "Jebja",
            "name_glyph": "Jebja",
            "part": "Noun (Conc.)",
            "definition": "Modesty of means, minimalism"
          },
          {
            "name_roman": "Jebjad",
            "name_glyph": "Jebjad",
            "part": "Verb",
            "definition": "To live modestly; to knock down a peg"
          },
          {
            "name_roman": "Jebjadjul",
            "name_glyph": "Jebjadjul",
            "part": "Adjective (Phys.)",
            "definition": "Modest"
          },
          {
            "name_roman": "Jebjo",
            "name_glyph": "Jebjo",
            "part": "Adjective (Phys.)",
            "definition": "Uncomfortable"
          },
          {
            "name_roman": "Jebjau",
            "name_glyph": "Jebjau",
            "part": "Adjective (Both)",
            "definition": "Minimal"
          },
          {
            "name_roman": "Jebjauesh",
            "name_glyph": "Jebjauesh",
            "part": "Noun (Conc.)",
            "definition": "The minimum"
          },
          {
            "name_roman": "Jepreit",
            "name_glyph": "Jepreit",
            "part": "Noun (Conc.)",
            "definition": "Lack of value, poverty"
          },
          {
            "name_roman": "Jepreita",
            "name_glyph": "Jepreita",
            "part": "Noun (Conc.)",
            "definition": "Contentment"
          },
          {
            "name_roman": "Jepreitad",
            "name_glyph": "Jepreitad",
            "part": "Verb",
            "definition": "To want not; to remove one''s appetite, cause another to no longer want something"
          },
          {
            "name_roman": "Jepreitadjul",
            "name_glyph": "Jepreitadjul",
            "part": "Adjective (Phys.)",
            "definition": "Wanting nothing"
          },
          {
            "name_roman": "Jepreito",
            "name_glyph": "Jepreito",
            "part": "Adjective (Both)",
            "definition": "Worthless, poor"
          },
          {
            "name_roman": "Jepreitau",
            "name_glyph": "Jepreitau",
            "part": "Adjective (Both)",
            "definition": "Content"
          },
          {
            "name_roman": "Jeda",
            "name_glyph": "Jeda",
            "part": "Noun (Conc.)",
            "definition": "Agreement (of nature or ideals)"
          },
          {
            "name_roman": "Jedad",
            "name_glyph": "Jedad",
            "part": "Verb",
            "definition": "To be whole; to agree with"
          },
          {
            "name_roman": "Jedadle",
            "name_glyph": "Jedadle",
            "part": "Noun (Phys.)",
            "definition": "Blender"
          },
          {
            "name_roman": "Jedadne",
            "name_glyph": "Jedadne",
            "part": "Noun (Phys.)",
            "definition": "Merger, combiner"
          },
          {
            "name_roman": "Jeda''a",
            "name_glyph": "Jeda''A",
            "part": "Noun (Conc.)",
            "definition": "Peacetime"
          },
          {
            "name_roman": "Jeda''ad",
            "name_glyph": "Jeda''Ad",
            "part": "Verb",
            "definition": "To make peace"
          },
          {
            "name_roman": "Jeda''adne",
            "name_glyph": "Jeda''Adne",
            "part": "Noun (Phys.)",
            "definition": "Peacemaker, peacekeeper"
          },
          {
            "name_roman": "Jeda''au",
            "name_glyph": "Jeda''Au",
            "part": "Adjective (Both)",
            "definition": "Peaceful (anti-war, unwarlike)"
          },
          {
            "name_roman": "Jedau",
            "name_glyph": "Jedau",
            "part": "Adjective (Both)",
            "definition": "Agreeable, compatible"
          },
          {
            "name_roman": "Jedauesh",
            "name_glyph": "Jedauesh",
            "part": "Noun (Conc.)",
            "definition": "Treaty"
          },
          {
            "name_roman": "Jedjar",
            "name_glyph": "Jedjar",
            "part": "Noun (Conc.)",
            "definition": "Salutation"
          },
          {
            "name_roman": "Jedjarad",
            "name_glyph": "Jedjarad",
            "part": "Verb",
            "definition": "To say goodbye; to leave someone"
          },
          {
            "name_roman": "Jedjarva",
            "name_glyph": "Jedjarva",
            "part": "Interjection",
            "definition": "Goodbye"
          },
          {
            "name_roman": "Jekalto",
            "name_glyph": "Jekalto",
            "part": "Adjective (Conc.)",
            "definition": "Visceral"
          },
          {
            "name_roman": "Jekaltoesh",
            "name_glyph": "Jekaltoesh",
            "part": "Noun (Conc.)",
            "definition": "Reality, existence"
          },
          {
            "name_roman": "Jekaltoesha",
            "name_glyph": "Jekaltoesha",
            "part": "Noun (Conc.)",
            "definition": "Soul"
          },
          {
            "name_roman": "Jekello",
            "name_glyph": "Jekello",
            "part": "Adjective (Phys.)",
            "definition": "Barren"
          },
          {
            "name_roman": "Jekelloesh",
            "name_glyph": "Jekelloesh",
            "part": "Noun (Phys.)",
            "definition": "Wasteland"
          },
          {
            "name_roman": "Jekuit",
            "name_glyph": "Jekuit",
            "part": "Noun (Conc.)",
            "definition": "Blindness"
          },
          {
            "name_roman": "Jekuitad",
            "name_glyph": "Jekuitad",
            "part": "Verb",
            "definition": "To be blind; to blind"
          },
          {
            "name_roman": "Jekuitadle",
            "name_glyph": "Jekuitadle",
            "part": "Noun (Phys.)",
            "definition": "Blindfold"
          },
          {
            "name_roman": "Jekuitadne",
            "name_glyph": "Jekuitadne",
            "part": "Noun (Phys.)",
            "definition": "Blinder"
          },
          {
            "name_roman": "Jekuitadjul",
            "name_glyph": "Jekuitadjul",
            "part": "Adjective (Phys.)",
            "definition": "Blind"
          },
          {
            "name_roman": "Jekuito",
            "name_glyph": "Jekuito",
            "part": "Adjective (Both)",
            "definition": "Invisible (Phys.), unapparent (Conc.)"
          },
          {
            "name_roman": "Jekuitoesh",
            "name_glyph": "Jekuitoesh",
            "part": "Noun (Conc.)",
            "definition": "Ghost, specter"
          },
          {
            "name_roman": "Jekrestad",
            "name_glyph": "Jekrestad",
            "part": "Verb",
            "definition": "To unglimmer, as in to be diffuse"
          },
          {
            "name_roman": "Jekrestadne",
            "name_glyph": "Jekrestadne",
            "part": "Noun (Phys.)",
            "definition": "Diffuser"
          },
          {
            "name_roman": "Jekrestadjul",
            "name_glyph": "Jekrestadjul",
            "part": "Adjective (Phys.)",
            "definition": "Matte, diffuse"
          },
          {
            "name_roman": "Jecroafa",
            "name_glyph": "Jecroafa",
            "part": "Noun (Phys.)",
            "definition": "Dam, obstruction"
          },
          {
            "name_roman": "Jecroafad",
            "name_glyph": "Jecroafad",
            "part": "Verb",
            "definition": "To obstruct"
          },
          {
            "name_roman": "Jecroafadle",
            "name_glyph": "Jecroafadle",
            "part": "Noun (Phys.)",
            "definition": "Barrier, fence"
          },
          {
            "name_roman": "Jecroafadne",
            "name_glyph": "Jecroafadne",
            "part": "Noun (Phys.)",
            "definition": "Obstructor"
          },
          {
            "name_roman": "Jecroo",
            "name_glyph": "Jecroo",
            "part": "Adjective (Phys.)",
            "definition": "Dry"
          },
          {
            "name_roman": "Jecroo''esh",
            "name_glyph": "Jecroo''Esh",
            "part": "Noun (Phys.)",
            "definition": "Dust"
          },
          {
            "name_roman": "Jecrod",
            "name_glyph": "Jecrod",
            "part": "Verb",
            "definition": "To evaporate; to dry"
          },
          {
            "name_roman": "Jecrodle",
            "name_glyph": "Jecrodle",
            "part": "Noun (Phys.)",
            "definition": "Towel"
          },
          {
            "name_roman": "Jecrodne",
            "name_glyph": "Jecrodne",
            "part": "Noun (Phys.)",
            "definition": "Dryer"
          },
          {
            "name_roman": "Jecrodjul",
            "name_glyph": "Jecrodjul",
            "part": "Adjective (Phys.)",
            "definition": "Parched"
          },
          {
            "name_roman": "Jecrodjulesh",
            "name_glyph": "Jecrodjulesh",
            "part": "Noun (Phys.)",
            "definition": "Desert"
          },
          {
            "name_roman": "Jezettad",
            "name_glyph": "Jezettad",
            "part": "Verb",
            "definition": "To no longer exist; to destroy"
          },
          {
            "name_roman": "Jezettadne",
            "name_glyph": "Jezettadne",
            "part": "Noun (Phys.)",
            "definition": "Destroyer"
          },
          {
            "name_roman": "Jesen",
            "name_glyph": "Jesen",
            "part": "Noun (Conc.)",
            "definition": "Dullness"
          },
          {
            "name_roman": "Jesenadjul",
            "name_glyph": "Jesenadjul",
            "part": "Adjective (Both)",
            "definition": "Whole again, as something once divided"
          },
          {
            "name_roman": "Jesenafa",
            "name_glyph": "Jesenafa",
            "part": "Noun (Conc.)",
            "definition": "Foolishness, stupidity"
          },
          {
            "name_roman": "Jesenafau",
            "name_glyph": "Jesenafau",
            "part": "Adjective (Both)",
            "definition": "Foolish, stupid"
          },
          {
            "name_roman": "Jeseno",
            "name_glyph": "Jeseno",
            "part": "Adjective (Both)",
            "definition": "Dull, of wit or blade"
          },
          {
            "name_roman": "Jeshuro",
            "name_glyph": "Jeshuro",
            "part": "Adjective (Both)",
            "definition": "Dishonest"
          },
          {
            "name_roman": "Jeshjad",
            "name_glyph": "Jeshjad",
            "part": "Verb",
            "definition": "To be displeased; to displease"
          },
          {
            "name_roman": "Jeshjadne",
            "name_glyph": "Jeshjadne",
            "part": "Noun (Phys.)",
            "definition": "Displeaser"
          },
          {
            "name_roman": "Jemyra",
            "name_glyph": "Jemyra",
            "part": "Noun (Conc.)",
            "definition": "Asphyxiation"
          },
          {
            "name_roman": "Jemyrad",
            "name_glyph": "Jemyrad",
            "part": "Verb",
            "definition": "To choke on food; to choke someone"
          },
          {
            "name_roman": "Jemyradne",
            "name_glyph": "Jemyradne",
            "part": "Noun (Phys.)",
            "definition": "Choker"
          },
          {
            "name_roman": "Jemyro",
            "name_glyph": "Jemyro",
            "part": "Adjective (Phys.)",
            "definition": "Sluggish, slow"
          },
          {
            "name_roman": "Jemyroa",
            "name_glyph": "Jemyroa",
            "part": "Noun (Conc.)",
            "definition": "Slowness"
          },
          {
            "name_roman": "Jenar",
            "name_glyph": "Jenar",
            "part": "Noun (Conc.)",
            "definition": "Lack of sensation, numness"
          },
          {
            "name_roman": "Jenara",
            "name_glyph": "Jenara",
            "part": "Noun (Conc.)",
            "definition": "Apathy"
          },
          {
            "name_roman": "Jenarad",
            "name_glyph": "Jenarad",
            "part": "Verb",
            "definition": "To not care; to bore"
          },
          {
            "name_roman": "Jenaradjul",
            "name_glyph": "Jenaradjul",
            "part": "Adjective (Phys.)",
            "definition": "Apathetic"
          },
          {
            "name_roman": "Jenaro",
            "name_glyph": "Jenaro",
            "part": "Adjective (Phys.)",
            "definition": "Numb, lacking feeling"
          },
          {
            "name_roman": "Jenaroesh",
            "name_glyph": "Jenaroesh",
            "part": "Noun (Conc.)",
            "definition": "Senselessness, dissociativity"
          },
          {
            "name_roman": "Jenaroeshad",
            "name_glyph": "Jenaroeshad",
            "part": "Verb",
            "definition": "To dissociate"
          },
          {
            "name_roman": "Jenaroeshadne",
            "name_glyph": "Jenaroeshadne",
            "part": "Noun (Phys.)",
            "definition": "Dissociator"
          },
          {
            "name_roman": "Jenaroeshadjul",
            "name_glyph": "Jenaroeshadjul",
            "part": "Adjective (Both)",
            "definition": "Dissociative"
          },
          {
            "name_roman": "Jenarau",
            "name_glyph": "Jenarau",
            "part": "Adjective (Both)",
            "definition": "Emotionless"
          },
          {
            "name_roman": "Jemjotadjul",
            "name_glyph": "Jemjotadjul",
            "part": "Adjective (Both)",
            "definition": "Sedentary"
          },
          {
            "name_roman": "Jemjoto",
            "name_glyph": "Jemjoto",
            "part": "Adjective (Both)",
            "definition": "Unwelcome, uninvited"
          },
          {
            "name_roman": "Jemjotoesh",
            "name_glyph": "Jemjotoesh",
            "part": "Noun (Phys.)",
            "definition": "Intruder, invader"
          },
          {
            "name_roman": "Jeval",
            "name_glyph": "Jeval",
            "part": "Noun (Conc.)",
            "definition": "Timelessness, immortality, permanence"
          },
          {
            "name_roman": "Jevala",
            "name_glyph": "Jevala",
            "part": "Noun (Conc.)",
            "definition": "Deity, god (general concept)"
          },
          {
            "name_roman": "Jevalafa",
            "name_glyph": "Jevalafa",
            "part": "Noun (Conc.)",
            "definition": "Deity, god (Entity)"
          },
          {
            "name_roman": "Jevala''a",
            "name_glyph": "Jevala''A",
            "part": "Noun (Conc.)",
            "definition": "Religion"
          },
          {
            "name_roman": "Jevala''ad",
            "name_glyph": "Jevala''Ad",
            "part": "Verb",
            "definition": "To worship"
          },
          {
            "name_roman": "Jevala''adne",
            "name_glyph": "Jevala''Adne",
            "part": "Noun (Phys.)",
            "definition": "Worshiper"
          },
          {
            "name_roman": "Jevalo",
            "name_glyph": "Jevalo",
            "part": "Adjective (Both)",
            "definition": "Timeless, immortal, permanent"
          },
          {
            "name_roman": "Jevalau",
            "name_glyph": "Jevalau",
            "part": "Adjective (Both)",
            "definition": "Divine"
          },
          {
            "name_roman": "Jevalaurok",
            "name_glyph": "Jevalaurok",
            "part": "Noun (Phys.)",
            "definition": "Deity, god (Avatar)"
          },
          {
            "name_roman": "Jevidod",
            "name_glyph": "Jevidod",
            "part": "Verb",
            "definition": "To degrade; to ruin"
          },
          {
            "name_roman": "Jevidodle",
            "name_glyph": "Jevidodle",
            "part": "Noun (Conc.)",
            "definition": "Insult"
          },
          {
            "name_roman": "Jevidodne",
            "name_glyph": "Jevidodne",
            "part": "Noun (Phys.)",
            "definition": "Degrader"
          },
          {
            "name_roman": "Jefango",
            "name_glyph": "Jefango",
            "part": "Adjective (Phys.)",
            "definition": "Opaque"
          },
          {
            "name_roman": "Jefangoa",
            "name_glyph": "Jefangoa",
            "part": "Noun (Conc.)",
            "definition": "Opacity"
          },
          {
            "name_roman": "Jefila",
            "name_glyph": "Jefila",
            "part": "Noun (Conc.)",
            "definition": "Condescension"
          },
          {
            "name_roman": "Jefilad",
            "name_glyph": "Jefilad",
            "part": "Verb",
            "definition": "To condescend"
          },
          {
            "name_roman": "Jefiladne",
            "name_glyph": "Jefiladne",
            "part": "Noun (Phys.)",
            "definition": "Condescender"
          },
          {
            "name_roman": "Jefiladjul",
            "name_glyph": "Jefiladjul",
            "part": "Adjective (Both)",
            "definition": "Condescending"
          },
          {
            "name_roman": "Jefilau",
            "name_glyph": "Jefilau",
            "part": "Adjective (Both)",
            "definition": "Lesser (negative connotation)"
          },
          {
            "name_roman": "Jefum",
            "name_glyph": "Jefum",
            "part": "Noun (Conc.)",
            "definition": "Stillness"
          },
          {
            "name_roman": "Jefuma",
            "name_glyph": "Jefuma",
            "part": "Noun (Conc.)",
            "definition": "Silence"
          },
          {
            "name_roman": "Jefumad",
            "name_glyph": "Jefumad",
            "part": "Verb",
            "definition": "To be quiet; silence"
          },
          {
            "name_roman": "Jefumadle",
            "name_glyph": "Jefumadle",
            "part": "Noun (Phys.)",
            "definition": "Muffler, gag"
          },
          {
            "name_roman": "Jefumadne",
            "name_glyph": "Jefumadne",
            "part": "Noun (Phys.)",
            "definition": "Silencer"
          },
          {
            "name_roman": "Jefumo",
            "name_glyph": "Jefumo",
            "part": "Adjective (Phys.)",
            "definition": "Still"
          },
          {
            "name_roman": "Jefumau",
            "name_glyph": "Jefumau",
            "part": "Adjective (Phys.)",
            "definition": "Quiet, silent"
          },
          {
            "name_roman": "Jehyjalad",
            "name_glyph": "Jehyjalad",
            "part": "Verb",
            "definition": "To be still, hibernate; knock out"
          },
          {
            "name_roman": "Jehyjaladle",
            "name_glyph": "Jehyjaladle",
            "part": "Noun (Phys.)",
            "definition": "Tranquilizer"
          },
          {
            "name_roman": "Jehyjaladne",
            "name_glyph": "Jehyjaladne",
            "part": "Noun (Phys.)",
            "definition": "Hibernator"
          },
          {
            "name_roman": "Jehyjaladjul",
            "name_glyph": "Jehyjaladjul",
            "part": "Adjective (Phys.)",
            "definition": "Sedentary, hibernatory"
          },
          {
            "name_roman": "Jehyjaladjulesh",
            "name_glyph": "Jehyjaladjulesh",
            "part": "Noun (Conc.)",
            "definition": "Hibernation, coma"
          },
          {
            "name_roman": "Jehyjalau",
            "name_glyph": "Jehyjalau",
            "part": "Adjective (Phys.)",
            "definition": "Atrophied"
          },
          {
            "name_roman": "Jehyjalaua",
            "name_glyph": "Jehyjalaua",
            "part": "Noun (Conc.)",
            "definition": "Weakness"
          },
          {
            "name_roman": "Jehyjalauesh",
            "name_glyph": "Jehyjalauesh",
            "part": "Noun (Conc.)",
            "definition": "Atrophy"
          },
          {
            "name_roman": "Jehjulto",
            "name_glyph": "Jehjulto",
            "part": "Adjective (Phys.)",
            "definition": "Short of stature"
          },
          {
            "name_roman": "Jehjultoesh",
            "name_glyph": "Jehjultoesh",
            "part": "Noun (Conc.)",
            "definition": "Shortness of stature"
          },
          {
            "name_roman": "Jehjultau",
            "name_glyph": "Jehjultau",
            "part": "Adjective (Phys.)",
            "definition": "Sparse, scarce"
          },
          {
            "name_roman": "Jehjultaua",
            "name_glyph": "Jehjultaua",
            "part": "Noun (Conc.)",
            "definition": "Scarcity"
          },
          {
            "name_roman": "Jehjultauesh",
            "name_glyph": "Jehjultauesh",
            "part": "Noun (Conc.)",
            "definition": "Collapse of an ecosystem"
          },
          {
            "name_roman": "Jehjultaueshad",
            "name_glyph": "Jehjultaueshad",
            "part": "Verb",
            "definition": "To collapse, as an ecosystem; to cause environmental collapse"
          },
          {
            "name_roman": "Jehjultaueshadjul",
            "name_glyph": "Jehjultaueshadjul",
            "part": "Adjective (Phys.)",
            "definition": "Uninhabitable"
          },
          {
            "name_roman": "Jeji''anad",
            "name_glyph": "Jeji''Anad",
            "part": "Verb",
            "definition": "To loosen; to let go of"
          },
          {
            "name_roman": "Jeji''anadle",
            "name_glyph": "Jeji''Anadle",
            "part": "Noun (Phys.)",
            "definition": "Wrench"
          },
          {
            "name_roman": "Jeji''anadne",
            "name_glyph": "Jeji''Anadne",
            "part": "Noun (Phys.)",
            "definition": "Loosener"
          },
          {
            "name_roman": "Jerusad",
            "name_glyph": "Jerusad",
            "part": "Verb",
            "definition": "To hurt"
          },
          {
            "name_roman": "Jerusadle",
            "name_glyph": "Jerusadle",
            "part": "Noun (Phys.)",
            "definition": "Weapon"
          },
          {
            "name_roman": "Jerusadne",
            "name_glyph": "Jerusadne",
            "part": "Noun (Phys.)",
            "definition": "Harmer"
          },
          {
            "name_roman": "Jerusadle''e",
            "name_glyph": "Jerusadle''E",
            "part": "Verb",
            "definition": "To attack"
          },
          {
            "name_roman": "Jerusadle''ene",
            "name_glyph": "Jerusadle''Ene",
            "part": "Noun (Phys.)",
            "definition": "Attacker"
          },
          {
            "name_roman": "Jerusadjul",
            "name_glyph": "Jerusadjul",
            "part": "Adjective (Phys.)",
            "definition": "Pained, painful"
          },
          {
            "name_roman": "Jerusadjula",
            "name_glyph": "Jerusadjula",
            "part": "Noun (Conc.)",
            "definition": "Harm"
          },
          {
            "name_roman": "Jerusadjulesh",
            "name_glyph": "Jerusadjulesh",
            "part": "Noun (Conc.)",
            "definition": "Pain"
          },
          {
            "name_roman": "Jerusadjulau",
            "name_glyph": "Jerusadulau",
            "part": "Adjective (Both)",
            "definition": "Harmful"
          },
          {
            "name_roman": "Jeruvak",
            "name_glyph": "Jeruvak",
            "part": "Noun (Conc.)",
            "definition": "Laziness"
          },
          {
            "name_roman": "Jeruvako",
            "name_glyph": "Jeruvako",
            "part": "Adjective (Phys.)",
            "definition": "Lazy"
          },
          {
            "name_roman": "Je''ygad",
            "name_glyph": "Je''Ygad",
            "part": "Verb",
            "definition": "To shrink"
          },
          {
            "name_roman": "Je''ygadne",
            "name_glyph": "Je''Ygadne",
            "part": "Noun (Phys.)",
            "definition": "Shrinker"
          },
          {
            "name_roman": "Je''ygadjul",
            "name_glyph": "Je''Ygadjul",
            "part": "Adjective (Phys.)",
            "definition": "Shrunken"
          },
          {
            "name_roman": "Je''ygo",
            "name_glyph": "Je''Ygo",
            "part": "Adjective (Phys.)",
            "definition": "Small"
          },
          {
            "name_roman": "Je''aut",
            "name_glyph": "Je''Aut",
            "part": "Noun (Conc.)",
            "definition": "Undeath"
          },
          {
            "name_roman": "Je''auta",
            "name_glyph": "Je''Auta",
            "part": "Noun (Conc.)",
            "definition": "Necromancy, resurrection"
          },
          {
            "name_roman": "Je''autad",
            "name_glyph": "Je''Autad",
            "part": "Verb",
            "definition": "To come back to life, resurrect, reanimate"
          },
          {
            "name_roman": "Je''autadne",
            "name_glyph": "Je''Autadne",
            "part": "Noun (Phys.)",
            "definition": "Reanimator, resurrector"
          },
          {
            "name_roman": "Je''auto",
            "name_glyph": "Je''Auto",
            "part": "Adjective (Phys.)",
            "definition": "Undead, reborn"
          },
          {
            "name_roman": "Je''autoesh",
            "name_glyph": "Je''Autoesh",
            "part": "Noun (Phys.)",
            "definition": "Zombie"
          },
          {
            "name_roman": "Je''autau",
            "name_glyph": "Je''Autau",
            "part": "Adjective (Both)",
            "definition": "Necromantic, resurrective"
          },
          {
            "name_roman": "Ji''an",
            "name_glyph": "Ji''An",
            "part": "Noun (Conc.)",
            "definition": "Traction, grasp"
          },
          {
            "name_roman": "Ji''ana",
            "name_glyph": "Ji''Ana",
            "part": "Noun (Phys.)",
            "definition": "Hand"
          },
          {
            "name_roman": "Ji''anad",
            "name_glyph": "Ji''Anad",
            "part": "Verb",
            "definition": "To tighten; to grab"
          },
          {
            "name_roman": "Ji''anadle",
            "name_glyph": "Ji''Anadle",
            "part": "Noun (Phys.)",
            "definition": "Vice"
          },
          {
            "name_roman": "Ji''anadne",
            "name_glyph": "Ji''Anadne",
            "part": "Noun (Phys.)",
            "definition": "Tightener, grabber"
          },
          {
            "name_roman": "Ji''ano",
            "name_glyph": "Ji''Ano",
            "part": "Adjective (Phys.)",
            "definition": "Tight"
          },
          {
            "name_roman": "Ji''anau",
            "name_glyph": "Ji''Anau",
            "part": "Adjective (Phys.)",
            "definition": "Manual"
          },
          {
            "name_roman": "Jyt",
            "name_glyph": "Jyt",
            "part": "Noun (Conc.)",
            "definition": "Distance"
          },
          {
            "name_roman": "Jyta",
            "name_glyph": "Jyta",
            "part": "Noun (Conc.)",
            "definition": "Path"
          },
          {
            "name_roman": "Jytad",
            "name_glyph": "Jytad",
            "part": "Verb",
            "definition": "To pathfind, to follow; to guide"
          },
          {
            "name_roman": "Jytadle",
            "name_glyph": "Jytadle",
            "part": "Noun (Phys.)",
            "definition": "Directional sign"
          },
          {
            "name_roman": "Jytadne",
            "name_glyph": "Jytadne",
            "part": "Noun (Phys.)",
            "definition": "Guide, pathfinder, navigator"
          },
          {
            "name_roman": "Jytahjultji''ana",
            "name_glyph": "Jytahjultji''Ana",
            "part": "Noun (Phys.)",
            "definition": "Leg"
          },
          {
            "name_roman": "Jytahjultji''anad",
            "name_glyph": "Jytahjultji''Anad",
            "part": "Verb",
            "definition": "To walk"
          },
          {
            "name_roman": "Jytahjultji''anadle",
            "name_glyph": "Jytahjultji''Anadle",
            "part": "Noun (Phys.)",
            "definition": "Cane, walker"
          },
          {
            "name_roman": "Jytahjultji''anadne",
            "name_glyph": "Jytahjultji''Anadne",
            "part": "Noun (Phys.)",
            "definition": "Hiker, walker"
          },
          {
            "name_roman": "Jytaji''ana",
            "name_glyph": "Jytaji''Ana",
            "part": "Noun (Phys.)",
            "definition": "Foot"
          },
          {
            "name_roman": "Jytaji''anad",
            "name_glyph": "Jytaji''Anad",
            "part": "Verb",
            "definition": "To step; to kick"
          },
          {
            "name_roman": "Jytaji''anadle",
            "name_glyph": "Jytaji''Anadle",
            "part": "Noun (Phys.)",
            "definition": "Shoe"
          },
          {
            "name_roman": "Jytaji''anadne",
            "name_glyph": "Jytaji''Anadne",
            "part": "Noun (Phys.)",
            "definition": "Kicker"
          },
          {
            "name_roman": "Jytau",
            "name_glyph": "Jytau",
            "part": "Adjective (Phys.)",
            "definition": "Meandering"
          },
          {
            "name_roman": "Jytaurok",
            "name_glyph": "Jytaurok",
            "part": "Noun (Phys.)",
            "definition": "Road, pathway"
          },
          {
            "name_roman": "J''erad",
            "name_glyph": "J''Erad",
            "part": "Verb",
            "definition": "To bring a physical object"
          },
          {
            "name_roman": "J''eradne",
            "name_glyph": "J''Eradne",
            "part": "Noun (Phys.)",
            "definition": "Bringer"
          }
        ]
      },
      "r": {
        "glyph": "R",
        "words": [
          {
            "name_roman": "Rok",
            "name_glyph": "Rok",
            "part": "Noun (Phys.)",
            "definition": "Fire, heat"
          },
          {
            "name_roman": "Roka",
            "name_glyph": "Roka",
            "part": "Noun (Conc.)",
            "definition": "Temperature"
          },
          {
            "name_roman": "Rokad",
            "name_glyph": "Rokad",
            "part": "Verb",
            "definition": "To be hot; to heat"
          },
          {
            "name_roman": "Rokadle",
            "name_glyph": "Rokadle",
            "part": "Noun (Phys.)",
            "definition": "Heater"
          },
          {
            "name_roman": "Rokadne",
            "name_glyph": "Rokadne",
            "part": "Noun (Phys.)",
            "definition": "One who warms"
          },
          {
            "name_roman": "Roko",
            "name_glyph": "Roko",
            "part": "Adjective (Phys.)",
            "definition": "Aflame, molten"
          },
          {
            "name_roman": "Rokau",
            "name_glyph": "Rokau",
            "part": "Adjective (Phys.)",
            "definition": "Hot"
          },
          {
            "name_roman": "Rokkrest",
            "name_glyph": "Rokkrest",
            "part": "Noun (Conc.)",
            "definition": "Sunlight"
          },
          {
            "name_roman": "Rokkrestad",
            "name_glyph": "Rokkrestad",
            "part": "Verb",
            "definition": "To become brighter; to brighten"
          },
          {
            "name_roman": "Rokkrestadle",
            "name_glyph": "Rokkrestadle",
            "part": "Noun (Phys.)",
            "definition": "Lamp, torch"
          },
          {
            "name_roman": "Rokkrestadne",
            "name_glyph": "Rokkrestadne",
            "part": "Noun (Phys.)",
            "definition": "Brightener"
          },
          {
            "name_roman": "Rokkresto",
            "name_glyph": "Rokkresto",
            "part": "Adjective (Phys.)",
            "definition": "Bright"
          },
          {
            "name_roman": "Rus",
            "name_glyph": "Rus",
            "part": "Noun (Conc.)",
            "definition": "Life"
          },
          {
            "name_roman": "Rusa",
            "name_glyph": "Rusa",
            "part": "Noun (Phys.)",
            "definition": "Organism"
          },
          {
            "name_roman": "Rusad",
            "name_glyph": "Rusad",
            "part": "Verb",
            "definition": "To live; to animate, heal"
          },
          {
            "name_roman": "Rusadne",
            "name_glyph": "Rusadne",
            "part": "Noun (Phys.)",
            "definition": "Healer"
          },
          {
            "name_roman": "Rusadjul",
            "name_glyph": "Rusadjul",
            "part": "Adjective (Phys.)",
            "definition": "Healed, rejuvenated"
          },
          {
            "name_roman": "Rusadjula",
            "name_glyph": "Rusadjula",
            "part": "Noun (Conc.)",
            "definition": "Health"
          },
          {
            "name_roman": "Rusadjulau",
            "name_glyph": "Rusadjulau",
            "part": "Adjective (Phys.)",
            "definition": "Healthy"
          },
          {
            "name_roman": "Ruso",
            "name_glyph": "Ruso",
            "part": "Adjective (Phys.)",
            "definition": "Alive"
          },
          {
            "name_roman": "Rusau",
            "name_glyph": "Rusau",
            "part": "Adjective (Phys.)",
            "definition": "Organic"
          },
          {
            "name_roman": "Ruvak",
            "name_glyph": "Ruvak",
            "part": "Noun (Conc.)",
            "definition": "Vigilance"
          },
          {
            "name_roman": "Ruvako",
            "name_glyph": "Ruvako",
            "part": "Adjective (Phys.)",
            "definition": "Vigilant"
          }
        ]
      }
    }');