-- Dummy table --
DROP TABLE IF EXISTS planets;

CREATE TABLE planets(
  id SERIAL UNIQUE PRIMARY KEY NOT NULL, 
  "name" VARCHAR(32), 
  "description" BYTEA,
  "information" BYTEA
);

CREATE TABLE deities(
  id SERIAL UNIQUE PRIMARY KEY NOT NULL,
  "name" VARCHAR(32),
  "description" BYTEA,
  "information" BYTEA
);

CREATE TABLE languages(
  id SERIAL UNIQUE PRIMARY KEY NOT NULL,
  "name_roman" VARCHAR(32),
  "name_glyph" VARCHAR(32),
  "description" BYTEA,
  "main_info" BYTEA,
  "inv_orth_info" BYTEA,
  "syntax_info" BYTEA,
  "lex_info" BYTEA
);

CREATE TABLE lexicons (
  id SERIAL UNIQUE PRIMARY KEY NOT NULL,
  "language" VARCHAR(32),
  "order" VARCHAR(32)[],
  "sections" JSONB
);
